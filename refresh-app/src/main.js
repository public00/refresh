import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import 'vuetify/dist/vuetify.min.css'
import VueResource from 'vue-resource'
import store from './stores/app-store.js';

// custom components
import Entrances from "./components/Entrances.vue";
import EntrancesByCity from "./components/EntrancesByCity.vue";
import Finances from "./components/Finances.vue";
import NotFound from "./components/NotFound.vue";
import Announcements from "./components/Announcements.vue";
import LegalOffice from "./components/LegalOffice.vue";
import ExternalUsers from "./components/ExternalUsers.vue";
import InternalReports from "./components/InternalReports.vue";
import JanitorReports from "./components/JanitorReports.vue";
import Invoices from "./components/Invoices.vue";
import Login from "./components/Login.vue";
import Cities from "./components/Cities.vue";
import Owners from "./components/Owners.vue";
import PropertyTypes from "./components/PropertyTypes.vue";
import Properties from "./components/Properties.vue";
import ExpenseCategories from "./components/ExpenseCategories.vue";
import EntityView from "./components/EntityView.vue";
import PropertyView from "./components/PropertyView.vue";
import TicketView from "./components/TicketView.vue";
import Employees from "./components/Employees.vue";
import AddEditInvoices from "./components/AddEditInvoices.vue";
import ActiveTickets from "./components/ActiveTickets.vue";
import "@progress/kendo-ui";
import "@progress/kendo-theme-material/dist/all.css";
import { GridInstaller } from "@progress/kendo-grid-vue-wrapper";
import { Editor, EditorTool, EditorInstaller } from '@progress/kendo-editor-vue-wrapper';


import {
  // AutoComplete,
  // ComboBox,
  // DropDownList,
  // MultiSelect,
  // MultiColumnComboBox,
  // MultiColumnComboBoxColumn,
  DropdownsInstaller
} from '@progress/kendo-dropdowns-vue-wrapper'

Vue.use(DropdownsInstaller);
Vue.use(VueResource);
Vue.use(GridInstaller);
Vue.use(EditorInstaller);
Vue.use(VueRouter)
Vue.use(Vuetify, {
  theme: {
    primary: "#1980bb",
    accent: '#13618e',
  }
})
Vue.use(Vuex)
Vue.config.productionTip = false

Vue.http.options.root = process.env.VUE_APP_BASE_URI;
Vue.http.interceptors.push(function (request) {
  if (store.state.token) {
    request.headers.set('Authorization', 'Bearer ' + store.state.token);
  } else {
    request.headers.set('Authorization', '');
  }
});

const routes = [
  { path: '/', component: Entrances },
  { path: '/ulazi', component: Entrances },
  { path: '/ulazi/gradovi', component: EntrancesByCity },
  { path: '/finansije', component: Finances },
  { path: '/pravna-sluzba', component: LegalOffice },
  { path: '/saopstenja', component: Announcements },
  { path: '/eksterni-klijenti', component: ExternalUsers },
  { path: '/interni-izvjestaji', component: InternalReports },
  { path: '/fakturisanje', component: Invoices },
  { path: '/login', component: Login },
  { path: '/gradovi', component: Cities },
  { path: '/vlasnici', component: Owners },
  { path: '/tipovi-stanova', component: PropertyTypes },
  { path: '/stanovi', component: Properties },
  { path: '/zaposleni', component: Employees },
  { path: '/kategorije-troskova', component: ExpenseCategories },
  { path: '/pregled', name: 'pregled', component: EntityView},
  { path: '/prostor', name: 'prostor', component: PropertyView},
  { path: '/zalba', name: 'zalba', component: TicketView},
  { path: '/domarska-sluzba', name: 'domarska-sluzba', component: JanitorReports},
  { path: '/faktura', name: 'faktura', component: AddEditInvoices},
  { path: '/aktivne-zalbe', name: 'aktivne-zalbe', component: ActiveTickets},
  {
    path: '*',
    component: NotFound
  }
]

const router = new VueRouter({
  routes

})

router.beforeEach((to, from, next) => {
  const unauthorized = !store.state.token;
  const isEndUser = store.state.userInfo.userTypeId === 5; 


  if (unauthorized && to.path !== '/login') {
    next({ path: '/login' })
  } else {
    if (isEndUser) {
      if (to.path === "/ulazi/gradovi" || to.path === "/ulazi" || to.path === "/pregled" || to.path === "/prostor" || to.path === "/zalba") {
        next()
      } else {
        next({path: '/ulazi'})
      }
    } else {
      next()
    }
  }

})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
