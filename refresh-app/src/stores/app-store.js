import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
      token: localStorage.getItem('user-token') ? JSON.parse(localStorage.getItem('user-token')).result.accessToken : '',
      currentTab: {id: 0, name: "Info"},
      entityId: '',
      userInfo: localStorage.getItem('user-info') ? JSON.parse(localStorage.getItem('user-info')) : ''
    },
    mutations: {
      setToken (state, data) {
        if (data && data.result) {
          state.token = data.result.accessToken;
        } else {
          state.token = data;
        }
      },
      setCurrentTab(state, data) {
          state.currentTab = data;
      },
      setEntityId(state, data) {
          state.entityId = data;
      },
      setUserInfo(state, data) {
        Vue.set(state, 'userInfo', data);
      }
    },
    getters: {
      getToken(state) {
        return state.token
      }
    }
  })