import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
      token: localStorage.getItem('user-token') ? JSON.parse(localStorage.getItem('user-token')).result.accessToken : ''
    },
    mutations: {
      setToken (state, data) {
        if (data && data.result) {
          state.token = data.result.accessToken;
        } else {
          state.token = data;
        }
      }
    },
    getters: {
      getToken(state) {
        return state.token
      }
    }
  })