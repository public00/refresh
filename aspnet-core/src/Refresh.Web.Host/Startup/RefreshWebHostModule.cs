﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Refresh.Configuration;

namespace Refresh.Web.Host.Startup
{
    using Abp.Quartz;

    [DependsOn(
        typeof(RefreshWebCoreModule),
        typeof(AbpQuartzModule))]
    public class RefreshWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public RefreshWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(RefreshWebHostModule).GetAssembly());
        }
    }
}
