using Microsoft.AspNetCore.Antiforgery;
using Refresh.Controllers;

namespace Refresh.Web.Host.Controllers
{
    public class AntiForgeryController : RefreshControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
