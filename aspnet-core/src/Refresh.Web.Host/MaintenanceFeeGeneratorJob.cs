﻿namespace Refresh.Web.Host
{
    using Abp.Dependency;
    using Abp.Domain.Repositories;
    using Abp.Domain.Uow;
    using Abp.Quartz;
    using Billing;
    using Entities.Buildings;
    using Quartz;
    using System;
    using System.Threading.Tasks;

    [DisallowConcurrentExecution]
    public class MaintenanceFeeGeneratorJob : JobBase, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly BillGenerator _billGenerator;
        private readonly IRepository<Building> _buildingRepository;
        private readonly PaymentManager _paymentManager;

        public MaintenanceFeeGeneratorJob(
            BillGenerator billGenerator, 
            IRepository<Building> buildingRepository, 
            IUnitOfWorkManager unitOfWorkManager, 
            PaymentManager paymentManager)
        {
            _billGenerator = billGenerator;
            _buildingRepository = buildingRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _paymentManager = paymentManager;
        }
         
        public override async Task Execute(IJobExecutionContext context)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                if (DateTime.UtcNow.Day < 7 && DateTime.UtcNow.Hour > 18 && DateTime.UtcNow.Hour < 5)
                {
                    var buildings = await _buildingRepository.GetAllListAsync(x =>
                        !x.IsDeleted
                        && x.ContractStartDate < DateTime.UtcNow
                        && (!x.ContractEndDate.HasValue
                            || x.ContractEndDate.HasValue
                            && x.ContractEndDate > DateTime.UtcNow));

                    foreach (var building in buildings)
                    {
                        await _billGenerator.GenerateMonthlyMaintenanceFeesBuilding(building.Id);
                        await _billGenerator.GenerateScheduledFeesBuilding(building.Id);
                        await _billGenerator.GenerateScheduledExpenses(building.Id);
                    }
                }
                
                await _paymentManager.SyncPayments();
                unitOfWork.Complete();
            }
        }
    }
}
