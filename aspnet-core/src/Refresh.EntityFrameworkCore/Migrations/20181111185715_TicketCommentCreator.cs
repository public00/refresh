﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class TicketCommentCreator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_TicketComments_CreatorUserId",
                schema: "dbo",
                table: "TicketComments",
                column: "CreatorUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TicketComments_AbpUsers_CreatorUserId",
                schema: "dbo",
                table: "TicketComments",
                column: "CreatorUserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TicketComments_AbpUsers_CreatorUserId",
                schema: "dbo",
                table: "TicketComments");

            migrationBuilder.DropIndex(
                name: "IX_TicketComments_CreatorUserId",
                schema: "dbo",
                table: "TicketComments");
        }
    }
}
