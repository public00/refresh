﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class references : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Tickets_BuildingId",
                schema: "dbo",
                table: "Tickets",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_PropertyId",
                schema: "dbo",
                table: "Tickets",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_SentPayments_BuildingId",
                schema: "dbo",
                table: "SentPayments",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SentPayments_ExpenseId",
                schema: "dbo",
                table: "SentPayments",
                column: "ExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledMaintenanceFees_CategoryId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledMaintenanceFees_PropertyId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledExpense_BuildingId",
                schema: "dbo",
                table: "ScheduledExpense",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledExpense_CategoryId",
                schema: "dbo",
                table: "ScheduledExpense",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledExpense_SubCategoryId",
                schema: "dbo",
                table: "ScheduledExpense",
                column: "SubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceivedPayments_BuildingId",
                schema: "dbo",
                table: "ReceivedPayments",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceivedPayments_PropertyId",
                schema: "dbo",
                table: "ReceivedPayments",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceFee_CategoryId",
                schema: "dbo",
                table: "MaintenanceFee",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceFee_PropertyId",
                schema: "dbo",
                table: "MaintenanceFee",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_BuildingId",
                schema: "dbo",
                table: "Expenses",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_CategoryId",
                schema: "dbo",
                table: "Expenses",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_SubCategoryId",
                schema: "dbo",
                table: "Expenses",
                column: "SubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Buildings_BuildingId",
                schema: "dbo",
                table: "Expenses",
                column: "BuildingId",
                principalSchema: "dbo",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_ExpenseCategory_CategoryId",
                schema: "dbo",
                table: "Expenses",
                column: "CategoryId",
                principalSchema: "dbo",
                principalTable: "ExpenseCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_ExpenseCategory_SubCategoryId",
                schema: "dbo",
                table: "Expenses",
                column: "SubCategoryId",
                principalSchema: "dbo",
                principalTable: "ExpenseCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_MaintenanceFee_MaintenanceFeeCategories_CategoryId",
                schema: "dbo",
                table: "MaintenanceFee",
                column: "CategoryId",
                principalSchema: "dbo",
                principalTable: "MaintenanceFeeCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_MaintenanceFee_Properties_PropertyId",
                schema: "dbo",
                table: "MaintenanceFee",
                column: "PropertyId",
                principalSchema: "dbo",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ReceivedPayments_Buildings_BuildingId",
                schema: "dbo",
                table: "ReceivedPayments",
                column: "BuildingId",
                principalSchema: "dbo",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ReceivedPayments_Properties_PropertyId",
                schema: "dbo",
                table: "ReceivedPayments",
                column: "PropertyId",
                principalSchema: "dbo",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledExpense_Buildings_BuildingId",
                schema: "dbo",
                table: "ScheduledExpense",
                column: "BuildingId",
                principalSchema: "dbo",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledExpense_ExpenseCategory_CategoryId",
                schema: "dbo",
                table: "ScheduledExpense",
                column: "CategoryId",
                principalSchema: "dbo",
                principalTable: "ExpenseCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledExpense_ExpenseCategory_SubCategoryId",
                schema: "dbo",
                table: "ScheduledExpense",
                column: "SubCategoryId",
                principalSchema: "dbo",
                principalTable: "ExpenseCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledMaintenanceFees_MaintenanceFeeCategories_CategoryId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees",
                column: "CategoryId",
                principalSchema: "dbo",
                principalTable: "MaintenanceFeeCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledMaintenanceFees_Properties_PropertyId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees",
                column: "PropertyId",
                principalSchema: "dbo",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_SentPayments_Buildings_BuildingId",
                schema: "dbo",
                table: "SentPayments",
                column: "BuildingId",
                principalSchema: "dbo",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_SentPayments_Expenses_ExpenseId",
                schema: "dbo",
                table: "SentPayments",
                column: "ExpenseId",
                principalSchema: "dbo",
                principalTable: "Expenses",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Buildings_BuildingId",
                schema: "dbo",
                table: "Tickets",
                column: "BuildingId",
                principalSchema: "dbo",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Properties_PropertyId",
                schema: "dbo",
                table: "Tickets",
                column: "PropertyId",
                principalSchema: "dbo",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Buildings_BuildingId",
                schema: "dbo",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_ExpenseCategory_CategoryId",
                schema: "dbo",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_ExpenseCategory_SubCategoryId",
                schema: "dbo",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_MaintenanceFee_MaintenanceFeeCategories_CategoryId",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.DropForeignKey(
                name: "FK_MaintenanceFee_Properties_PropertyId",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.DropForeignKey(
                name: "FK_ReceivedPayments_Buildings_BuildingId",
                schema: "dbo",
                table: "ReceivedPayments");

            migrationBuilder.DropForeignKey(
                name: "FK_ReceivedPayments_Properties_PropertyId",
                schema: "dbo",
                table: "ReceivedPayments");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledExpense_Buildings_BuildingId",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledExpense_ExpenseCategory_CategoryId",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledExpense_ExpenseCategory_SubCategoryId",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledMaintenanceFees_MaintenanceFeeCategories_CategoryId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledMaintenanceFees_Properties_PropertyId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees");

            migrationBuilder.DropForeignKey(
                name: "FK_SentPayments_Buildings_BuildingId",
                schema: "dbo",
                table: "SentPayments");

            migrationBuilder.DropForeignKey(
                name: "FK_SentPayments_Expenses_ExpenseId",
                schema: "dbo",
                table: "SentPayments");

            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Buildings_BuildingId",
                schema: "dbo",
                table: "Tickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Properties_PropertyId",
                schema: "dbo",
                table: "Tickets");

            migrationBuilder.DropIndex(
                name: "IX_Tickets_BuildingId",
                schema: "dbo",
                table: "Tickets");

            migrationBuilder.DropIndex(
                name: "IX_Tickets_PropertyId",
                schema: "dbo",
                table: "Tickets");

            migrationBuilder.DropIndex(
                name: "IX_SentPayments_BuildingId",
                schema: "dbo",
                table: "SentPayments");

            migrationBuilder.DropIndex(
                name: "IX_SentPayments_ExpenseId",
                schema: "dbo",
                table: "SentPayments");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledMaintenanceFees_CategoryId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledMaintenanceFees_PropertyId",
                schema: "dbo",
                table: "ScheduledMaintenanceFees");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledExpense_BuildingId",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledExpense_CategoryId",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropIndex(
                name: "IX_ScheduledExpense_SubCategoryId",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropIndex(
                name: "IX_ReceivedPayments_BuildingId",
                schema: "dbo",
                table: "ReceivedPayments");

            migrationBuilder.DropIndex(
                name: "IX_ReceivedPayments_PropertyId",
                schema: "dbo",
                table: "ReceivedPayments");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceFee_CategoryId",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceFee_PropertyId",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_BuildingId",
                schema: "dbo",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_CategoryId",
                schema: "dbo",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_SubCategoryId",
                schema: "dbo",
                table: "Expenses");
        }
    }
}
