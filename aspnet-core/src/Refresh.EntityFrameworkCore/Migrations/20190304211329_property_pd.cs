﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class property_pd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Pd",
                schema: "dbo",
                table: "Properties",
                maxLength: 32,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pd",
                schema: "dbo",
                table: "Properties");
        }
    }
}
