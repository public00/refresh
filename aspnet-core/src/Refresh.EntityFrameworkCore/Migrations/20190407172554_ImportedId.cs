﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class ImportedId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImportedId",
                schema: "dbo",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ImportedId",
                schema: "dbo",
                table: "Buildings",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImportedId",
                schema: "dbo",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "ImportedId",
                schema: "dbo",
                table: "Buildings");
        }
    }
}
