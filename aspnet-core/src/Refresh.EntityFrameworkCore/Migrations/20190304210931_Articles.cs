﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class Articles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                schema: "dbo",
                table: "InvoiceSendingTrackers");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ContractStartDate",
                schema: "dbo",
                table: "Buildings",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 128, nullable: false),
                    Unit = table.Column<string>(maxLength: 64, nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Vat = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articles_Title",
                table: "Articles",
                column: "Title",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                schema: "dbo",
                table: "InvoiceSendingTrackers",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ContractStartDate",
                schema: "dbo",
                table: "Buildings",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
