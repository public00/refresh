﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class IsFinishedPayment_Expenses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFinishedPayment",
                schema: "dbo",
                table: "Expenses",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFinishedPayment",
                schema: "dbo",
                table: "Expenses");
        }
    }
}
