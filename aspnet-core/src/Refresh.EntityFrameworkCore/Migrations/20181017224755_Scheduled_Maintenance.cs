﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class Scheduled_Maintenance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SubCategory",
                schema: "dbo",
                table: "ScheduledExpense",
                newName: "SubCategoryId");

            migrationBuilder.RenameColumn(
                name: "Building",
                schema: "dbo",
                table: "Expenses",
                newName: "BuildingId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastRunDate",
                schema: "dbo",
                table: "ScheduledExpense",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "Months",
                schema: "dbo",
                table: "ScheduledExpense",
                maxLength: 32,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                schema: "dbo",
                table: "MaintenanceFee",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "MaintenanceFee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                schema: "dbo",
                table: "MaintenanceFee",
                maxLength: 128,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Months",
                schema: "dbo",
                table: "ScheduledExpense");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.DropColumn(
                name: "Description",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.DropColumn(
                name: "Title",
                schema: "dbo",
                table: "MaintenanceFee");

            migrationBuilder.RenameColumn(
                name: "SubCategoryId",
                schema: "dbo",
                table: "ScheduledExpense",
                newName: "SubCategory");

            migrationBuilder.RenameColumn(
                name: "BuildingId",
                schema: "dbo",
                table: "Expenses",
                newName: "Building");

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastRunDate",
                schema: "dbo",
                table: "ScheduledExpense",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
