﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class MaintenanceFeeMonths : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Month",
                schema: "Dbo",
                table: "MaintenancePaymentTrackers");

            migrationBuilder.DropColumn(
                name: "Year",
                schema: "Dbo",
                table: "MaintenancePaymentTrackers");

            migrationBuilder.AddColumn<decimal>(
                name: "MaintenanceFeeFixedPrice",
                schema: "dbo",
                table: "Properties",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaintenanceFeeMonths",
                schema: "dbo",
                table: "Properties",
                maxLength: 32,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaintenanceFeeFixedPrice",
                schema: "dbo",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "MaintenanceFeeMonths",
                schema: "dbo",
                table: "Properties");

            migrationBuilder.AddColumn<int>(
                name: "Month",
                schema: "Dbo",
                table: "MaintenancePaymentTrackers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Year",
                schema: "Dbo",
                table: "MaintenancePaymentTrackers",
                nullable: false,
                defaultValue: 0);
        }
    }
}
