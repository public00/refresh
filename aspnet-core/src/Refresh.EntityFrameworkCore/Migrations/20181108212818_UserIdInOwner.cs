﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class UserIdInOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                schema: "dbo",
                table: "Owners",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                schema: "dbo",
                table: "Owners",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
