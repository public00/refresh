﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class Buildings_ContractEndDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ContactEndDate",
                schema: "dbo",
                table: "Buildings",
                newName: "ContractEndDate");

            migrationBuilder.AlterColumn<int>(
                name: "PropertyId",
                schema: "dbo",
                table: "ReceivedPayments",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "BuildingId",
                schema: "dbo",
                table: "ReceivedPayments",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuildingId",
                schema: "dbo",
                table: "ReceivedPayments");

            migrationBuilder.RenameColumn(
                name: "ContractEndDate",
                schema: "dbo",
                table: "Buildings",
                newName: "ContactEndDate");

            migrationBuilder.AlterColumn<int>(
                name: "PropertyId",
                schema: "dbo",
                table: "ReceivedPayments",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
