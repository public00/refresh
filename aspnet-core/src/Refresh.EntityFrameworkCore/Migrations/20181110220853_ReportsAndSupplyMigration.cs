﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Refresh.Migrations
{
    public partial class ReportsAndSupplyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.AddColumn<int>(
                name: "BuildingId",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CityId",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "From",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Invoice",
                schema: "dbo",
                table: "Reports",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Janitor",
                schema: "dbo",
                table: "Reports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaintenanceType",
                schema: "dbo",
                table: "Reports",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NPrice",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                schema: "dbo",
                table: "Reports",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PPrice",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SpentResources",
                schema: "dbo",
                table: "Reports",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeSpent",
                schema: "dbo",
                table: "Reports",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "To",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "WorkType",
                schema: "dbo",
                table: "Reports",
                maxLength: 128,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Supplies",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    InvoiceNumber = table.Column<string>(nullable: true),
                    CityId = table.Column<int>(nullable: false),
                    ItemName = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    ItemPrice = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplies", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Supplies",
                schema: "dbo");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "CityId",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "From",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "Invoice",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "Janitor",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "MaintenanceType",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "NPrice",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "Note",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "PPrice",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "Quantity",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "SpentResources",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TimeSpent",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "To",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "WorkType",
                schema: "dbo",
                table: "Reports");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "Reports",
                nullable: false,
                defaultValue: "");
        }
    }
}
