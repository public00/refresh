﻿namespace Refresh.EntityFrameworkCore.Seed.MaintenanceFeeCategories
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities.Billing;
    using Microsoft.EntityFrameworkCore;

    public class MaintenanceFeeCategoriesBuilder
    {
        private readonly List<string> _categories = new List<string>
        {
            "Akontacija na ime troškova održavanja",
            "Zakup poslovnog prostora",
            "Zakup za baštu",
            "Reklama",
            "Ostalo"
        };

        private readonly RefreshDbContext _context;

        public MaintenanceFeeCategoriesBuilder(RefreshDbContext context)
        {
            _context = context;
        }
        
        public void Create()
        {
            CreateMaintenanceFeeCategories();
        }

        private void CreateMaintenanceFeeCategories()
        {
            foreach (var categoryName in _categories)
            {
                var category = _context.MaintenanceFeeCategories.IgnoreQueryFilters().FirstOrDefault(c => c.Name == categoryName);
                if (category != null)
                {
                    continue;
                }

                category = new MaintenanceFeeCategory { Name = categoryName };
                _context.MaintenanceFeeCategories.Add(category);
                _context.SaveChanges();
            }
        }
    }
}
