﻿namespace Refresh.EntityFrameworkCore.Seed.PropertyTypes
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities.Properties;
    using Microsoft.EntityFrameworkCore;

    public class PropertyTypesBuilder
    {
        private readonly RefreshDbContext _context;

        private readonly List<string> _propertyTypeNames = new List<string> { "Stan", "Garaža", "Poslovni prostor", "Nestambeni prostor" };

        public PropertyTypesBuilder(RefreshDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreatePropertyTypes();
        }

        private void CreatePropertyTypes()
        {
            foreach (var propertyTypeName in _propertyTypeNames)
            {
                var propertyType = _context.PropertyTypes.IgnoreQueryFilters().FirstOrDefault(c => c.Name == propertyTypeName);
                if (propertyType != null)
                {
                    continue;
                }

                propertyType = new PropertyType { Name = propertyTypeName };
                _context.PropertyTypes.Add(propertyType);
                _context.SaveChanges();
            }
        }
    }
}
