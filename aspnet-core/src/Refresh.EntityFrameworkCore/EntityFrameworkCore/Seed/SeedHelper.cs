﻿using System;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Refresh.EntityFrameworkCore.Seed.Host;
using Refresh.EntityFrameworkCore.Seed.Tenants;

namespace Refresh.EntityFrameworkCore.Seed
{
    using Cities;
    using ExpenseCategories;
    using MaintenanceFeeCategories;
    using PropertyTypes;

    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<RefreshDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(RefreshDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            // Host seed
            new InitialHostDbBuilder(context).Create();

            // Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();

            // Seed cities, property types, expense and maintenance fee categories
            new CitiesBuilder(context).Create();
            new ExpenseCategoriesBuilder(context).Create();
            new PropertyTypesBuilder(context).Create();
            new MaintenanceFeeCategoriesBuilder(context).Create();
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

                    contextAction(context);

                    uow.Complete();
                }
            }
        }
    }
}
