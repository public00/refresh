﻿namespace Refresh.EntityFrameworkCore.Seed.ExpenseCategories
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities.Billing;
    using Microsoft.EntityFrameworkCore;

    public class ExpenseCategoriesBuilder
    {
        private readonly RefreshDbContext _context;

        private readonly List<string> _rootCategories = new List<string>
        {
            "Radovi redovnog održavanja",
            "Nužni radovi",
            "Hitni radov"
        };

        /// <summary>
        /// Potkategorije radova redovnog odrzavanja
        /// </summary>
        private readonly List<string> _firstChildCategories = new List<string>
        {
            "Krečenje ili bojanje unutrašnjih zidova",
            "Bojanje spoljne i unutrašnje stolarije i tapetarski radovi",
            "Farbanje bravarije, radijatora, drugih grejnih tijela i drugih odgovarajućih elemenata u zgradi",
            "Keramički i drugi radovi na završnim oblogama podova i zidova",
            "Zamjena podnih obloga i premazivanje podova",
            "Bojanje fasade",
            "Zamjena i popravke stolarije, uključujući elemente zaštite od spoljnih uticaja (kapci, žaluzine grilje i sl.)",
            "Opravke ravnih i kosih krovova",
            "Održavanje rasvjete i drugih električnih uređaja (zamjena sijalica, prekidača, utičnica, zvonca, svjetiljki, interfona i sl.), kao i održavanje spoljne rasvjete koja pripada zgradi",
            "Zamjena i popravke brava i drugih elemenata koji pripadaju zgradi",
            "Redovni servisi na uređajima za grijanje i pripremu tople vode (kotlarnice i toplane)",
            "Redovni servisi zajedničkih sistema za klimatizaciju",
            "Redovni servisi liftova",
            "Redovni servisi protivpožarne instalacije i protivpožarnih aparata u zgradi",
            "Redovni servisi agregata za rasvjetu, hidroforskih postrojenja i pumpnih stanica za vodu i otpadnu vodu",
            "Redovni servisi na antenskim uređajima, uređajima za prijem televizijskog programa, uključujući uređaje za kablovsku i satelitsku TV",
            "Redovni servisi na instalacijama vodovoda, kanalizacije, elektrike, plina i dr.",
            "Redovni servisi ostalih aparata i uređaja u zgradi prema uputstvu proizvođača",
            "Čišćenje dimnjaka i dimnjačkih kanala (dimničarske usluge), dezinsekcija i deratizacija zajedničkih prostora zgrade i posebnih djelova zgrade kada se obavlja u cijeloj zgradi u cilju trajnog otklanjanja štetočina i gamadi",
            "Čišćenje kanala za smeće",
            "Čišćenje odvodnih rešetaka, slivnika i oluka",
            "Održavanje higijene u zajedničkim djelovima stambene zgrade",
            "Ostali radovi redovnog održavanja"
        };

        /// <summary>
        /// Potkategorije nuznih radova
        /// </summary>
        private readonly List<string> _secondChildCategories = new List<string>
        {
            "Rekonstrukcija krovne konstrukcije, nosivih zidova, stubova, međuspratnih konstrukcija, temelja",
            "Radovi na dimnjacima i dimovodnim kanalima",
            "Radovi na ravnim i kosim krovovima",
            "Zamjena instalacija na zajedničkim djelovima i uređajima zgrade (vodovodne, kanalizacione, električne, plinske, centralnog grijanja i sl.)",
            "Radovi na fasadama, odnosno radovi na spoljašnjem izgledu stambene zgrade",
            "Izolaciji zidova, podova i temelja zgrade",
        };

        /// <summary>
        /// Potkategorije hitnih radova
        /// </summary>
        private readonly List<string> _thirdChildCategories = new List<string>
        {
            "Radovi na plinskim instalacijama",
            "Radovi na sistemu centralnog grijanja i toplovodnog sistema",
            "Radovi na sistemu za klimatizaciju",
            "Radovi u slučaju napuknuća, oštećenja i začepljenja vodovodne i kanalizacione instalacije radi sprječavanja daljih štetnih posljedica",
            "Radovi na fasadama, odnosno radovi na spoljašnjem izgledu stambene zgrade",
            "Radovi na električnoj instalaciji",
            "Radovi kod znatnih oštećenja dimnjaka i dimnjačkih kanala",
            "Radovi na popravci oštećenih djelova krova",
            "Radovi na liftu",
            "Radovi na fasadi",
            "Radovi radi očuvanja statičke stabilnosti zgrade ili pojedinih djelova zgrade",
            "Radovi na sanaciji klizišta"
        };

        public ExpenseCategoriesBuilder(RefreshDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateExpenseCategories();
        }

        private void CreateExpenseCategories()
        {
            foreach (var rootCategory in _rootCategories)
            {
                var category = _context.ExpenseCategories.IgnoreQueryFilters().FirstOrDefault(x => x.Name == rootCategory);
                if (category != null)
                {
                    continue;
                }

                category = new ExpenseCategory { Name = rootCategory };
                _context.ExpenseCategories.Add(category);
                _context.SaveChanges();
            }

            // Radovi redovnog odrzavanja
            var firstParentCategory = _context.ExpenseCategories.IgnoreQueryFilters().First(x => x.Name == _rootCategories[0]);
            foreach (var childCategory in _firstChildCategories)
            {
                var category = _context.ExpenseCategories.IgnoreQueryFilters().FirstOrDefault(x => x.Name == childCategory);
                if (category != null)
                {
                    continue;
                }

                category = new ExpenseCategory { Name = childCategory, ParentId = firstParentCategory.Id };
                _context.ExpenseCategories.Add(category);
                _context.SaveChanges();
            }

            // Nuzni radovi
            var secondParentCategory = _context.ExpenseCategories.IgnoreQueryFilters().First(x => x.Name == _rootCategories[1]);
            foreach (var childCategory in _secondChildCategories)
            {
                var category = _context.ExpenseCategories.IgnoreQueryFilters().FirstOrDefault(x => x.Name == childCategory);
                if (category != null)
                {
                    continue;
                }

                category = new ExpenseCategory { Name = childCategory, ParentId = secondParentCategory.Id };
                _context.ExpenseCategories.Add(category);
                _context.SaveChanges();
            }

            // Hitni radovi
            var thirdParentCategory = _context.ExpenseCategories.IgnoreQueryFilters().First(x => x.Name == _rootCategories[2]);
            foreach (var childCategory in _thirdChildCategories)
            {
                var category = _context.ExpenseCategories.IgnoreQueryFilters().FirstOrDefault(x => x.Name == childCategory);
                if (category != null)
                {
                    continue;
                }

                category = new ExpenseCategory { Name = childCategory, ParentId = thirdParentCategory.Id };
                _context.ExpenseCategories.Add(category);
                _context.SaveChanges();
            }
        }
    }
}
