﻿namespace Refresh.EntityFrameworkCore.Seed.Cities
{
    using System.Collections.Generic;
    using System.Linq;
    using Entities.Cities;
    using Microsoft.EntityFrameworkCore;

    public class CitiesBuilder
    {
        private readonly RefreshDbContext _context;

        private readonly List<string> _cityNames = new List<string> { "Podgorica", "Herceg Novi", "Budva", "Bar", "Kolašin" };

        public CitiesBuilder(RefreshDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateCities();
        }

        private void CreateCities()
        {
            foreach (var cityName in _cityNames)
            {
                var city = _context.Cities.IgnoreQueryFilters().FirstOrDefault(c => c.Name == cityName);
                if (city != null)
                {
                    continue;
                }

                city = new City { Name = cityName };
                _context.Cities.Add(city);
                _context.SaveChanges();
            }
        }
    }
}
