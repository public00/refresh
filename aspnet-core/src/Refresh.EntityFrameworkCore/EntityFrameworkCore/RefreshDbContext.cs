﻿namespace Refresh.EntityFrameworkCore
{
    using Microsoft.EntityFrameworkCore;
    using Abp.Zero.EntityFrameworkCore;
    using Authorization.Roles;
    using Authorization.Users;
    using Entities.Cities;
    using MultiTenancy;
    using Entities.Owners;
    using Entities.Properties;
    using Entities.Buildings;
    using Entities.Tickets;
    using Entities.Billing;
    using Entities.Announcements;
    using Entities.Billing.Invoices;
    using Entities.Reports;
    using Entities.InvoiceSendingTrackers;

    public class RefreshDbContext : AbpZeroDbContext<Tenant, Role, User, RefreshDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public DbSet<City> Cities { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyType> PropertyTypes { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketComment> TicketComments { get; set; }
        public DbSet<ExpenseCategory> ExpenseCategories { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<ScheduledExpense> ScheduledExpenses { get; set; }
        public DbSet<MaintenanceFee> MaintenanceFees { get; set; }
        public DbSet<MaintenancePaymentTracker> MaintenancePaymentTrackers { get; set; }
        public DbSet<ReceivedPayment> ReceivedPayments { get; set; }
        public DbSet<SentPayment> SentPayments { get; set; } 
        public DbSet<PropertyOwner> PropertyOwners { get; set; }
        public DbSet<Inventory> Inventory { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<ScheduledMaintenanceFee> ScheduledMaintenanceFees { get; set; }
        public DbSet<MaintenanceFeeCategory> MaintenanceFeeCategories { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Supply> Supplies { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceItem> InvoiceItems { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<PropertyLegalStatus> PropertyLegalStatuses { get; set; }
        public DbSet<InvoiceSendingTracker> InvoiceSendingTrackers { get; set; }

        public RefreshDbContext(DbContextOptions<RefreshDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<City>().HasIndex(x => x.Name).IsUnique();
            builder.Entity<Owner>();
            builder.Entity<Property>();
            builder.Entity<PropertyType>().HasIndex(x => x.Name).IsUnique();
            builder.Entity<Building>().HasIndex(x => x.PIB).IsUnique();
            builder.Entity<Ticket>();
            builder.Entity<TicketComment>();
            builder.Entity<ExpenseCategory>();
            builder.Entity<Expense>();
            builder.Entity<ScheduledExpense>();
            builder.Entity<MaintenanceFee>();
            builder.Entity<MaintenancePaymentTracker>();
            builder.Entity<ReceivedPayment>();
            builder.Entity<SentPayment>();
            builder.Entity<PropertyOwner>();
            builder.Entity<Inventory>();
            builder.Entity<Announcement>();
            builder.Entity<ScheduledMaintenanceFee>();
            builder.Entity<MaintenanceFeeCategory>();
            builder.Entity<Report>();
            builder.Entity<Supply>();
            builder.Entity<Invoice>();
            builder.Entity<InvoiceItem>();
            builder.Entity<Article>().HasIndex(x => x.Title).IsUnique();
            builder.Entity<InvoiceSendingTracker>();
            builder.Entity<PropertyLegalStatus>().HasIndex(x => x.PropertyId).IsUnique();

            // register views below
            builder.Query<BuildingView>().ToView("BuildingView");
            builder.Query<PropertyOwnerView>().ToView("PropertyOwnerView");
            builder.Query<OwnerView>().ToView("OwnerView");
            builder.Query<ReportView>().ToView("ReportView");
            builder.Query<SupplyView>().ToView("SupplyView");
            builder.Query<InvoiceView>().ToView("InvoiceView");
            builder.Query<BalanceView>().ToView("BalanceView");
            builder.Query<TicketView>().ToView("TicketView");
            builder.Query<PropertyDebtView>().ToView("PropertyDebtView");
            builder.Query<PropertyPickerView>().ToView("PropertyPickerView");
        }
    }
}
