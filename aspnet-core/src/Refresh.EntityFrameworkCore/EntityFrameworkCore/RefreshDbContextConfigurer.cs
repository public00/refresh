using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Refresh.EntityFrameworkCore
{
    public static class RefreshDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<RefreshDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<RefreshDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
