﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Refresh.Configuration;
using Refresh.Web;

namespace Refresh.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class RefreshDbContextFactory : IDesignTimeDbContextFactory<RefreshDbContext>
    {
        public RefreshDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<RefreshDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            RefreshDbContextConfigurer.Configure(builder, configuration.GetConnectionString(RefreshConsts.ConnectionStringName));

            return new RefreshDbContext(builder.Options);
        }
    }
}
