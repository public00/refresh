﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Properties;
    using Refresh.Views;

    public class PropertyPickerViewQuery : DbQueryBase<PropertyPickerView>, IPropertyPickerViewQuery
    {
        public PropertyPickerViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
