﻿namespace Refresh.EntityFrameworkCore.Views
{
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Refresh.Views;

    public class DbQueryBase<TView> : IDbQueryBase<TView> where TView : class, IEntityDto
    {
        private readonly IDbContextProvider<RefreshDbContext> _contextProvider;

        private RefreshDbContext Context => _contextProvider.GetDbContext();

        public DbQueryBase(IDbContextProvider<RefreshDbContext> contextProvider)
        {
            _contextProvider = contextProvider;
        }

        public IQueryable<TView> GetQueryable()
        {
            var data = Context.Query<TView>().AsQueryable();
            return data;
        }

        public IQueryable<TView> GetDataQuery(QueryInfo queryInfo)
        {
            var data = Context.Query<TView>().AsQueryable();
            return queryInfo.ApplyDataQuery(data);
        }

        public IQueryable<TView> GetCountQuery(QueryInfo queryInfo)
        {
            var data = Context.Query<TView>().AsQueryable();
            return queryInfo.ApplyCountQuery(data);
        }

        public async Task<TView> Get(int id)
        {
            return await Context.Query<TView>().FirstAsync(x => x.Id == id);
        }
    }
}
