﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Properties;
    using Refresh.Views;

    public class PropertyOwnerViewQuery : DbQueryBase<PropertyOwnerView>, IPropertyOwnerViewQuery
    {
        public PropertyOwnerViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
