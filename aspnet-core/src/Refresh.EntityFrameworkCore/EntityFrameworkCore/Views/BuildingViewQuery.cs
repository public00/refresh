﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Buildings;
    using Refresh.Views;

    public class BuildingViewQuery : DbQueryBase<BuildingView>, IBuildingViewQuery
    {
        public BuildingViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
