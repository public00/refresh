﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Billing;
    using Refresh.Views;

    public class BalanceViewQuery : DbQueryBase<BalanceView>, IBalanceViewQuery
    {
        public BalanceViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
