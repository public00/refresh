﻿using Abp.EntityFrameworkCore;
using Refresh.Entities.Reports;
using Refresh.Views;

namespace Refresh.EntityFrameworkCore.Views
{
    public class ReportViewQuery : DbQueryBase<ReportView>, IReportViewQuery
    {
        public ReportViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
