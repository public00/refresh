﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Properties;
    using Refresh.Views;

    public class PropertyDebtViewQuery : DbQueryBase<PropertyDebtView>, IPropertyDebtViewQuery
    {
        public PropertyDebtViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
