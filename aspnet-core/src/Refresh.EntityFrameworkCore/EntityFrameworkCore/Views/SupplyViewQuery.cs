﻿using Abp.EntityFrameworkCore;
using Refresh.Entities.Reports;
using Refresh.Views;

namespace Refresh.EntityFrameworkCore.Views
{
    public class SupplyViewQuery : DbQueryBase<SupplyView>, ISupplyViewQuery
    {
        public SupplyViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
