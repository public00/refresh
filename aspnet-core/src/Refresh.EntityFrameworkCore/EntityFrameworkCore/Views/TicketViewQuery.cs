﻿using Abp.EntityFrameworkCore;
using Refresh.Entities.Tickets;
using Refresh.Views;

namespace Refresh.EntityFrameworkCore.Views
{
    public class TicketViewQuery : DbQueryBase<TicketView>, ITicketViewQuery
    {
        public TicketViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        { 
        }
    }
}
