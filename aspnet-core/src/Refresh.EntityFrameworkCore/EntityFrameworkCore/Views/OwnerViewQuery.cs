﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Owners;
    using Refresh.Views;

    public class OwnerViewQuery : DbQueryBase<OwnerView>, IOwnerViewQuery
    {
        public OwnerViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
