﻿namespace Refresh.EntityFrameworkCore.Views
{
    using Abp.EntityFrameworkCore;
    using Entities.Billing.Invoices;
    using Refresh.Views;

    public class InvoiceViewQuery : DbQueryBase<InvoiceView>, IInvoiceViewQuery
    {
        public InvoiceViewQuery(IDbContextProvider<RefreshDbContext> contextProvider) : base(contextProvider)
        {
        }
    }
}
