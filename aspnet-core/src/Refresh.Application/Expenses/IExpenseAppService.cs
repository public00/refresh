﻿namespace Refresh.Expenses
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IExpenseAppService :  IAsyncCrudAppService<ExpenseDto, int, PagedResultRequestDto, CreateExpenseDto, UpdateExpenseDto>
    {
    }
}
