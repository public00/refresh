﻿namespace Refresh.Expenses.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(Expense))]
    public class CreateExpenseDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public int BuildingId { get; set; }

        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }

        [StringLength(128)]
        public string Company { get; set; }

        [StringLength(128)]
        public string InvoiceNumber { get; set; }
    }
}
