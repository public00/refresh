﻿namespace Refresh.Expenses.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Buildings.Dto;
    using Entities.Billing;
    using ExpenseCategories.Dto;

    [AutoMap(typeof(Expense))]
    public class ExpenseDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public int BuildingId { get; set; }

        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }

        [StringLength(128)]
        public string Company { get; set; }

        [StringLength(128)]
        public string InvoiceNumber { get; set; }

        public bool IsFinishedPayment { get; set; }

        public virtual BuildingDto Building { get; set; }

        public virtual ExpenseCategoryDto Category { get; set; }

        public virtual ExpenseCategoryDto SubCategory { get; set; }
    }
}
