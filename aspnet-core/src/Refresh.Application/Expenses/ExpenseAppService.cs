﻿namespace Refresh.Expenses
{
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Entities.Billing;
    using Dto;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Services.Export;

    [AbpAuthorize]
    public class ExpenseAppService : RefreshCrudAppService<Expense, ExpenseDto, int, PagedResultRequestDto, CreateExpenseDto, UpdateExpenseDto>, IExpenseAppService
    {
        public ExpenseAppService(IRepository<Expense, int> repository, IExportService exportService, IRepository<User, long> userRepository, ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
        }

        [HttpGet]
        public override async Task<ExpenseDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Building, x => x.Category, x => x.SubCategory).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(Expense), input.Id);
            }

            return MapToEntityDto(building);
        }
    }
}
