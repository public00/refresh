﻿namespace Refresh.PropertyOwners
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Properties;
    using Entities.Buildings;
    using Entities.Owners;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Services.Export;
    using Views;

    [AbpAuthorize]
    public class PropertyOwnerAppService : RefreshCrudAppService<PropertyOwner, PropertyOwnerDto, int, PagedResultRequestDto, CreatePropertyOwnerDto, PropertyOwnerDto>, IPropertyOwnerAppService
    {
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<Owner> _ownersRepository;
        private readonly IPropertyOwnerViewQuery _propertyOwnerViewQuery;

        public PropertyOwnerAppService(
            IRepository<PropertyOwner> repository, 
            IExportService exportService, 
            IRepository<Building> buildingRepository, 
            IRepository<Property> propertyRepository, 
            IRepository<User, long> userRepository, 
            IRepository<Owner> ownersRepository, 
            IPropertyOwnerViewQuery propertyOwnerViewQuery,
            ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
            _buildingRepository = buildingRepository;
            _propertyRepository = propertyRepository;
            _ownersRepository = ownersRepository;
            _propertyOwnerViewQuery = propertyOwnerViewQuery;
        }

        [HttpGet]
        public override async Task<PropertyOwnerDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Owner, x => x.Property).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(PropertyOwner), input.Id);
            }

            return MapToEntityDto(building);
        }

        public List<PropertyOwnerViewDto> GetPropertyOwnersViews(int buildingId)
        {
            var list = _propertyRepository.GetAll().Where(x => x.BuildingId == buildingId).ToList();
            var building = _buildingRepository.Get(buildingId);
            List<PropertyOwnerViewDto> model = new List<PropertyOwnerViewDto>();
            foreach (var property in list)
            {
                var owner = Repository.GetAllIncluding(x => x.Owner).FirstOrDefault(x => x.PropertyId == property.Id);
                var pricePerSquareMeter = owner?.Property.PricePerSquareMeter ?? building.PricePerSquareMeter;
                var modelDto = new PropertyOwnerViewDto
                {
                    PhoneNumber = owner?.Owner.PhoneNumber,
                    PricePerSquareMeter = pricePerSquareMeter,
                    Quadrature = property.Quadrature,
                    UserId = owner?.OwnerId ?? 0,
                    UserName = owner?.Owner.FirstName + " " + owner?.Owner.LastName,
                    Rate = property.Quadrature * pricePerSquareMeter
                };
                model.Add(modelDto);
            }

            return model;
        }

        [HttpPost]
        public PagedResultDto<PropertyOwnerView> GetGridData(QueryInfo queryInfo)
        {

            if (queryInfo == null)
            {
                return new PagedResultDto<PropertyOwnerView>();
            }
            try
            {
                var result = new PagedResultDto<PropertyOwnerView>();

                var dataQuery = _propertyOwnerViewQuery.GetDataQuery(queryInfo);
                var countQuery = _propertyOwnerViewQuery.GetCountQuery(queryInfo);

                result.Items = dataQuery.ToList();
                result.TotalCount = countQuery.Count();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpPost]
        public override async Task<PropertyOwnerDto> Create(CreatePropertyOwnerDto input)
        {
            try
            {
                var property = await _propertyRepository.GetAsync(input.PropertyId);
                var owner = await _ownersRepository.GetAsync(input.OwnerId);
                var propertyOwner =
                    await Repository.FirstOrDefaultAsync(
                        x => x.PropertyId == property.Id && x.OwnerId == owner.Id);
                if (propertyOwner != null)
                {
                    throw new Exception($"User is already owner of property ID:{input.PropertyId}");
                }

                return await base.Create(input);
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpGet]
        public async Task<bool> IsPropertyOwner(int userId, int propertyId)
        {
            var owner = await _ownersRepository.FirstOrDefaultAsync(x => x.UserId == userId);
            var proerOwner = await Repository.FirstOrDefaultAsync(x => x.OwnerId == owner.Id && x.PropertyId == propertyId);
            return proerOwner != null;
        }
    }
}
