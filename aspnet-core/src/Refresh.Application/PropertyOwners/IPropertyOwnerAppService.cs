﻿namespace Refresh.PropertyOwners
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IPropertyOwnerAppService : IAsyncCrudAppService<PropertyOwnerDto, int, PagedResultRequestDto, CreatePropertyOwnerDto, PropertyOwnerDto>
    {
    }
}
