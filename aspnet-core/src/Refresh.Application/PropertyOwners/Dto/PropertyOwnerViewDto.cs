﻿namespace Refresh.PropertyOwners.Dto
{
    public class PropertyOwnerViewDto
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string PhoneNumber { get; set; }
        
        //Properties {Kvadratura}
        public int Quadrature { get; set; }
        
        public decimal PricePerSquareMeter { get; set; }

        public decimal Rate { get; set; }
    }
}
