﻿namespace Refresh.PropertyOwners.Dto
{
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Properties;
    using Owners.Dto;
    using PropertiesService.Dto;

    [AutoMap(typeof(PropertyOwner))]
    public class PropertyOwnerDto : FullAuditedEntityDto
    {
        public int OwnerId { get; set; }
        
        public int PropertyId { get; set; }

        public virtual OwnerDto Owner { get; set; }

        public virtual PropertyDto Property { get; set; }
    }
}
