﻿namespace Refresh.PropertyOwners.Dto
{
    using Abp.AutoMapper;
    using Entities.Properties;

    [AutoMapTo(typeof(PropertyOwner))]
    public class CreatePropertyOwnerDto
    {
        public int OwnerId { get; set; }

        public int PropertyId { get; set; }
    }
}
