﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Refresh.Authorization.Accounts.Dto;

namespace Refresh.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
