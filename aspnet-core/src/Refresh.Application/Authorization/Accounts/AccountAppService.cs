using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Zero.Configuration;
using Refresh.Authorization.Accounts.Dto;
using Refresh.Authorization.Users;

namespace Refresh.Authorization.Accounts
{
    using System;
    using Abp.AutoMapper;
    using Abp.Domain.Repositories;
    using Entities.Owners;
    using Owners.Dto;

    public class AccountAppService : RefreshAppServiceBase, IAccountAppService
    {
        private readonly UserRegistrationManager _userRegistrationManager;

        private readonly IRepository<Owner> _ownersRepository;

        public AccountAppService(
            UserRegistrationManager userRegistrationManager, 
            IRepository<Owner> ownersRepository)
        {
            _userRegistrationManager = userRegistrationManager;
            _ownersRepository = ownersRepository;
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        public async Task<OwnerDto> RegistrationCheck(string jmbg)
        {
            var user = await _ownersRepository.FirstOrDefaultAsync(x =>
                string.Equals(x.JMBG, jmbg, StringComparison.CurrentCultureIgnoreCase) 
                && !x.IsDeleted);
            return user.MapTo<OwnerDto>();
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            var user = await _userRegistrationManager.RegisterAsync(
                input.Name,
                input.Surname,
                input.EmailAddress,
                input.UserName,
                input.Password,
                true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
            );

            var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);
            var registerOutput = new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };

            if (registerOutput.CanLogin)
            {
                var owner = await _ownersRepository.GetAsync(input.OwnerId);
                owner.UserId = user.Id;
            }

            return registerOutput;
        }
    }
}
