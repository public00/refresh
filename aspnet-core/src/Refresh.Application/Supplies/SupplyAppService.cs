﻿namespace Refresh.Supplies
{
    using Abp.Application.Services.Dto;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Dto;
    using Entities.Reports;
    using Microsoft.AspNetCore.Mvc;
    using Refresh.QueryBuilder;
    using Refresh.Views;
    using Services.Export;
    using System;
    using System.Linq;
    using Castle.Core.Logging;

    public class SupplyAppService : RefreshCrudAppService<Supply, SupplyDto, int, PagedResultRequestDto, CreateSupplyDto, UpdateSupplyDto>, ISupplyAppService
    {
        private readonly ISupplyViewQuery _supplyViewQuery;

        public SupplyAppService(
            IRepository<Supply, int> repository, 
            ISupplyViewQuery supplyViewQuery, 
            IExportService exportService, 
            IRepository<User, long> userRepository,
            ILogger logger) 
            : base(repository, exportService, userRepository, logger) 
        {
            _supplyViewQuery = supplyViewQuery;
        }

        [HttpPost]
        public PagedResultDto<SupplyView> GetGridData(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<SupplyView>();
            }
            try
            {
                var result = new PagedResultDto<SupplyView>();

                var dataQuery = _supplyViewQuery.GetDataQuery(queryInfo);
                var countQuery = _supplyViewQuery.GetCountQuery(queryInfo);

                result.Items = dataQuery.ToList();
                result.TotalCount = countQuery.Count();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

    }
}
