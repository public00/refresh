﻿using Abp.AutoMapper;
using Refresh.Entities.Reports;
using System;

namespace Refresh.Supplies.Dto
{
    using System.ComponentModel.DataAnnotations;

    [AutoMapTo(typeof(Supply))]
    public class CreateSupplyDto
    {
        public DateTime Date { get; set; }

        [Required]
        [StringLength(64)]
        public string InvoiceNumber { get; set; }

        public int? CityId { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        public int Quantity { get; set; }

        public decimal ItemPrice { get; set; }

        [StringLength(512)]
        public string Note { get; set; }
    }
}
