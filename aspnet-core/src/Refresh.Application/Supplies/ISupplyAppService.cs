﻿namespace Refresh.Supplies
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Refresh.Supplies.Dto;

    public interface ISupplyAppService : IAsyncCrudAppService<SupplyDto, int, PagedResultRequestDto, CreateSupplyDto, UpdateSupplyDto>
    {
    }
}
