﻿namespace Refresh.ExpenseCategories
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IExpenseCategoryAppService : IAsyncCrudAppService<ExpenseCategoryDto, int, PagedResultRequestDto, CreateExpenseCategoryDto, UpdateExpenseCategoryDto>
    {
    }
}
