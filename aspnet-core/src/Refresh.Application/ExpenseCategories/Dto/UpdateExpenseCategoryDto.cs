﻿namespace Refresh.ExpenseCategories.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(ExpenseCategory))]
    public class UpdateExpenseCategoryDto : EntityDto
    {
        public int? ParentId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
