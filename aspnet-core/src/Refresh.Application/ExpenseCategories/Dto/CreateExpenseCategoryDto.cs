﻿namespace Refresh.ExpenseCategories.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(ExpenseCategory))]
    public class CreateExpenseCategoryDto
    {
        public int? ParentId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
