﻿namespace Refresh.ExpenseCategories.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMap(typeof(ExpenseCategory))]
    public class ExpenseCategoryDto : FullAuditedEntityDto
    {
        public int? ParentId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
