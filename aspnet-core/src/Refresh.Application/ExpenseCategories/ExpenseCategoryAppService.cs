﻿namespace Refresh.ExpenseCategories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Services.Export;

    [AbpAuthorize]
    public class ExpenseCategoryAppService : RefreshCrudAppService<ExpenseCategory, ExpenseCategoryDto, int, PagedResultRequestDto, CreateExpenseCategoryDto, UpdateExpenseCategoryDto>, IExpenseCategoryAppService
    {
        public ExpenseCategoryAppService(IRepository<ExpenseCategory, int> repository, IExportService exportService, IRepository<User, long> userRepository, ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
        }

        public async Task<IList<ExpenseCategoryDto>> GetRootCategories()
        {
            var categories = await Repository.GetAllListAsync(x => x.ParentId == null);
            return categories.Select(MapToEntityDto).ToList();
        }

        public async Task<IList<ExpenseCategoryDto>> GetChildren(int parentId)
        {
            var categories = await Repository.GetAllListAsync(x => x.ParentId == parentId);
            return categories.Select(MapToEntityDto).ToList();
        }
    }
}
