﻿namespace Refresh.MaintenanceFees
{
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Services.Export;

    [AbpAuthorize]
    public class MaintenanceFeeCategoryAppService : RefreshCrudAppService<MaintenanceFeeCategory, MaintenanceFeeCategoryDto, int, PagedResultRequestDto, CreateMaintenanceFeeCategoryDto, UpdateMaintenanceFeeCategoryDto>, IMaintenanceFeeCategoryAppService
    {
        public MaintenanceFeeCategoryAppService(IRepository<MaintenanceFeeCategory, int> repository, IExportService exportService, IRepository<User, long> userRepository, ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
        }
    }
}
