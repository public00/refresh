﻿namespace Refresh.MaintenanceFees
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IMaintenanceFeeAppService : IAsyncCrudAppService<MaintenanceFeeDto, int, PagedResultRequestDto, CreateMaintenanceFeeDto, UpdateMaintenanceFeeDto>
    {
    }
}
