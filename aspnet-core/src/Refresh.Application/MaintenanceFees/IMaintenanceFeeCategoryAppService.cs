﻿namespace Refresh.MaintenanceFees
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IMaintenanceFeeCategoryAppService : IAsyncCrudAppService<MaintenanceFeeCategoryDto, int, PagedResultRequestDto, CreateMaintenanceFeeCategoryDto, UpdateMaintenanceFeeCategoryDto>
    {
    }
}
