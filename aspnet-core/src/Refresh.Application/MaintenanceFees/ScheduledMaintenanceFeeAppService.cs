﻿namespace Refresh.MaintenanceFees
{
    using System;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Billing;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Entities.Properties;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Services.Export;

    [AbpAuthorize]
    public class ScheduledMaintenanceFeeAppService : RefreshCrudAppService<ScheduledMaintenanceFee, ScheduledMaintenanceFeeDto, int, PagedResultRequestDto, CreateScheduledMaintenanceFeeDto, UpdateScheduledMaintenanceFeeDto>, IScheduledMaintenanceFeeAppService
    {
        private readonly BillGenerator _billGenerator;
        private readonly IRepository<Property> _propertyRepository;

        public ScheduledMaintenanceFeeAppService(
            IRepository<ScheduledMaintenanceFee, int> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger, 
            IRepository<Property> propertyRepository, 
            BillGenerator billGenerator) 
            : base(repository, exportService, userRepository, logger)
        {
            _propertyRepository = propertyRepository;
            _billGenerator = billGenerator;
        }

        [HttpGet]
        public override async Task<ScheduledMaintenanceFeeDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Property, x => x.Category).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(ScheduledMaintenanceFee), input.Id);
            }

            return MapToEntityDto(building);
        }

        public override async Task<ScheduledMaintenanceFeeDto> Create(CreateScheduledMaintenanceFeeDto input)
        {
            var fee = await base.Create(input);
            if (!fee.GetScheduledMonths().Contains(DateTime.UtcNow.Month))
            {
                return fee;
            }

            var property = await _propertyRepository.GetAsync(fee.PropertyId);
            await _billGenerator.GenerateScheduledFeesProperty(property);
            return fee;
        }
    }
}
