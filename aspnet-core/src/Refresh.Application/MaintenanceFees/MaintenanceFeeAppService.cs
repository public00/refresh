﻿namespace Refresh.MaintenanceFees
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Billing;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Entities.Buildings;
    using Entities.Properties;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Services.Export;

    [AbpAuthorize]
    public class MaintenanceFeeAppService : RefreshCrudAppService<MaintenanceFee, MaintenanceFeeDto, int, PagedResultRequestDto, CreateMaintenanceFeeDto, UpdateMaintenanceFeeDto>, IMaintenanceFeeAppService
    {
        private readonly PaymentManager _paymentManager;
        private readonly BillGenerator _billGenerator;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<Property> _propertyRepository;

        public MaintenanceFeeAppService(
            IRepository<MaintenanceFee, int> repository,
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger, 
            PaymentManager paymentManager, 
            BillGenerator billGenerator, 
            IRepository<Building> buildingRepository, 
            IRepository<Property> propertyRepository)
            : base(repository, exportService, userRepository, logger)
        {
            _paymentManager = paymentManager;
            _billGenerator = billGenerator;
            _buildingRepository = buildingRepository;
            _propertyRepository = propertyRepository;
        }

        [HttpGet]
        public override async Task<MaintenanceFeeDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Property, x => x.Category).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(MaintenanceFee), input.Id);
            }

            return MapToEntityDto(building);
        }

        [HttpPost]
        public override async Task<PagedResultDto<MaintenanceFeeDto>> GetList(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<MaintenanceFeeDto>();
            }
            try
            {
                var result = new PagedResultDto<MaintenanceFeeDto>();
                
                var dataQuery = Repository.GetAll().AsNoTracking();
                var countQuery = Repository.GetAll().AsNoTracking();

                dataQuery = queryInfo.ApplyDataQuery(dataQuery);
                if (queryInfo.Sorter == null)
                {
                    dataQuery = dataQuery.OrderByDescending(x => x.Year).ThenByDescending(x => x.Month);
                }
 
                countQuery = queryInfo.ApplyCountQuery(countQuery);

                var data = await dataQuery.ToListAsync();

                result.Items = data.Select(MapToEntityDto).ToList();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        public override async Task<MaintenanceFeeDto> Create(CreateMaintenanceFeeDto input)
        {
            var fee = await base.Create(input);
            var property = await _propertyRepository.GetAsync(fee.PropertyId);
            await _paymentManager.SyncPropertyPayments(property);
            return fee;
        }

        public async Task RunGeneratorJob()
        {
            var buildings = await _buildingRepository.GetAllListAsync(x =>
                !x.IsDeleted
                && x.ContractStartDate < DateTime.UtcNow
                && (!x.ContractEndDate.HasValue
                    || x.ContractEndDate.HasValue
                    && x.ContractEndDate > DateTime.UtcNow));

            foreach (var building in buildings)
            {
                await _billGenerator.GenerateMonthlyMaintenanceFeesBuilding(building.Id);
                await _billGenerator.GenerateScheduledFeesBuilding(building.Id);
                await _billGenerator.GenerateScheduledExpenses(building.Id);
            }

            await _paymentManager.SyncPayments();
        }

        public override async Task Delete(EntityDto<int> input)
        {
            await _paymentManager.DeletePaymentTrackers(input.Id);
            await base.Delete(input);
        }

        [HttpPut]
        public override async Task<MaintenanceFeeDto> Update(UpdateMaintenanceFeeDto input)
        {
            var fee = await Repository.GetAsync(input.Id);
            var previousAmount = fee.Amount;
            var result = await base.Update(input);

            if (previousAmount != input.Amount)
            {
                await _paymentManager.UpdatePaymentTrackers(fee);
            }

            return result;
        }
    }
}
