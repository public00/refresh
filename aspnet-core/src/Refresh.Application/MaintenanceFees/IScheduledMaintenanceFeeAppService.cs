﻿namespace Refresh.MaintenanceFees
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IScheduledMaintenanceFeeAppService : IAsyncCrudAppService<ScheduledMaintenanceFeeDto, int, PagedResultRequestDto, CreateScheduledMaintenanceFeeDto, UpdateScheduledMaintenanceFeeDto>
    {
    }
}
