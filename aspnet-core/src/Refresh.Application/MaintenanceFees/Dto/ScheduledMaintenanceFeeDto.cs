﻿namespace Refresh.MaintenanceFees.Dto
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;
    using PropertiesService.Dto;

    [AutoMap(typeof(ScheduledMaintenanceFee))]
    public class ScheduledMaintenanceFeeDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public int CategoryId { get; set; }

        public int PropertyId { get; set; }

        public decimal Amount { get; set; }

        public DateTime? LastRunDate { get; set; }

        public virtual MaintenanceFeeCategoryDto Category { get; set; }

        public virtual PropertyDto Property { get; set; }

        [Required]
        [StringLength(32)]
        public string Months { get; set; }

        public List<int> GetScheduledMonths()
        {
            return Months.Split(',').Select(x => Convert.ToInt32(x)).ToList();
        }
    }
}
