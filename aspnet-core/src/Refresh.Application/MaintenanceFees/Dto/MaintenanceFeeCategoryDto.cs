﻿namespace Refresh.MaintenanceFees.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMap(typeof(MaintenanceFeeCategory))]
    public class MaintenanceFeeCategoryDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
