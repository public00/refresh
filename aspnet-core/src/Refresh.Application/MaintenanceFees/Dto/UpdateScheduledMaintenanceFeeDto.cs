﻿namespace Refresh.MaintenanceFees.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(ScheduledMaintenanceFee))]
    public class UpdateScheduledMaintenanceFeeDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public int CategoryId { get; set; }

        public int PropertyId { get; set; }

        public decimal Amount { get; set; }

        [Required]
        [StringLength(32)]
        public string Months { get; set; }
    }
}
