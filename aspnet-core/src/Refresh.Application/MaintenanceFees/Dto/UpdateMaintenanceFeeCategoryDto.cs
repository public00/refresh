﻿namespace Refresh.MaintenanceFees.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(MaintenanceFeeCategory))]
    public class UpdateMaintenanceFeeCategoryDto : EntityDto
    {
        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
