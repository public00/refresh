﻿namespace Refresh.MaintenanceFees.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;
    using PropertiesService.Dto;

    [AutoMap(typeof(MaintenanceFee))]
    public class MaintenanceFeeDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public int CategoryId { get; set; }

        public int PropertyId { get; set; }

        public decimal Amount { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public bool IsFinnishedPayment { get; set; }

        public virtual MaintenanceFeeCategoryDto Category { get; set; }

        public virtual PropertyDto Property { get; set; }
    }
}
