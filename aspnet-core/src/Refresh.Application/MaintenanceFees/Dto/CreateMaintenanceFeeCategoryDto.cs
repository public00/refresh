﻿namespace Refresh.MaintenanceFees.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(MaintenanceFeeCategory))]
    public class CreateMaintenanceFeeCategoryDto
    {
        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
