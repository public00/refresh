﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Refresh.Authorization;

namespace Refresh
{
    [DependsOn(
        typeof(RefreshCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class RefreshApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<RefreshAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(RefreshApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
