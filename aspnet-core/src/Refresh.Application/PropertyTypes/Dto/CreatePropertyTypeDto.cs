﻿namespace Refresh.PropertyTypes.Dto
{
    using Abp.AutoMapper;
    using Entities.Properties;
    using System.ComponentModel.DataAnnotations;

    [AutoMapTo(typeof(PropertyType))]
    public class CreatePropertyTypeDto
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
