﻿namespace Refresh.PropertyTypes.Dto
{
    using Abp.AutoMapper;
    using Entities.Properties;
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;

    [AutoMapTo(typeof(PropertyType))]
    public class UpdatePropertyTypeDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
