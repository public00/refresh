﻿namespace Refresh.PropertyTypes.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Properties;

    [AutoMap(typeof(PropertyType))]
    public class PropertyTypeDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
