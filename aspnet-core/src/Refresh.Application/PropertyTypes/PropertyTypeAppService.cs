﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Refresh.Entities.Properties;
using Refresh.PropertyTypes.Dto;
using Refresh.Services.Export;

namespace Refresh.PropertyTypes
{
    using Abp.Authorization;
    using Authorization.Users;
    using Castle.Core.Logging;

    [AbpAuthorize]
    public class PropertyTypeAppService : RefreshCrudAppService<PropertyType, PropertyTypeDto, int, PagedResultRequestDto, CreatePropertyTypeDto, UpdatePropertyTypeDto>, IPropertyTypeAppService
    {
        public PropertyTypeAppService(IRepository<PropertyType> repository, IExportService exportService, IRepository<User, long> userRepository, ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
        }
    }
}
