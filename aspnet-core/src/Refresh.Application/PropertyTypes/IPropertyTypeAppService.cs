﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Refresh.PropertyTypes.Dto;

namespace Refresh.PropertyTypes
{
    public interface IPropertyTypeAppService : IAsyncCrudAppService<PropertyTypeDto, int, PagedResultRequestDto, CreatePropertyTypeDto, UpdatePropertyTypeDto>
    {
    }
}
