﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using Refresh.Entities.Tickets;

namespace Refresh.TicketComments.Dto
{
    using Users.Dto;

    [AutoMap(typeof(TicketComment))]
    public class TicketCommentDto : FullAuditedEntityDto
    {
        public int TicketId { get; set; }

        public string Comment { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual UserDto Creator { get; set; }
    }
}
