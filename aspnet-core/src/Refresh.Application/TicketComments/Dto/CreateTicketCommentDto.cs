﻿using Abp.AutoMapper;
using System;
using Refresh.Entities.Tickets;

namespace Refresh.TicketComments.Dto
{
    [AutoMapTo(typeof(TicketComment))]
    public class CreateTicketCommentDto
    {
        public int TicketId { get; set; }

        public string Comment { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
