﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Refresh.Entities.Tickets;
using Refresh.Services.Export;
using Refresh.TicketComments.Dto;

namespace Refresh.TicketComments
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    [AbpAuthorize]
    public class TicketCommentAppService : RefreshCrudAppService<TicketComment, TicketCommentDto, int, PagedResultRequestDto, CreateTicketCommentDto, UpdateTicketCommentDto>, ITicketCommentAppService
    {
        public TicketCommentAppService(
            IRepository<TicketComment> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
        }

        [HttpGet]
        public async Task<IList<TicketCommentDto>> GetComments(int ticketId)
        {
            var comments = await Repository.GetAllIncluding(x => x.Creator).Where(c => c.TicketId == ticketId).ToListAsync();
            return comments.Select(MapToEntityDto).ToList();
        }
    }
}
