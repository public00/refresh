﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Refresh.TicketComments.Dto;

namespace Refresh.TicketComments
{
    public interface ITicketCommentAppService : IAsyncCrudAppService<TicketCommentDto, int, PagedResultRequestDto, CreateTicketCommentDto, UpdateTicketCommentDto>
    {

    }
}
