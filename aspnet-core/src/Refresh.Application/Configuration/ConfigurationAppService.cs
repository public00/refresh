﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Refresh.Configuration.Dto;

namespace Refresh.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : RefreshAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
