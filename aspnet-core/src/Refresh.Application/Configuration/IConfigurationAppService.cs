﻿using System.Threading.Tasks;
using Refresh.Configuration.Dto;

namespace Refresh.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
