﻿namespace Refresh
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Services.Export;

    public class RefreshCrudAppService<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TCreateInput, TUpdateInput> 
        : AsyncCrudAppService<TEntity, TEntityDto, TPrimaryKey, PagedResultRequestDto, TCreateInput, TUpdateInput, EntityDto<TPrimaryKey>>
        where TEntity : class, IEntity<TPrimaryKey>
        where TEntityDto : IEntityDto<TPrimaryKey>
        where TUpdateInput : IEntityDto<TPrimaryKey>
    {
        protected readonly IExportService ExportService;
        private readonly IRepository<User, long> _userRepository;
        public new readonly ILogger Logger;

        public RefreshCrudAppService(
            IRepository<TEntity, TPrimaryKey> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger) : base(repository)
        {
            ExportService = exportService;
            _userRepository = userRepository;
            Logger = logger;
        }

        /// <summary>
        /// Method used to get grid dataSource.
        /// </summary>
        /// <param name="queryInfo"></param>
        [HttpPost]
        public virtual async Task<PagedResultDto<TEntityDto>> GetList(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<TEntityDto>();
            }
            try
            {
                var result = new PagedResultDto<TEntityDto>();

                UserCityAccessCheck(queryInfo);

                var dataQuery = GetDataQuery(queryInfo).AsNoTracking();
                var countQuery = GetCountQuery(queryInfo).AsNoTracking();

                var data = await dataQuery.ToListAsync();

                result.Items = data.Select(MapToEntityDto).ToList();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Exports grid data
        /// </summary>
        [HttpGet]
        public virtual async Task<FileResult> Export()
        {
            var queryInfo = new QueryInfo();
            queryInfo.Skip = null;
            queryInfo.Take = null;
            queryInfo.ExportColumns = queryInfo.ExportColumns != null && queryInfo.ExportColumns.Any() ? queryInfo.ExportColumns : typeof(TEntity).GetProperties().Select(x => new ExportColumnInfo {Title = x.Name, Property = x.Name}).ToList();

            UserCityAccessCheck(queryInfo);

            var data = await GetDataQuery(queryInfo).ToListAsync();
            var stream = ExportService.ExportToExcel(data, queryInfo.ExportColumns);
            return stream == null ? null : new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        private void UserCityAccessCheck(QueryInfo queryInfo)
        {
            if (!AbpSession.UserId.HasValue || typeof(TEntity).GetProperty("CityId") == null)
            {
                return;
            }

            var user = _userRepository.Get(AbpSession.UserId.Value);
            if (!user.CityId.HasValue)
            {
                return;
            }

            queryInfo.FilterRule.Condition = QueryCondition.And;
            queryInfo.FilterRule.Rules.Add(new RuleInfo
            {
                Field = "CityId",
                Value = user.CityId,
                Operator = QueryOperator.Equal
            });
        }

        protected void ApplyCityIdFilterRule(QueryInfo queryInfo, string propertyName)
        {
            var user = _userRepository.Get(AbpSession.UserId.Value);
            if (user.CityId.HasValue)
            {
                queryInfo.FilterRule.Condition = QueryCondition.And;
                queryInfo.FilterRule.Rules.Add(new RuleInfo { Field = propertyName, Value = user.CityId, Operator = QueryOperator.Equal });
            }
        }

        protected bool IsEndUser()
        {
            var user = _userRepository.Get(AbpSession.UserId.Value);
            return user?.UserTypeId == (int)UserTypes.PropertyOwner;
        }

        protected IQueryable<TEntity> GetDataQuery(QueryInfo queryInfo)
        {
            var query = Repository.GetAll();
            return queryInfo.ApplyDataQuery(query);
        }

        protected IQueryable<TEntity> GetCountQuery(QueryInfo queryInfo)
        {
            var query = Repository.GetAll();
            return queryInfo.ApplyCountQuery(query);
        }
    }
}
