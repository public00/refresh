﻿namespace Refresh.Invoices
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IInvoiceItemAppService : IAsyncCrudAppService<InvoiceItemDto, int, PagedResultRequestDto, CreateInvoiceItemDto, UpdateInvoiceItemDto>
    {
    }
}
