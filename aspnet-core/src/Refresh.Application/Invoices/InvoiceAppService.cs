﻿namespace Refresh.Invoices
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing.Invoices;
    using Entities.Buildings;
    using Entities.Cities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Services.Export;
    using Views;

    [AbpAuthorize]
    public class InvoiceAppService : RefreshCrudAppService<Invoice, InvoiceDto, int, PagedResultRequestDto, CreateInvoiceDto, InvoiceDto>, IInvoiceAppService
    {
        private readonly IInvoiceViewQuery _invoiceViewQuery;
        private readonly IRepository<InvoiceItem> _invoiceItemsRepository;
        private readonly IRepository<Building> _buildingsRepository;
        private readonly IRepository<City> _citiesRepository;

        public InvoiceAppService(
            IRepository<Invoice, int> repository, 
            IRepository<InvoiceItem> invoiceItemsRepository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            IInvoiceViewQuery invoiceViewQuery, 
            IRepository<Building> buildingsRepository, 
            IRepository<City> citiesRepository,
            ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
            _invoiceViewQuery = invoiceViewQuery;
            _buildingsRepository = buildingsRepository;
            _citiesRepository = citiesRepository;
            _invoiceItemsRepository = invoiceItemsRepository;
        }

        [HttpGet]
        public async Task<InvoiceView> GetView(int id)
        {
            return await _invoiceViewQuery.Get(id);
        }

        [HttpPost]
        public async Task<PagedResultDto<InvoiceView>> GetGridData(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<InvoiceView>();
            }

            try
            {
                var result = new PagedResultDto<InvoiceView>();

                var dataQuery = _invoiceViewQuery.GetDataQuery(queryInfo);
                var countQuery = _invoiceViewQuery.GetCountQuery(queryInfo);

                result.Items = await dataQuery.ToListAsync();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        public async Task<FileResult> Print(int id) // Word
        {
            var invoice = await Repository.GetAsync(id);
            if (!invoice.InvoiceDate.HasValue)
            {
                invoice.InvoiceDate = DateTime.Now;
                await Repository.UpdateAsync(invoice);
            }

            var items = await _invoiceItemsRepository.GetAllListAsync(x => x.InvoiceId == id);
            var building = await _buildingsRepository.GetAsync(invoice.BuildingId);
            var city = await _citiesRepository.GetAsync(building.CityId);

            var model = new
            {
                Id = $"IF{invoice.Id:0000}",
                Date = invoice.InvoiceDate.Value.ToString("dd.mm.yyyy"),
                City = city.Name,
                Zip = "81000",
                Pib = building.PIB,
                Note = "Održavanje za mjesec oktobar 2018",
                ItemsNote = "",
                Items = items.Select((x, i) => new
                {
                    Id = i + 1,
                    ItemId = $"ART{x.Id:0000}",
                    x.Title,
                    x.Unit,
                    x.Quantity,
                    Price = x.Price.ToString("N2"),
                    Vat = x.Vat.GetValueOrDefault(0).ToString("N2"),
                    VatAmount = x.VatAmount.ToString("N2"),
                    Total = x.Total.ToString("N2")
                }).ToList(),
                Totals = items.GroupBy(x => x.Vat).Select(x => new
                {
                    VatPercent = x.Key ?? 0,
                    Amount = x.Sum(i => i.Quantity * i.Price).ToString("N2"),
                    VatAmount = x.Sum(i => i.VatAmount).ToString("N2")
                }).ToList(),
                TotalNoVat = items.Sum(x => x.Quantity * x.Price).ToString("N2"),
                VatTotal = items.Sum(x => x.VatAmount).ToString("N2"),
                Total = items.Sum(x => x.Total).ToString("N2")
            };

            var stream = ExportService.ExportWordInvoice(model, id);
            return stream == null ? null : new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPut]
        public override Task<InvoiceDto> Update(InvoiceDto input)
        {
            throw new InvalidOperationException("Updating invoices is not allowed.");
        }

        [HttpDelete]
        public override async Task Delete(EntityDto<int> input)
        {
            var item = await Repository.GetAsync(input.Id);
            if (item.ProinvoiceDate.HasValue || item.InvoiceDate.HasValue)
            {
                throw new InvalidOperationException("Deleting an invoice already printed invoice is not allowed.");
            }

            await base.Delete(input);
        }
    }
}
