﻿namespace Refresh.Invoices
{
    using System;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing.Invoices;
    using Microsoft.AspNetCore.Mvc;
    using Services.Export;

    [AbpAuthorize]
    public class InvoiceItemAppService : RefreshCrudAppService<InvoiceItem, InvoiceItemDto, int, PagedResultRequestDto, CreateInvoiceItemDto, UpdateInvoiceItemDto>, IInvoiceItemAppService
    {
        private readonly IRepository<Invoice> _invoicesRepository;

        public InvoiceItemAppService(
            IRepository<InvoiceItem, int> repository,
            IExportService exportService,
            IRepository<User, long> userRepository,
            IRepository<Invoice> invoicesRepository,
            ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
            _invoicesRepository = invoicesRepository;
        }

        [HttpPost]
        public override async Task<InvoiceItemDto> Create(CreateInvoiceItemDto input)
        {
            await EnsureInvoiceUpdateIsAllowed(input.InvoiceId);
            return await base.Create(input);
        }

        [HttpPut]
        public override async Task<InvoiceItemDto> Update(UpdateInvoiceItemDto input)
        {
            await EnsureInvoiceUpdateIsAllowed(input.InvoiceId);
            return await base.Update(input);
        }

        [HttpDelete]
        public override async Task Delete(EntityDto<int> input)
        {
            var item = await Repository.GetAsync(input.Id);
            await EnsureInvoiceUpdateIsAllowed(item.InvoiceId);
            await base.Delete(input);
        }

        /// <summary>
        /// Throw exception if a printed invoice is edited.
        /// </summary>
        private async Task EnsureInvoiceUpdateIsAllowed(int invoiceId)
        {
            var invoice = await _invoicesRepository.GetAsync(invoiceId);
            if (invoice.ProinvoiceDate.HasValue || invoice.InvoiceDate.HasValue)
            {
                throw new InvalidOperationException("Updating an already printed invoice is not allowed.");
            }
        }
    }
}
