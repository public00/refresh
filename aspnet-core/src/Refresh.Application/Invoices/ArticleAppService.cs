﻿namespace Refresh.Invoices
{
    using Abp.Application.Services.Dto;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing.Invoices;
    using Services.Export;

    public class ArticleAppService : RefreshCrudAppService<Article, ArticleDto, int, PagedResultRequestDto, CreateArticleDto, ArticleDto>, IArticleAppService
    {
        public ArticleAppService(
            IRepository<Article, int> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
        }
    }
}
