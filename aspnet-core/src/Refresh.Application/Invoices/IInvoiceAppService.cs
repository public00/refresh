﻿namespace Refresh.Invoices
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IInvoiceAppService : IAsyncCrudAppService<InvoiceDto, int, PagedResultRequestDto, CreateInvoiceDto, InvoiceDto>
    {
    }
}
