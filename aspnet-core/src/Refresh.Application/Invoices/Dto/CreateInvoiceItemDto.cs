﻿namespace Refresh.Invoices.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Billing.Invoices;

    [AutoMapTo(typeof(InvoiceItem))]
    public class CreateInvoiceItemDto
    {
        public int InvoiceId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string Unit { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public decimal? Vat { get; set; }
    }
}
