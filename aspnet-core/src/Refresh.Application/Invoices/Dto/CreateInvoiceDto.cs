﻿namespace Refresh.Invoices.Dto
{
    using Abp.AutoMapper;
    using Entities.Billing.Invoices;

    [AutoMapTo(typeof(Invoice))]
    public class CreateInvoiceDto
    {
        public int BuildingId { get; set; }
    }
}
