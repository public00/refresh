﻿namespace Refresh.Invoices.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;

    public class UpdateArticleDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string Unit { get; set; }

        public decimal Price { get; set; }

        public decimal? Vat { get; set; }
    }
}
