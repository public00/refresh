﻿namespace Refresh.Invoices.Dto
{
    using System;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing.Invoices;

    [AutoMap(typeof(Invoice))]
    public class InvoiceDto : FullAuditedEntityDto
    {
        public string ReferenceNumber { get; set; }

        public int BuildingId { get; set; }

        public DateTime? ProinvoiceDate { get; set; }

        public DateTime? InvoiceDate { get; set; }
    }
}
