﻿namespace Refresh.Invoices.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing.Invoices;

    [AutoMap(typeof(InvoiceItem))]
    public class InvoiceItemDto : FullAuditedEntityDto
    {
        public int InvoiceId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string Unit { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public decimal? Vat { get; set; }

        public decimal VatAmount => Vat.HasValue ? Price * Quantity * Vat.Value / 100 : 0;

        public decimal Total => Price * Quantity + VatAmount;
    }
}
