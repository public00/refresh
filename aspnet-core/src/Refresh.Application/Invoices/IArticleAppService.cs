﻿namespace Refresh.Invoices
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IArticleAppService : IAsyncCrudAppService<ArticleDto, int, PagedResultRequestDto, CreateArticleDto, ArticleDto>
    {
    }
}
