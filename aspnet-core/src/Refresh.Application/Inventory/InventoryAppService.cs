﻿namespace Refresh.Inventory
{
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Buildings;
    using Services.Export;

    [AbpAuthorize]
    public class InventoryAppService : RefreshCrudAppService<Inventory, InventoryDto, int, PagedResultRequestDto, CreateInventoryDto, UpdateInventoryDto>, IInventoryAppService
    {
        public InventoryAppService(IRepository<Inventory, int> repository, IExportService exportService, IRepository<User, long> userRepository, ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
        }
    }
}
