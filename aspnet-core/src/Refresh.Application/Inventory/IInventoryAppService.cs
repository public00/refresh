﻿namespace Refresh.Inventory
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IInventoryAppService : IAsyncCrudAppService<InventoryDto, int, PagedResultRequestDto, CreateInventoryDto, UpdateInventoryDto>
    {
    }
}
