﻿namespace Refresh.Inventory.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Buildings;

    [AutoMap(typeof(Inventory))]
    public class InventoryDto : FullAuditedEntityDto
    {
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public int? ExpenseId { get; set; }
    }
}
