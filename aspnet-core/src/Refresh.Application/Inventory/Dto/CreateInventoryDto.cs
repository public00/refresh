﻿namespace Refresh.Inventory.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Buildings;

    [AutoMapTo(typeof(Inventory))]
    public class CreateInventoryDto
    {
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public int? ExpenseId { get; set; }
    }
}
