using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Refresh.Roles.Dto;
using Refresh.Users.Dto;

namespace Refresh.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
