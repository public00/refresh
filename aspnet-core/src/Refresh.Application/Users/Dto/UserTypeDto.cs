﻿namespace Refresh.Users.Dto
{
    using System;

    public class UserTypeDto
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Translation
        {
            get
            {
                switch (Id)
                {
                    case (int)UserTypes.SystemAdministrator:
                        return "Sistem administrator";
                    case (int)UserTypes.CompanyOwner:
                        return "Refresh vlasnik";
                    case (int)UserTypes.CityAdministrator:
                        return "Adminstrator gradova";
                    case (int)UserTypes.Janitor:
                        return "Domar";
                    case (int)UserTypes.PropertyOwner:
                        return "Vlasnik prostora";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
