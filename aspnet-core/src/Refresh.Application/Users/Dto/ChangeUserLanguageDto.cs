using System.ComponentModel.DataAnnotations;

namespace Refresh.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}