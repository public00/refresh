using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using Refresh.Authorization.Roles;
using Refresh.Authorization.Users;
using Refresh.Roles.Dto;
using Refresh.Services.Export;
using Refresh.Users.Dto;

namespace Refresh.Users
{
    using Abp.Net.Mail;
    using Castle.Core.Logging;
    using System;

    [AbpAuthorize]
    public class UserAppService : RefreshCrudAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IEmailSender _emailSender;

        public UserAppService(
            IEmailSender emailSender,
            IRepository<User, long> repository,
            UserManager userManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher,
            IExportService exportService,
            IRepository<User, long> userRepository,
            ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
            _emailSender = emailSender;
            _userManager = userManager;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
        }

        //  https://stackoverflow.com/questions/20906077/gmail-error-the-smtp-server-requires-a-secure-connection-or-the-client-was-not
        [AbpAllowAnonymous]
        public async Task<string> MailLinkConfiguration(string emailAddress)
        {
            var emails = await Repository.GetAllListAsync(x => x.EmailAddress == emailAddress);
            if (emails.Count() == 0)
            {
         //       return "No email in database";
            }

            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());

            var link = "http://localhost:21021/api/services/app/User/GetNewPassword?token=" + token+ "&address=" + emailAddress;

            //Send a notification email
            _emailSender.Send(
                to: emailAddress,
                subject: "You have a new task!",
                body: $"Reset your password on this link: </br> </br> " + link,
                isBodyHtml: true
            );

            return string.Empty;
        }

        [AbpAllowAnonymous]
        public string GetNewPassword(string token, string address)
        {
            byte[] data = Convert.FromBase64String(token);
            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            if (when < DateTime.UtcNow.AddHours(-1))
            {
                return "Link not valid";
            }
            
            var user = Repository.GetAllList(x => x.EmailAddress == address).FirstOrDefault();

            //return user.EmailAddress ?? string.Empty;
            return address;
        }

        [AbpAllowAnonymous]
        public int SaveNewPassword(string email, string newPassword, string token)
        {
            var user = Repository.GetAllList(x => x.EmailAddress == email).FirstOrDefault();
            
            user.Password = _passwordHasher.HashPassword(user, newPassword);

            _userManager.UpdateAsync(user);

            return 0;
        }

        public override async Task<UserDto> Create(CreateUserDto input)
        {
            CheckCreatePermission();

            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.Password = _passwordHasher.HashPassword(user, input.Password);
            user.IsEmailConfirmed = true;

            CheckErrors(await _userManager.CreateAsync(user));

            CurrentUnitOfWork.SaveChanges();

            return MapToEntityDto(user);
        }

        public override async Task<UserDto> Update(UpdateUserDto input)
        {
            CheckUpdatePermission();

            var user = await _userManager.GetUserByIdAsync(input.Id);

            MapToEntity(input, user);

            CheckErrors(await _userManager.UpdateAsync(user));

            return await Get(input);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            await _userManager.DeleteAsync(user);
        }

        public List<UserTypeDto> GetUserTypes()
        {
            var dict =
                System.Enum.GetValues(typeof(UserTypes))
                    .Cast<UserTypes>().Select(x => new UserTypeDto
                    {
                        Id = (int)x,
                        Type = x.ToString()
                    }).ToList();
            return dict;
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UpdateUserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return user;
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
