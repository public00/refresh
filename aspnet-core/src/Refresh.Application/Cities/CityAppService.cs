﻿namespace Refresh.Cities
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Buildings;
    using Entities.Cities;
    using Entities.Owners;
    using Entities.Properties;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Services.Export;
    using Sessions;

    [AbpAuthorize]
    public class CityAppService : RefreshCrudAppService<City, CityDto, int, PagedResultRequestDto, CreateCityDto, UpdateCityDto>, ICityAppService
    {
        private readonly ISessionAppService _sessionAppService;
        private readonly IRepository<Property> _propertiesRepository;
        private readonly IRepository<PropertyOwner> _propertyOwnersRepository;
        private readonly IRepository<Owner> _ownersRepository;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<User, long> _userRepository;

        public CityAppService(
            IRepository<City, int> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ISessionAppService sessionAppService, 
            IRepository<Property> propertiesRepository, 
            IRepository<PropertyOwner> propertyOwnersRepository, 
            IRepository<Owner> ownersRepository, 
            IRepository<Building> buildingRepository,
            ILogger logger, 
            IRepository<User, long> userRepository1)
            : base(repository, exportService, userRepository, logger)
        {
            _sessionAppService = sessionAppService;
            _propertiesRepository = propertiesRepository;
            _propertyOwnersRepository = propertyOwnersRepository;
            _ownersRepository = ownersRepository;
            _buildingRepository = buildingRepository;
            _userRepository = userRepository1;
        }

        [HttpPost]
        public override async Task<PagedResultDto<CityDto>> GetList(QueryInfo queryInfo)
        {
            // User may only access cities of owned properites
            if (_sessionAppService.IsPropertyOwner(out var userId))
            {
                return await GetPropertyOwnerCities(userId, queryInfo);
            }

            ApplyCityIdFilterRule(queryInfo, "Id");
            
            return await base.GetList(queryInfo);
        }

        private async Task<PagedResultDto<CityDto>> GetPropertyOwnerCities(long userId, QueryInfo queryInfo)
        {
            var owner = await _ownersRepository.FirstOrDefaultAsync(o => o.UserId == userId);
            var propertyOwners = await _propertyOwnersRepository.GetAllListAsync(po => po.OwnerId == owner.Id);
            var propertyIds = propertyOwners.Select(x => x.PropertyId);
            var properties = await _propertiesRepository.GetAllListAsync(p => propertyIds.Contains(p.Id));
            var buildingIds = properties.Select(p => p.BuildingId);
            var buildings = await _buildingRepository.GetAllListAsync(b => buildingIds.Contains(b.Id));
            var cityIds = buildings.Select(b => b.CityId);

            try
            {
                var result = new PagedResultDto<CityDto>();

                var dataQuery = Repository.GetAll().Where(x => cityIds.Contains(x.Id));
                var countQuery = Repository.GetAll().Where(x => cityIds.Contains(x.Id));

                dataQuery = queryInfo.ApplyDataQuery(dataQuery);
                countQuery = queryInfo.ApplyCountQuery(countQuery);

                var data = await dataQuery.ToListAsync();

                result.Items = data.Select(MapToEntityDto).ToList();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
