﻿namespace Refresh.Cities
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface ICityAppService : IAsyncCrudAppService<CityDto, int, PagedResultRequestDto, CreateCityDto, UpdateCityDto>
    {
    }
}
