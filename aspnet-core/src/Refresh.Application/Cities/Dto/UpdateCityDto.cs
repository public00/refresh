﻿namespace Refresh.Cities.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Cities;

    [AutoMapTo(typeof(City))]
    public class UpdateCityDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(64)]
        public string PhoneNumber { get; set; }

        [StringLength(256)]
        public string Address { get; set; }
    }
}
