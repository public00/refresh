﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Refresh.Authorization.Users;
using Refresh.InvoiceSendingTracker.Dto;
using Refresh.Services.Export;
using InvoiceSending = Refresh.Entities.InvoiceSendingTrackers;

namespace Refresh.InvoiceSendingTracker
{
    using Castle.Core.Logging;

    [AbpAuthorize]
    public class InvoiceSendingTrackerAppService : RefreshCrudAppService<InvoiceSending.InvoiceSendingTracker, InvoiceSendingTrackerDto, int, PagedResultRequestDto, CreateInvoiceSendingTrackerDto, UpdateInvoiceSendingTrackerDto>, IInvoiceSendingTrackerAppService
    {
        public InvoiceSendingTrackerAppService(IRepository<InvoiceSending.InvoiceSendingTracker, int> repository,
           IExportService exportService,
           IRepository<User, long> userRepository, 
           ILogger logger)
           : base(repository, exportService, userRepository, logger)
        {
        }
    }
}