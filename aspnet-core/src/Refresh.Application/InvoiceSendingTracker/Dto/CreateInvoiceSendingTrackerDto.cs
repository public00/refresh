﻿using Abp.AutoMapper;
using InvoiceSendingTrackers = Refresh.Entities.InvoiceSendingTrackers;

namespace Refresh.InvoiceSendingTracker.Dto
{
    [AutoMap(typeof(InvoiceSendingTrackers.InvoiceSendingTracker))]
    public class CreateInvoiceSendingTrackerDto
    {
        public int OwnerId { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }
    }
}
