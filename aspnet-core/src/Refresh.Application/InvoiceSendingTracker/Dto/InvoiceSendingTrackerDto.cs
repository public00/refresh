﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
namespace Refresh.InvoiceSendingTracker.Dto
{
    [AutoMap(typeof(Entities.InvoiceSendingTrackers.InvoiceSendingTracker))] 
    public class InvoiceSendingTrackerDto : FullAuditedEntityDto
    {
        public int OwnerId { get; set; }

        public int Month{ get; set; }

        public int Year { get; set; }
    }
}