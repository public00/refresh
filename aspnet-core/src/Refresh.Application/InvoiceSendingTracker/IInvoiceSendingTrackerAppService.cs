﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Refresh.InvoiceSendingTracker.Dto;
namespace Refresh.InvoiceSendingTracker
{
    public interface IInvoiceSendingTrackerAppService : IAsyncCrudAppService<InvoiceSendingTrackerDto, int, PagedResultRequestDto, CreateInvoiceSendingTrackerDto, UpdateInvoiceSendingTrackerDto>
    {

    }
}
