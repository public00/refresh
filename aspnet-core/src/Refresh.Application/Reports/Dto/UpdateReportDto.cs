﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System;
using Abp.AutoMapper;
using Refresh.Entities.Reports;

namespace Refresh.Reports.Dto
{
    [AutoMapTo(typeof(Report))]
    public class UpdateReportDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string MaintenanceType { get; set; }

        [StringLength(128)]
        public string Invoice { get; set; }

        [StringLength(128)]
        public string SpentResources { get; set; }

        public int Quantity { get; set; }

        public decimal NPrice { get; set; }

        public decimal? PPrice { get; set; }

        [StringLength(32)]
        public string TimeSpent { get; set; }

        [StringLength(256)]
        public string Note { get; set; }
    }
}
