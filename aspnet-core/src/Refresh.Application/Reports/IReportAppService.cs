﻿namespace Refresh.Reports
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IReportAppService : IAsyncCrudAppService<ReportDto, int, PagedResultRequestDto, CreateReportDto, UpdateReportDto>
    {
    }
}
 