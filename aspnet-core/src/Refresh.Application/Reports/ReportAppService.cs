﻿namespace Refresh.Reports
{
    using Abp.Application.Services.Dto;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Dto;
    using Entities.Reports;
    using Microsoft.AspNetCore.Mvc;
    using Refresh.QueryBuilder;
    using Refresh.Views;
    using Services.Export;
    using System;
    using System.Linq;
    using Castle.Core.Logging;

    public class ReportAppService : RefreshCrudAppService<Report, ReportDto, int, PagedResultRequestDto, CreateReportDto, UpdateReportDto>, IReportAppService
    {
        private readonly IReportViewQuery _reportViewQuery;

        public ReportAppService(IRepository<Report, int> repository,  
            IExportService exportService,
            IReportViewQuery reportViewQuery,
            IRepository<User, long> userRepository,
            ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
            _reportViewQuery = reportViewQuery;
        }

        [HttpPost]
        public PagedResultDto<ReportView> GetGridData(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<ReportView>();
            }
            try
            {
                var result = new PagedResultDto<ReportView>();

                var dataQuery = _reportViewQuery.GetDataQuery(queryInfo);
                var countQuery = _reportViewQuery.GetCountQuery(queryInfo);

                result.Items = dataQuery.ToList();
                result.TotalCount = countQuery.Count();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }
    }
}
