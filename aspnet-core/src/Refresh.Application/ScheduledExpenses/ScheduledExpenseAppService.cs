﻿namespace Refresh.ScheduledExpenses
{
    using System;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Billing;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Services.Export;

    [AbpAuthorize]
    public class ScheduledExpenseAppService : RefreshCrudAppService<ScheduledExpense, ScheduledExpenseDto, int, PagedResultRequestDto, CreateScheduledExpenseDto, UpdateScheduledExpenseDto>, IScheduledExpenseAppService
    {
        private readonly BillGenerator _billGenerator;

        public ScheduledExpenseAppService(
            IRepository<ScheduledExpense, int> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger, 
            BillGenerator billGenerator) 
            : base(repository, exportService, userRepository, logger)
        {
            _billGenerator = billGenerator;
        }

        public override async Task<ScheduledExpenseDto> Create(CreateScheduledExpenseDto input)
        {
            var expense = await base.Create(input);
            if (!expense.GetScheduledMonths().Contains(DateTime.UtcNow.Month))
            {
                return expense;
            }

            await _billGenerator.GenerateScheduledExpenses(expense.BuildingId);
            return expense;
        }
    }
}
