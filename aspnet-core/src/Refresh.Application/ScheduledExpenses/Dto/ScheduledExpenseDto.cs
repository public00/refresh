﻿namespace Refresh.ScheduledExpenses.Dto
{
    using System;
    using System.Collections.Generic;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Billing;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    [AutoMap(typeof(ScheduledExpense))]
    public class ScheduledExpenseDto : FullAuditedEntityDto
    {
        public int BuildingId { get; set; }

        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        [StringLength(128)]
        public string Company { get; set; }

        public DateTime? LastRunDate { get; set; }

        [Required]
        [StringLength(32)]
        public string Months { get; set; }

        public List<int> GetScheduledMonths()
        {
            return Months.Split(',').Select(x => Convert.ToInt32(x)).ToList();
        }
    }
}
