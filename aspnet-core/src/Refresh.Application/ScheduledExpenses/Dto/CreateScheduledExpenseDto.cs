﻿namespace Refresh.ScheduledExpenses.Dto
{
    using Abp.AutoMapper;
    using Entities.Billing;
    using System.ComponentModel.DataAnnotations;

    [AutoMapTo(typeof(ScheduledExpense))]
    public class CreateScheduledExpenseDto
    {
        public int BuildingId { get; set; }

        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        [StringLength(128)]
        public string Company { get; set; }

        [Required]
        [StringLength(32)]
        public string Months { get; set; }
    }
}
