﻿namespace Refresh.ScheduledExpenses
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IScheduledExpenseAppService : IAsyncCrudAppService<ScheduledExpenseDto, int, PagedResultRequestDto, CreateScheduledExpenseDto, UpdateScheduledExpenseDto>
    {
    }
}
