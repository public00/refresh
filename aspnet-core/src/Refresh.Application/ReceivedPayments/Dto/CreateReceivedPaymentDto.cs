﻿namespace Refresh.ReceivedPayments.Dto
{
    using Abp.AutoMapper;
    using Entities.Billing;
    using System.ComponentModel.DataAnnotations;

    [AutoMapTo(typeof(ReceivedPayment))]
    public class CreateReceivedPaymentDto 
    {
        public int? PropertyId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public int BuildingId { get; set; }

        [StringLength(128)]
        public string BankStatementNumber { get; set; }

        public decimal Amount { get; set; }

        [StringLength(128)]
        public string BankAccount { get; set; }
    }
}
