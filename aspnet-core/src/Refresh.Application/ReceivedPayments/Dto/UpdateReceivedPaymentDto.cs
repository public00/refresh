﻿namespace Refresh.ReceivedPayments.Dto
{
    using Abp.AutoMapper;
    using Entities.Billing;
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;

    [AutoMapTo(typeof(ReceivedPayment))]
    public class UpdateReceivedPaymentDto : EntityDto
    {
        public int? PropertyId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [StringLength(128)]
        public string BankStatementNumber { get; set; }

        public decimal Amount { get; set; }

        [StringLength(128)]
        public string BankAccount { get; set; }
    }
}
