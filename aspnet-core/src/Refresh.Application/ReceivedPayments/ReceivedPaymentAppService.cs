﻿namespace Refresh.ReceivedPayments
{
    using System;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Billing;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Services.Export;

    [AbpAuthorize]
    public class ReceivedPaymentAppService : RefreshCrudAppService<ReceivedPayment, ReceivedPaymentDto, int, PagedResultRequestDto, CreateReceivedPaymentDto, UpdateReceivedPaymentDto>, IReceivedPaymentAppService
    {
        private readonly PaymentManager _paymentManager;

        public ReceivedPaymentAppService(IRepository<ReceivedPayment, int> repository, IExportService exportService, PaymentManager paymentManager, IRepository<User, long> userRepository, ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
            _paymentManager = paymentManager;
        }

        [HttpGet]
        public override async Task<ReceivedPaymentDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Building, x => x.Property).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(ReceivedPayment), input.Id);
            }

            return MapToEntityDto(building);
        }

        [HttpPost]
        public override async Task<ReceivedPaymentDto> Create(CreateReceivedPaymentDto input)
        {
            var payment = await base.Create(input);
            if (payment.Id > 0 && payment.PropertyId.HasValue)
            {
                var paymentEntity = await Repository.GetAsync(payment.Id);
                await _paymentManager.SaveReceivedPaymentTrackers(paymentEntity);
            }

            return payment;
        }

        [HttpDelete]
        public override async Task Delete(EntityDto<int> input)
        {
            await _paymentManager.DeleteReceivedPayment(input);
        }

        [HttpPut]
        public override async Task<ReceivedPaymentDto> Update(UpdateReceivedPaymentDto input)
        {
            var payment = await Repository.GetAsync(input.Id);
            var previousAmount = payment.Amount;
            var result = await base.Update(input);

            if (previousAmount != input.Amount)
            {
                await _paymentManager.UpdatePaymentTrackers(payment);
            }

            if (!payment.PropertyId.HasValue && input.PropertyId.HasValue)
            {
                await _paymentManager.SaveReceivedPaymentTrackers(payment);
            }

            return result;
        }
    }
}
