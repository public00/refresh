﻿namespace Refresh.ReceivedPayments
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IReceivedPaymentAppService : IAsyncCrudAppService<ReceivedPaymentDto, int, PagedResultRequestDto, CreateReceivedPaymentDto, UpdateReceivedPaymentDto>
    {
    }
}
