﻿namespace Refresh.Owners.Dto
{
    using Abp.AutoMapper;
    using Entities.Owners;
    using System;
    using System.ComponentModel.DataAnnotations;

    [AutoMap(typeof(Owner))] 
    public class CreateOwnerDto
    {
        public long? UserId { get; set; }

        [StringLength(64)]
        public string JMBG { get; set; }

        [Required]
        [StringLength(128)]
        public string FirstName { get; set; }

        [StringLength(128)]
        public string LastName { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(256)]
        public string Address { get; set; }

        [StringLength(64)]
        public string PhoneNumber { get; set; }
    }
}
