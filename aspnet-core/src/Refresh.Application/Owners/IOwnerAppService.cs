﻿namespace Refresh.Owners
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IOwnerAppService : IAsyncCrudAppService<OwnerDto, int, PagedResultRequestDto, CreateOwnerDto, UpdateOwnerDto>
    {
    }
}
