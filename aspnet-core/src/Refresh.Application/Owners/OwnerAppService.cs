﻿namespace Refresh.Owners
{
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Entities.Owners;
    using Dto;
    using Services.Export;
    using Microsoft.AspNetCore.Mvc;
    using Refresh.Views;
    using Refresh.QueryBuilder;
    using System.Linq;
    using System;

    [AbpAuthorize]
    public class OwnerAppService : RefreshCrudAppService<Owner, OwnerDto, int, PagedResultRequestDto, CreateOwnerDto, UpdateOwnerDto> , IOwnerAppService
    {
        private readonly IOwnerViewQuery _ownerViewQuery;

        public OwnerAppService(
            IRepository<Owner> repository, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger,
            IOwnerViewQuery ownerViewQuery)
            : base(repository, exportService, userRepository, logger)
        {
            _ownerViewQuery = ownerViewQuery;
        }

        [HttpPost]
        public PagedResultDto<OwnerView> GetDropdownData(QueryInfo queryInfo)
        {

            if (queryInfo == null)
            {
                return new PagedResultDto<OwnerView>();
            }
            try
            {
                var result = new PagedResultDto<OwnerView>();

                var dataQuery = _ownerViewQuery.GetDataQuery(queryInfo);
                var countQuery = _ownerViewQuery.GetCountQuery(queryInfo);

                result.Items = dataQuery.ToList();
                result.TotalCount = countQuery.Count();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }
    }
}
