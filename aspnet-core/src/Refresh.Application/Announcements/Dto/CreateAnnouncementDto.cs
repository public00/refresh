﻿namespace Refresh.Announcements.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Announcements;

    [AutoMapTo(typeof(Announcement))]
    public class CreateAnnouncementDto
    {
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }
    }
}
