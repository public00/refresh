﻿namespace Refresh.Announcements.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Announcements;

    [AutoMapTo(typeof(Announcement))]
    public class UpdateAnnouncementDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }
    }
}
