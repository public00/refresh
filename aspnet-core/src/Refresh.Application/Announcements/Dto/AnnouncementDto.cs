﻿namespace Refresh.Announcements.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Announcements;

    [AutoMap(typeof(Announcement))]
    public class AnnouncementDto : FullAuditedEntityDto
    {
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }

        [StringLength(512)]
        public string Path { get; set; }
    }
}
