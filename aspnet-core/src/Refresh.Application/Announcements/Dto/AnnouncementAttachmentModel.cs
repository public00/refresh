﻿namespace Refresh.Announcements.Dto
{
    using System.IO;
    using Microsoft.AspNetCore.Http;

    public class AnnouncementAttachmentModel
    {
        public int AnnouncementId { get; set; }

        public IFormFile File { get; set; }

        public bool IsValid(string path)
        {
            return 
                !string.IsNullOrEmpty(File.FileName) 
                && File.FileName.IndexOfAny(Path.GetInvalidFileNameChars()) < 0 
                && !System.IO.File.Exists(Path.Combine("", File.FileName)) 
                && File.Length > 0;
        }
    }
}
