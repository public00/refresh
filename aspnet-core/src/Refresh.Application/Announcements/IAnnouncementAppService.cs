﻿namespace Refresh.Announcements
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IAnnouncementAppService : IAsyncCrudAppService<AnnouncementDto, int, PagedResultRequestDto, CreateAnnouncementDto, UpdateAnnouncementDto>
    {
    }
}
