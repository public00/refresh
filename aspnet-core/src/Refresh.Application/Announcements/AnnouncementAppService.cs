﻿namespace Refresh.Announcements
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Announcements;
    using Microsoft.AspNetCore.Mvc;
    using Services.Export;

    [AbpAuthorize]
    public class AnnouncementAppService : RefreshCrudAppService<Announcement, AnnouncementDto, int, PagedResultRequestDto, CreateAnnouncementDto, UpdateAnnouncementDto>, IAnnouncementAppService
    {
        public AnnouncementAppService(IRepository<Announcement, int> repository, IExportService exportService, IRepository<User, long> userRepository, ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
        }

        [HttpPost]
        public async Task<AnnouncementDto> Upload(AnnouncementAttachmentModel model)
        {
            var announcement = await Repository.GetAsync(model.AnnouncementId);

            var filePath = @"Files\Announcements\Building_" + announcement.BuildingId;
            var attachments = Directory.GetFiles(filePath);
            foreach (var attachment in attachments)
            {
                File.Delete(attachment);
            }

            if (!model.IsValid(filePath))
            {
                throw new ArgumentException();
            }

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await model.File.CopyToAsync(stream);
            }

            announcement.Path = model.File.FileName;
            await Repository.UpdateAsync(announcement);

            return MapToEntityDto(announcement);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Download(int id)
        {
            var announcement = await Repository.GetAsync(id);
            var directory = @"Files\Announcements\Building_" + announcement.BuildingId;
            var path = Path.Combine(directory, announcement.Path);
            var file = File.ReadAllBytes(path);
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK) {Content = new ByteArrayContent(file)};
            response.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") {FileName = announcement.Path};
            return response;
        }
    }
}
