﻿namespace Refresh.Buildings.Dto
{
    using Abp.AutoMapper;
    using Entities.Buildings;
    using System;
    using System.ComponentModel.DataAnnotations;

    [AutoMapTo(typeof(Building))] 
    public class CreateBuildingDto
    {
        [Required]
        [StringLength(64)]
        public string PIB { get; set; }

        [Required]
        [StringLength(256)]
        public string StreetAddress { get; set; }
        
        public int CityId { get; set; }

        [StringLength(32)]
        public string PlotNumber { get; set; }

        [StringLength(64)]
        public string ImmovableProperty { get; set; }

        [StringLength(128)]
        public string CadastralMunicipality { get; set; }

        public DateTime ContractStartDate { get; set; }

        public DateTime? ContractEndDate { get; set; }

        [StringLength(128)]
        public string District { get; set; }
        
        public int? ManagerId { get; set; }
        
        public int? PresidentId { get; set; }

        [StringLength(128)]
        public string BankAccountPrimary { get; set; }

        [StringLength(128)]
        public string BankAccountSecondary { get; set; }

        [StringLength(128)]
        public string BIC { get; set; }

        [StringLength(128)]
        public string IBAN { get; set; }

        [StringLength(128)]
        public string FCAName { get; set; }

        [StringLength(128)]
        public string FCAAddress { get; set; }

        public decimal PricePerSquareMeter { get; set; }
    }
}
