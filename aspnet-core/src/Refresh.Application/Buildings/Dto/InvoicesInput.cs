﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refresh.Buildings.Dto
{
    public class InvoicesInput
    {
        public int? PropertyId { get; set; }
        public int? BuildingId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
