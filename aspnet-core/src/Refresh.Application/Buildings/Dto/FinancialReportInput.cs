﻿using System;
namespace Refresh.Buildings.Dto
{
    public class FinancialReportInput
    {
        public int BuildingId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
