﻿namespace Refresh.Buildings.Dto
{
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Buildings;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Cities.Dto;
    using Owners.Dto;

    [AutoMap(typeof(Building))]
    public class BuildingDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(64)]
        public string PIB { get; set; }

        [Required]
        [StringLength(256)]
        public string StreetAddress { get; set; }

        [ForeignKey("City")]
        public int CityId { get; set; }

        [StringLength(32)]
        public string PlotNumber { get; set; }

        [StringLength(64)]
        public string ImmovableProperty { get; set; }

        [StringLength(128)]
        public string CadastralMunicipality { get; set; }

        public DateTime ContractStartDate { get; set; }

        public DateTime? ContractEndDate { get; set; }

        [StringLength(128)]
        public string District { get; set; }

        [ForeignKey("Manager")]
        public int? ManagerId { get; set; }

        [ForeignKey("President")]
        public int? PresidentId { get; set; }

        [StringLength(128)]
        public string BankAccountPrimary { get; set; }

        [StringLength(128)]
        public string BankAccountSecondary { get; set; }

        [StringLength(128)]
        public string BIC { get; set; }

        [StringLength(128)]
        public string IBAN { get; set; }

        [StringLength(128)]
        public string FCAName { get; set; }

        [StringLength(128)]
        public string FCAAddress { get; set; }

        public decimal PricePerSquareMeter { get; set; }

        public decimal Balance { get; set; }

        public virtual OwnerDto Manager { get; set; }

        public virtual OwnerDto President { get; set; }

        public virtual CityDto City { get; set; }
    }
}
