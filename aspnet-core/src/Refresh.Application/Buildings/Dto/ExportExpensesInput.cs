﻿namespace Refresh.Buildings.Dto
{
    public class ExportExpensesInput
    {
        public int BuildingId { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

    }
}
