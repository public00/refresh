﻿namespace Refresh.Buildings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Abp.Application.Services.Dto;
    using Abp.Domain.Repositories;
    using Dto;
    using Entities.Buildings;
    using Services.Export;
    using System.Threading.Tasks;
    using Abp.Authorization;
    using Authorization.Users;
    using Billing;
    using Entities.Owners;
    using Entities.Properties;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Sessions;
    using Views;
    using Abp.Domain.Entities;
    using Castle.Core.Logging;
    using Refresh.Entities.Billing;
    using Refresh.Entities.Billing.Reports;
    using Refresh.Services.Export.ExportModel;
    using Refresh.Services.Email;

    [AbpAuthorize]
    public class BuildingAppService : RefreshCrudAppService<Building, BuildingDto, int, PagedResultRequestDto, CreateBuildingDto, UpdateBuildingDto>, IBuildingAppService
    {
        private readonly IBuildingViewQuery _buildingViewQuery;
        private readonly IBalanceViewQuery _balanceViewQuery;
        private readonly EmailSenderService _sender; 

        private readonly ISessionAppService _sessionAppService;
        private readonly IRepository<Property> _propertiesRepository;
        private readonly IRepository<PropertyOwner> _propertyOwnersRepository;
        private readonly IRepository<Owner> _ownersRepository;
        private readonly BillGenerator _billGenerator;

        public BuildingAppService(
            IRepository<Building> repository,
            EmailSenderService sender,
            IExportService exportService,
            IRepository<User, long> userRepository,
            IBuildingViewQuery buildingViewQuery,
            ISessionAppService sessionAppService,
            IRepository<PropertyOwner> propertyOwnersRepository,
            IRepository<Owner> ownersRepository,
            IRepository<Property> propertiesRepository,
            BillGenerator billGenerator,
            IBalanceViewQuery balanceViewQuery,
            ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
            _buildingViewQuery = buildingViewQuery;
            _sessionAppService = sessionAppService;
            _sender = sender;
            _propertyOwnersRepository = propertyOwnersRepository;
            _ownersRepository = ownersRepository;
            _propertiesRepository = propertiesRepository;
            _balanceViewQuery = balanceViewQuery;
            _billGenerator = billGenerator;
        }

        [HttpGet]
        public void SendMail(int buildingid, int month, int year)
        {
            _sender.SendInvoices(buildingid, month, year);
        }

        [HttpGet]
        public override async Task<BuildingDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.City, x => x.Manager, x => x.President).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(Building), input.Id);
            }

            return MapToEntityDto(building);
        }

        public async Task<BuildingDto> GetBuildingView(int id)
        {
            var building = await Repository.GetAllIncluding(x => x.Manager, x => x.President, x => x.City).FirstOrDefaultAsync(x => x.Id == id);
            var result = MapToEntityDto(building);
            var buildingView = await _buildingViewQuery.Get(id);
            result.Balance = buildingView.Balance ?? 0;
            return result;
        }

        [HttpPut]
        public override async Task<BuildingDto> Update(UpdateBuildingDto input)
        {
            var building = await Repository.GetAsync(input.Id);
            var isContractDateUpdated = building.ContractStartDate != input.ContractStartDate;
            var result = await base.Update(input);
            if (isContractDateUpdated)
            {
                await _billGenerator.GenerateMonthlyMaintenanceFeesBuilding(input.Id);
            }

            return result;
        }

        [HttpPost]
        public async Task<PagedResultDto<BuildingView>> GetGridData(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<BuildingView>();
            }

            // User may only access buildings of owned properties
            if (_sessionAppService.IsPropertyOwner(out var userId))
            {
                return await GetPropertyOwnerBuildings(userId, queryInfo);
            }

            try
            {
                var result = new PagedResultDto<BuildingView>();

                var dataQuery = _buildingViewQuery.GetDataQuery(queryInfo);
                var countQuery = _buildingViewQuery.GetCountQuery(queryInfo);

                result.Items = await dataQuery.ToListAsync();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpPost]
        public async Task<PagedResultDto<BalanceView>> GetBalancesData(QueryInfo queryInfo, int buildingId, int year)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<BalanceView>();
            }
            try
            {
                var result = new PagedResultDto<BalanceView>();

                var dataQuery = _balanceViewQuery.GetQueryable().Where(x => x.BuildingId == buildingId && x.Year == year);
                result.Items = await dataQuery.ToListAsync();
                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        private async Task<PagedResultDto<BuildingView>> GetPropertyOwnerBuildings(long userId, QueryInfo queryInfo)
        {
            var owner = await _ownersRepository.FirstOrDefaultAsync(o => o.UserId == userId);
            var propertyOwners = await _propertyOwnersRepository.GetAllListAsync(x => x.OwnerId == owner.Id);
            var propertyIds = propertyOwners.Select(x => x.PropertyId);
            var properties = await _propertiesRepository.GetAllListAsync(x => propertyIds.Contains(x.Id));
            var buildingIds = properties.Select(x => x.BuildingId);

            try
            {
                var result = new PagedResultDto<BuildingView>();

                var dataQuery = _buildingViewQuery.GetQueryable().Where(x => buildingIds.Contains(x.Id));
                var countQuery = _buildingViewQuery.GetQueryable().Where(x => buildingIds.Contains(x.Id));

                dataQuery = queryInfo.ApplyDataQuery(dataQuery);
                countQuery = queryInfo.ApplyCountQuery(countQuery);

                result.Items = await dataQuery.ToListAsync();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }
         
        public FileResult ExportToExcel(ExportSendModel obj) // Stara app excel report
        {
            var model = this.GetBalancesData(new QueryInfo(), obj.Id, obj.Year);
            var list = model.Result.Items.ToList();

            if (!list.Any())
            {
                return null;
            }

            var building = this.Repository.Get(list[0].BuildingId);

            BuildingExportModel exportModel = new BuildingExportModel
            {
                Balances = GetBalanceWithoutCurrentMonthCost(list),
                CityName = building.StreetAddress,
                DateNow = DateTime.Now.ToString("dd'.'MM'.'yyyy")
            };

            var stream =  ExportService.ExportExcelReport(exportModel, obj.Id, obj.Year);
            return stream == null ? null : new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        // They made me do this :(
        private static List<BalanceView> GetBalanceWithoutCurrentMonthCost(List<BalanceView> list)
        {
            var now = DateTime.UtcNow;
            var currentMonth = now.ToString("MMM");
            var currentMonthCostProp = typeof(BalanceView).GetProperty($"{currentMonth}Cost");
            var currentMonthPaidProp = typeof(BalanceView).GetProperty($"{currentMonth}Paid");
            foreach (var item in list.Where(x => x.Year == now.Year))
            {
                var cost = Convert.ToDecimal(currentMonthCostProp.GetValue(item));
                item.TotalCost -= cost;
                currentMonthCostProp.SetValue(item, 0.00m);
                currentMonthPaidProp.SetValue(item, Convert.ToDecimal(currentMonthPaidProp.GetValue(item)) + item.Overpaid);
                item.Overpaid = 0;
            }

            return list;
        }

        public FileResult BuildingFinancialReport(FinancialReportInput input) // Financijski izvjestaj
        {
            var stream = ExportService.BuildingFinancialReport(input.BuildingId, input.From, input.To);
            return stream == null ? null : new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }
        
        public FileResult DownloadInvoice(InvoicesInput input)
        {
            var print = !IsEndUser();
            var stream = ExportService.GetPropertyInvoice(input.PropertyId.Value, input.Month, input.Year, print);
            return stream == null ? null : new FileStreamResult(stream, System.Net.Mime.MediaTypeNames.Application.Zip);
        }
        
        public FileResult DownloadInvoices(InvoicesInput input)
        {
            var stream = ExportService.GetInvoices(input.BuildingId.Value, input.Month, input.Year);
            return stream == null ? null : new FileStreamResult(stream, System.Net.Mime.MediaTypeNames.Application.Zip);
        }

        public FileResult DownloadExpenses(ExportExpensesInput input)
        {
            var stream = ExportService.GetExpenses(input.BuildingId, input.Month, input.Year);
            return stream == null ? null : new FileStreamResult(stream, System.Net.Mime.MediaTypeNames.Application.Zip);
        }

    }  
}
