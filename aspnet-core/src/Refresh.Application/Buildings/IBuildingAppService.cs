﻿namespace Refresh.Buildings
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IBuildingAppService : IAsyncCrudAppService<BuildingDto, int, PagedResultRequestDto, CreateBuildingDto, UpdateBuildingDto>
    {
    }
}
