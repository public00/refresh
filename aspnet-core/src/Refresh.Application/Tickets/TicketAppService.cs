﻿namespace Refresh.Tickets
{
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Entities.Tickets;
    using Services.Export;
    using Dto;
    using Refresh.Views;
    using Refresh.QueryBuilder;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Domain.Entities;
    using Castle.Core.Logging;
    using Microsoft.EntityFrameworkCore;

    [AbpAuthorize]
    public class TicketAppService : RefreshCrudAppService<Ticket, TicketDto, int, PagedResultRequestDto, CreateTicketDto, UpdateTicketDto>, ITicketAppService
    {
        private readonly ITicketViewQuery _ticketViewQuery;

        public TicketAppService(
            IRepository<Ticket> repository, 
            ITicketViewQuery ticketViewQuery, 
            IExportService exportService, 
            IRepository<User, long> userRepository, 
            ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
            _ticketViewQuery = ticketViewQuery;
        }

        [HttpGet]
        public override async Task<TicketDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Property, x => x.Building).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(Ticket), input.Id);
            }

            return MapToEntityDto(building);
        }

        [HttpGet]
        public async Task<TicketView> GetView(int id)
        {
            return await _ticketViewQuery.Get(id);
        }

        [HttpPost]
        public PagedResultDto<TicketView> GetGridData(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<TicketView>();
            }
            try
            {
                var result = new PagedResultDto<TicketView>();

                var dataQuery = _ticketViewQuery.GetDataQuery(queryInfo);
                var countQuery = _ticketViewQuery.GetCountQuery(queryInfo);

                result.Items = dataQuery.ToList();
                result.TotalCount = countQuery.Count();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpGet]
        public async Task<int> GetActiveTicketCount()
        {
            return await _ticketViewQuery.GetQueryable().CountAsync(x => !x.IsClosed);
        }
    }
}
