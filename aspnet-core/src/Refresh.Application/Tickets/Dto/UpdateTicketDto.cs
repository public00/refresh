﻿namespace Refresh.Tickets.Dto
{
    using Abp.AutoMapper;
    using Entities.Tickets;
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;

    [AutoMapTo(typeof(Ticket))]
    public class UpdateTicketDto : EntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }

        public int BuildingId { get; set; }

        public int? PropertyId { get; set; }

        public bool IsClosed { get; set; }
    }
}
