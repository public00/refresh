﻿namespace Refresh.Tickets.Dto
{
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Tickets;
    using System.ComponentModel.DataAnnotations;
    using Buildings.Dto;
    using PropertiesService.Dto;

    [AutoMap(typeof(Ticket))]
    public class TicketDto : FullAuditedEntityDto
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }

        public int BuildingId { get; set; }

        public int? PropertyId { get; set; }

        public bool IsClosed { get; set; }

        public virtual BuildingDto Building { get; set; }

        public virtual PropertyDto Property { get; set; }
    }
}
