﻿namespace Refresh.Tickets
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface ITicketAppService : IAsyncCrudAppService<TicketDto, int, PagedResultRequestDto, CreateTicketDto, UpdateTicketDto>
    {
    }
}
