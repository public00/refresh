﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Auditing;
using Refresh.Sessions.Dto;

namespace Refresh.Sessions
{
    public class SessionAppService : RefreshAppServiceBase, ISessionAppService
    {
        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                Application = new ApplicationInfoDto
                {
                    Version = AppVersionHelper.Version,
                    ReleaseDate = AppVersionHelper.ReleaseDate,
                    Features = new Dictionary<string, bool>()
                }
            };

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = ObjectMapper.Map<TenantLoginInfoDto>(await GetCurrentTenantAsync());
            }

            if (AbpSession.UserId.HasValue)
            {
                output.User = ObjectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
            }

            return output;
        }

        [DisableAuditing]
        public bool IsPropertyOwner(out long userId)
        {
            var user = GetCurrentUserAsync().Result;
            userId = user?.Id ?? 0;
            return user?.UserTypeId == (int)UserTypes.PropertyOwner;
        }
    }
}
