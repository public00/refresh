﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Refresh.MultiTenancy;

namespace Refresh.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}
