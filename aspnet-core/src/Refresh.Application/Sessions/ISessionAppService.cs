﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Refresh.Sessions.Dto;

namespace Refresh.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
        bool IsPropertyOwner(out long userId);
    }
}
