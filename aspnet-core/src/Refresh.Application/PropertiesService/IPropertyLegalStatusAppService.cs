﻿namespace Refresh.PropertiesService
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IPropertyLegalStatusAppService : IAsyncCrudAppService<PropertyLegalStatusDto, int, PagedResultRequestDto, CreatePropertyLegalStatusDto, UpdatePropertyLegalStatusDto>
    {
    }
}
