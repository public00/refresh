﻿namespace Refresh.PropertiesService
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface IPropertyAppService : IAsyncCrudAppService<PropertyDto, int, PagedResultRequestDto, CreatePropertyDto, UpdatePropertyDto>
    {
    }
}
