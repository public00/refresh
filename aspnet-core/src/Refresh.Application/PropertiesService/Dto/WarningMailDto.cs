﻿namespace Refresh.PropertiesService.Dto
{
    public class WarningMailDto
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string Building { get; set; } // isto sto i Address

        public decimal Debt { get; set; }

        public string Acc_Number { get; set; }

        public string Date { get; set; }
    }
}
