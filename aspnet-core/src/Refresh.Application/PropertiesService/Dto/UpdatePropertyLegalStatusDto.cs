﻿namespace Refresh.PropertiesService.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Properties;

    [AutoMapTo(typeof(PropertyLegalStatus))]
    public class UpdatePropertyLegalStatusDto : EntityDto
    {
        [StringLength(1024)]
        public string Note { get; set; }
    }
}
