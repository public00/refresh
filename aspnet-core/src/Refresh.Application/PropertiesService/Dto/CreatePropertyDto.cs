﻿namespace Refresh.PropertiesService.Dto
{
    using Abp.AutoMapper;
    using Entities.Properties;
    using System.ComponentModel.DataAnnotations;

    [AutoMapTo(typeof(Property))]
    public class CreatePropertyDto
    {
        public int BuildingId { get; set; }
        
        public int PropertyTypeId { get; set; }

        [Required]
        [StringLength(32)]
        public string Number { get; set; }

        [Required]
        [StringLength(32)]
        public string Pd { get; set; }

        [StringLength(32)]
        public string Floor { get; set; }

        public int Quadrature { get; set; }

        //Ako je null uzima se vrijednost iz zgrade
        public decimal? PricePerSquareMeter { get; set; }

        public decimal? MaintenanceFeeFixedPrice { get; set; }

        [StringLength(32)]
        public string MaintenanceFeeMonths { get; set; }

        public decimal? Debt { get; set; }
    }
}
