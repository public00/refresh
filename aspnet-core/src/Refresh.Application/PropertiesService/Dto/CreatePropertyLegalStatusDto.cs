﻿namespace Refresh.PropertiesService.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Properties;

    [AutoMapTo(typeof(PropertyLegalStatus))]
    public class CreatePropertyLegalStatusDto
    {
        public int PropertyId { get; set; }

        [StringLength(1024)]
        public string Note { get; set; }
    }
}
