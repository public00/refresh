﻿namespace Refresh.PropertiesService.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Properties;

    [AutoMapFrom(typeof(PropertyLegalStatus))]
    public class PropertyLegalStatusDto : EntityDto
    {
        public int PropertyId { get; set; }

        [StringLength(1024)]
        public string Note { get; set; }
    }
}
