﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refresh.PropertiesService.Dto
{
    public class WarningDto
    {
        public long PropertyId { get; set; }
        public string Lang { get; set; }
    }
}
