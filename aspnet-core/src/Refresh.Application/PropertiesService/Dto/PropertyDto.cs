﻿namespace Refresh.PropertiesService.Dto
{
    using System;
    using System.Collections.Generic;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Entities.Properties;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Entities.Buildings;

    [AutoMap(typeof(Property))]
    public class PropertyDto : FullAuditedEntityDto
    {
        [ForeignKey("Building")]
        public int BuildingId { get; set; }

        [StringLength(64)]
        public string ReferenceNumber { get; set; }

        [ForeignKey("PropertyType")]
        public int PropertyTypeId { get; set; }

        [Required]
        [StringLength(32)]
        public string Number { get; set; }

        [Required]
        [StringLength(32)]
        public string Pd { get; set; }

        [StringLength(32)]
        public string Floor { get; set; }

        public int Quadrature { get; set; }
        
        public decimal? PricePerSquareMeter { get; set; }

        public decimal? MaintenanceFeeFixedPrice { get; set; }

        [StringLength(32)]
        public string MaintenanceFeeMonths { get; set; }

        public virtual Building Building { get; set; }

        public virtual PropertyType PropertyType { get; set; }

        public List<int> GetMaintenanceFeeMonths()
        {
            return string.IsNullOrEmpty(MaintenanceFeeMonths) 
                ? new List<int>() 
                : MaintenanceFeeMonths.Split(',').Select(x => Convert.ToInt32(x)).ToList();
        }
    }
}
