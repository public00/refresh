﻿namespace Refresh.PropertiesService
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Repositories;
    using Abp.Net.Mail;
    using Authorization.Users;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Properties;
    using Microsoft.AspNetCore.Mvc;
    using Refresh.Entities.Billing;
    using Refresh.Entities.Buildings;
    using Refresh.Entities.Owners;
    using Services.Export;

    [AbpAuthorize]
    public class PropertyLegalStatusAppService : RefreshCrudAppService<PropertyLegalStatus, PropertyLegalStatusDto, int, PagedResultRequestDto, CreatePropertyLegalStatusDto, UpdatePropertyLegalStatusDto>, IPropertyLegalStatusAppService
    {
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Owner> _ownerRepository;
        private readonly IRepository<PropertyOwner> _propertyOwnerRepository;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;

        public PropertyLegalStatusAppService(IRepository<PropertyLegalStatus, int> repository,
            IEmailSender emailSender,
            IRepository<Owner> ownerRepository,
            IRepository<MaintenanceFee> maintenanceFeeRepository,
            IRepository<PropertyOwner> propertyOwnerRepository,
            IRepository<Building> buildingRepository,
            IRepository<Property> propertyRepository,
            IExportService exportService,
            IRepository<User, long> userRepository,
            ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
            this._emailSender = emailSender;
            this._propertyRepository = propertyRepository;
            this._ownerRepository = ownerRepository;
            this._buildingRepository = buildingRepository;
            this._maintenanceFeeRepository = maintenanceFeeRepository;
            this._propertyOwnerRepository = propertyOwnerRepository;
        }

        [HttpPost]
        public override async Task<PropertyLegalStatusDto> Create(CreatePropertyLegalStatusDto input)
        {
            var legalStatus = await Repository.FirstOrDefaultAsync(x => x.PropertyId == input.PropertyId) ?? new PropertyLegalStatus();
            ObjectMapper.Map(input, legalStatus);
            legalStatus = await Repository.InsertOrUpdateAsync(legalStatus);
            return MapToEntityDto(legalStatus);
        }

        [HttpPost]
        public async Task<bool> SendWarning(WarningDto input)
        {
            var propertyOwners = await _propertyOwnerRepository.GetAllListAsync(x => x.PropertyId == input.PropertyId);
            var propertyOwner = propertyOwners.Select(x => x.OwnerId).FirstOrDefault();
            var owner = await _ownerRepository.GetAsync(propertyOwner);

            using (var message = new MailMessage(
                System.Configuration.ConfigurationManager.AppSettings["Email"],
                owner.Email,
                "Upozorenje",
                "Poštovani/a,<br>&nbsp;<br>U prilogu Vam dostavljam napomenu za placanje odrzavanja.<br>" +
                "&nbsp;<br>Srdačan pozdrav,<br>Refresh services doo<br>069/555-120<br>II Crnogorskog Bataljona 2/J,<br>Podgorica"))
            {
                var file = await GetAttachmentFile(input.PropertyId, input.Lang, owner);

                message.IsBodyHtml = true;
                var attachement = file;
                message.Attachments.Add(file);
                _emailSender.Send(message);
            }

            return true;
        }

        [HttpPost]
        public async Task<FileResult> DownloadWarning(WarningDto input)
        {
            var propertyOwners = await _propertyOwnerRepository.GetAllListAsync(x => x.PropertyId == input.PropertyId);
            var propertyOwner = propertyOwners.Select(x => x.OwnerId).FirstOrDefault();
            var owner = await _ownerRepository.GetAsync(propertyOwner);

            var stream = await GetStreamFile(input.PropertyId, input.Lang, owner);

            return stream == null ? null : new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        public async Task<Attachment> GetAttachmentFile(long propertyId, string lang, Owner owner)
        {
            var stream = await this.GetStreamFile(propertyId, lang, owner);
            var attachment = new Attachment(stream, "Warning.docx", "application/msword");
            attachment.ContentDisposition.FileName = "Warning.docx";

            return attachment;
        }

        public async Task<Stream> GetStreamFile(long propertyId, string lang, Owner owner)
        {
            var folderPath = RefreshConsts.TempLocation + propertyId;
            var path = @"Templates\warning";
            if (lang == "en")
            {
                path += "E";
            }
            else if (lang == "ru")
            {
                path += "R";
            }

            path += ".docx";

            var property = await _propertyRepository.GetAsync((int)propertyId);

            var name = owner.FirstName + owner.LastName;
            Directory.CreateDirectory(folderPath);
            var building = await _buildingRepository.GetAsync(property.BuildingId);
            var maintenanceFees = await _maintenanceFeeRepository.GetAllListAsync(x => x.PropertyId == propertyId && x.IsDeleted == false);
            var debt = maintenanceFees.Sum(x => x.Amount);
            var model = new WarningMailDto
            {
                Acc_Number = building.BankAccountPrimary == string.Empty ? building.BankAccountPrimary : building.BankAccountSecondary,
                Address = building.StreetAddress,
                Building = building.StreetAddress,
                Name = owner.FirstName + " " + owner.LastName,
                Debt = debt,
                Date = DateTime.Now.ToString("dd/MM/yyyy")
            };

            var outputFile = RefreshConsts.TempLocation + propertyId + "\\" + "warning-" + owner.LastName + ".docx";
            var output = new FileStream(outputFile, FileMode.Create);
            using (var input = new MemoryStream(File.ReadAllBytes(path)))
            using (output)
            using (var document = NGS.Templater.Configuration.Factory.Open(input, output, "docx"))
            {
                document.Process(model);
            }

            byte[] file = File.ReadAllBytes(outputFile);
            Stream stream = new MemoryStream(file);
            Directory.Delete(folderPath, true);

            return stream;
        }
    }
}
