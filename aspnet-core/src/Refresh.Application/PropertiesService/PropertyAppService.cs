﻿namespace Refresh.PropertiesService
{
    using System;
    using System.Collections.Generic;
    using Abp.Application.Services.Dto;
    using Abp.Domain.Repositories;
    using Entities.Properties;
    using Dto;
    using Services.Export;

    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Authorization;
    using Authorization.Users;
    using Billing;
    using Castle.Core.Logging;
    using Entities.Billing;
    using Entities.Buildings;
    using Entities.Owners;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using QueryBuilder;
    using Sessions;
    using Views;

    [AbpAuthorize]
    public class PropertyAppService : RefreshCrudAppService<Property, PropertyDto, int, PagedResultRequestDto, CreatePropertyDto, UpdatePropertyDto>, IPropertyAppService
    {
        private readonly IPropertyDebtViewQuery _propertyDebtViewQuery;
        private readonly IBalanceViewQuery _balanceViewQuery;
        private readonly ISessionAppService _sessionAppService;
        private readonly IRepository<PropertyOwner> _propertyOwnersRepository;
        private readonly IRepository<Owner> _ownersRepository;
        private readonly IRepository<Building> _buildingsRepository;
        private readonly BillGenerator _billGenerator;
        private readonly IPropertyPickerViewQuery _propertyPickerViewQuery;

        public PropertyAppService(
            IRepository<Property> repository, 
            IExportService exportService,
            IRepository<User, long> userRepository, 
            ISessionAppService sessionAppService, 
            IRepository<PropertyOwner> propertyOwnersRepository, 
            IRepository<Owner> ownersRepository, 
            IRepository<Building> buildingsRepository, 
            IBalanceViewQuery balanceViewQuery,
            BillGenerator billGenerator, 
            IPropertyDebtViewQuery propertyDebtViewQuery, 
            IPropertyPickerViewQuery propertyPickerViewQuery, 
            ILogger logger)
            : base(repository, exportService, userRepository, logger)
        {
            _billGenerator = billGenerator;
            _propertyDebtViewQuery = propertyDebtViewQuery;
            _propertyPickerViewQuery = propertyPickerViewQuery;
            _sessionAppService = sessionAppService;
            _propertyOwnersRepository = propertyOwnersRepository;
            _ownersRepository = ownersRepository;
            _buildingsRepository = buildingsRepository;
            _balanceViewQuery = balanceViewQuery;
        }

        [HttpPost]
        public override async Task<PagedResultDto<PropertyDto>> GetList(QueryInfo queryInfo)
        {
            // User may only access owned properties
            if (_sessionAppService.IsPropertyOwner(out var userId))
            {
                return await GetPropertyOwnerProperties(userId, queryInfo);
            }

            return await base.GetList(queryInfo);
        }

        private async Task<PagedResultDto<PropertyDto>> GetPropertyOwnerProperties(long userId, QueryInfo queryInfo)
        {
            var owner = await _ownersRepository.FirstOrDefaultAsync(o => o.UserId == userId);
            var propertyOwners = await _propertyOwnersRepository.GetAllListAsync(po => po.OwnerId == owner.Id);
            var propertyIds = propertyOwners.Select(x => x.PropertyId);

            try
            {
                var result = new PagedResultDto<PropertyDto>();

                var dataQuery = Repository.GetAll().Where(x => propertyIds.Contains(x.Id));
                var countQuery = Repository.GetAll().Where(x => propertyIds.Contains(x.Id));

                dataQuery = queryInfo.ApplyDataQuery(dataQuery);
                countQuery = queryInfo.ApplyCountQuery(countQuery);

                var data = await dataQuery.ToListAsync();

                result.Items = data.Select(MapToEntityDto).ToList();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpPost]
        public override async Task<PropertyDto> Create(CreatePropertyDto input)
        {
            var savedProperty = await base.Create(input);
            var result = savedProperty;
            if (savedProperty.Id <= 0)
            {
                return result;
            }

            var property = await Repository.GetAsync(savedProperty.Id);
            var building = await _buildingsRepository.GetAsync(property.BuildingId);
            property.ReferenceNumber = $"{building.PIB}/{property.Pd ?? "-"}";
            result = MapToEntityDto(await Repository.UpdateAsync(property));
            await _billGenerator.GenerateMonthlyMaintenanceFeesProperty(property);
            if (input.Debt.HasValue)
            {
                await _billGenerator.GeneratePreviousDebt(property, input.Debt.Value);
            }

            return result;
        }

        [HttpGet]
        public Property GetPropertyView(int id)
        {
            return Repository.GetAllIncluding(x => x.PropertyType, x => x.Building).FirstOrDefault(x => x.Id == id);
        }

        [HttpPost]
        public async Task<PagedResultDto<BalanceView>> GetBalanceData(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<BalanceView>();
            }

            try
            {
                var result = new PagedResultDto<BalanceView>();

                var dataQuery = _balanceViewQuery.GetQueryable();
                var countQuery = _balanceViewQuery.GetQueryable();

                // User may only access buildings of owned properties
                if (_sessionAppService.IsPropertyOwner(out var userId))
                {
                    var owner = await _ownersRepository.FirstOrDefaultAsync(o => o.UserId == userId);
                    var propertyOwners = await _propertyOwnersRepository.GetAllListAsync(po => po.OwnerId == owner.Id);
                    var propertyIds = propertyOwners.Select(x => x.PropertyId);

                    dataQuery = dataQuery.Where(x => propertyIds.Contains(x.Id));
                    countQuery = countQuery.Where(x => propertyIds.Contains(x.Id));
                }

                dataQuery = queryInfo.ApplyDataQuery(dataQuery);
                countQuery = queryInfo.ApplyCountQuery(countQuery);

                result.Items = await dataQuery.ToListAsync();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpPost]
        public async Task<PagedResultDto<PropertyDebtView>> GetPropertiesWithDebt(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<PropertyDebtView>();
            }

            ApplyCityIdFilterRule(queryInfo, "CityId");

            try
            {
                var result = new PagedResultDto<PropertyDebtView>();

                var dataQuery = _propertyDebtViewQuery.GetQueryable();
                var countQuery = _propertyDebtViewQuery.GetQueryable();

                dataQuery = dataQuery.Where(x => x.IsOverdue);
                countQuery = countQuery.Where(x => x.IsOverdue);

                dataQuery = queryInfo.ApplyDataQuery(dataQuery);
                countQuery = queryInfo.ApplyCountQuery(countQuery);

                result.Items = await dataQuery.ToListAsync();
                result.TotalCount = await countQuery.CountAsync();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        [HttpPost]
        public PagedResultDto<PropertyPickerView> GetPickerList(QueryInfo queryInfo)
        {
            if (queryInfo == null)
            {
                return new PagedResultDto<PropertyPickerView>();
            }
            try
            {
                var result = new PagedResultDto<PropertyPickerView>();

                var dataQuery = _propertyPickerViewQuery.GetDataQuery(queryInfo);
                var countQuery = _propertyPickerViewQuery.GetCountQuery(queryInfo);

                result.Items = dataQuery.ToList();
                result.TotalCount = countQuery.Count();

                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }
    } 
}
