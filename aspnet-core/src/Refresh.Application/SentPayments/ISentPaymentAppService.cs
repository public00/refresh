﻿namespace Refresh.SentPayments
{
    using Abp.Application.Services;
    using Abp.Application.Services.Dto;
    using Dto;

    public interface ISentPaymentAppService : IAsyncCrudAppService<SentPaymentDto, int, PagedResultRequestDto, CreateSentPaymentDto, UpdateSentPaymentDto>
    {
    }
}
