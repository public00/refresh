﻿namespace Refresh.SentPayments.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.AutoMapper;
    using Entities.Billing;

    [AutoMapTo(typeof(SentPayment))]
    public class CreateSentPaymentDto 
    {
        public int BuildingId { get; set; }

        public int ExpenseId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [StringLength(128)]
        public string BankStatementNumber { get; set; }

        public decimal Amount { get; set; }
    }
}
