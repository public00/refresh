﻿namespace Refresh.SentPayments.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Application.Services.Dto;
    using Abp.AutoMapper;
    using Buildings.Dto;
    using Entities.Billing;
    using Expenses.Dto;

    [AutoMap(typeof(SentPayment))]
    public class SentPaymentDto : FullAuditedEntityDto
    {
        public int BuildingId { get; set; }

        public int ExpenseId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [StringLength(128)]
        public string BankStatementNumber { get; set; }

        public decimal Amount { get; set; }

        public virtual BuildingDto Building { get; set; }

        public virtual ExpenseDto Expense { get; set; }
    }
}
