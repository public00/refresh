﻿namespace Refresh.SentPayments
{
    using System;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Authorization;
    using Abp.Domain.Entities;
    using Abp.Domain.Repositories;
    using Authorization.Users;
    using Billing;
    using Castle.Core.Logging;
    using Dto;
    using Entities.Billing;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Services.Export;

    [AbpAuthorize]
    public class SentPaymentAppService : RefreshCrudAppService<SentPayment, SentPaymentDto, int, PagedResultRequestDto, CreateSentPaymentDto, UpdateSentPaymentDto>, ISentPaymentAppService
    {
        private readonly PaymentManager _paymentManager;

        public SentPaymentAppService(
            IRepository<SentPayment, int> repository, 
            IExportService exportService, 
            PaymentManager paymentManager, 
            IRepository<User, long> userRepository,
            ILogger logger) 
            : base(repository, exportService, userRepository, logger)
        {
            _paymentManager = paymentManager;
        }

        [HttpGet]
        public override async Task<SentPaymentDto> Get(EntityDto<int> input)
        {
            var building = await Repository.GetAllIncluding(x => x.Building, x => x.Expense).FirstOrDefaultAsync(x => x.Id == input.Id);
            if (building == null)
            {
                throw new EntityNotFoundException(typeof(SentPayment), input.Id);
            }

            return MapToEntityDto(building);
        }

        [HttpPost]
        public override async Task<SentPaymentDto> Create(CreateSentPaymentDto input)
        {
            var payment = ObjectMapper.Map<SentPayment>(input);

            payment = await _paymentManager.SaveSentPayment(payment);

            return MapToEntityDto(payment);
        }

        [HttpPut]
        public override Task<SentPaymentDto> Update(UpdateSentPaymentDto input)
        {
            throw new InvalidOperationException("Updating payments is not allowed.");
        }
    }
}
