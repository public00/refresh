using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Refresh.Controllers
{
    public abstract class RefreshControllerBase: AbpController
    {
        protected RefreshControllerBase()
        {
            LocalizationSourceName = RefreshConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
