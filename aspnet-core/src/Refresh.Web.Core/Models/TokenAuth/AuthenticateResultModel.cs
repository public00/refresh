﻿namespace Refresh.Models.TokenAuth
{
    public class AuthenticateResultModel
    {
        public string AccessToken { get; set; }

        public string EncryptedAccessToken { get; set; }

        public int ExpireInSeconds { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public int UserTypeId { get; set; }

        public int? CityId { get; set; }
    }
}
