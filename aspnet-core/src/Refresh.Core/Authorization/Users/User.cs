﻿namespace Refresh.Authorization.Users
{
    using Abp.Authorization.Users;
    using Abp.Extensions;
    using System;

    public class User : AbpUser<User>
    {
        public int UserTypeId { get; set; }

        public int? CityId { get; set; }

        public const string DefaultPassword = "123qwe";

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                UserTypeId = (int)UserTypes.SystemAdministrator
            };

            user.SetNormalizedNames();

            return user;
        }
    }
}
