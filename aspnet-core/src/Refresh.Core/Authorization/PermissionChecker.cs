﻿using Abp.Authorization;
using Refresh.Authorization.Roles;
using Refresh.Authorization.Users;

namespace Refresh.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
