﻿namespace Refresh
{
    public class RefreshConsts
    {
        public const string LocalizationSourceName = "Refresh";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const string MaintenanceFeeTitle = "Održavanje ulaza";

        public const string MaintenanceFeeDescription = "Mjesečni generisani račun";

        public const string PreviousDebtTitle = "Prethodni dug";

        public const string TempLocation = @"Temp\";
    }
}
