﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Refresh.Entities.Billing;
namespace Refresh.Billing
{
    using System;
    using System.Collections.Generic;
    using Entities.Buildings;
    using Entities.Properties;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Domain.Entities;
    using Abp.Domain.Uow;
    using Castle.Core.Logging;

    public class PaymentManager : ITransientDependency
    {
        private readonly ILogger _logger;
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<ReceivedPayment> _recievedPaymentRepository;
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;
        private readonly IRepository<MaintenancePaymentTracker> _maintenancePaymentTrackerRepository;
        private readonly IRepository<ReceivedPayment> _receivedPaymentRepository;
        private readonly IRepository<Expense> _expenseRepository;
        private readonly IRepository<SentPayment> _sentPaymentRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PaymentManager(IRepository<Property> propertyRepository,
            IRepository<Building> buildingRepository,
            IRepository<ReceivedPayment> recievedPaymentRepository,
            IRepository<MaintenanceFee> maintenanceFeeRepository,
            IRepository<MaintenancePaymentTracker> maintenancePaymentTrackerRepository, 
            IRepository<Expense> expenseRepository, 
            IRepository<SentPayment> sentPaymentRepository, 
            IRepository<ReceivedPayment> receivedPaymentRepository, 
            ILogger logger, 
            IUnitOfWorkManager unitOfWorkManager)
        {
            _propertyRepository = propertyRepository;
            _buildingRepository = buildingRepository;
            _recievedPaymentRepository = recievedPaymentRepository;
            _maintenanceFeeRepository = maintenanceFeeRepository;
            _maintenancePaymentTrackerRepository = maintenancePaymentTrackerRepository;
            _expenseRepository = expenseRepository;
            _sentPaymentRepository = sentPaymentRepository;
            _receivedPaymentRepository = receivedPaymentRepository;
            _logger = logger;
            _unitOfWorkManager = unitOfWorkManager;
        }

        /// <summary>
        /// Saves payment.
        /// Inserts payment trackers and updates maintenance fee if paid full.
        /// Generates future maintenance fees if user overpaid.,
        /// </summary>
        public async Task SaveReceivedPaymentTrackers(ReceivedPayment payment)
        {
            if (!payment.PropertyId.HasValue)
            {
                return;
            }

            var property = await _propertyRepository.GetAsync(payment.PropertyId ?? 0);

            var maintenanceFees =
                _maintenanceFeeRepository.GetAll()
                    .Where(x => x.PropertyId == property.Id && !x.IsFinishedPayment)
                    .OrderBy(x => x.Year)
                    .ThenBy(x => x.Month)
                    .ToList();

            var paymentAmount = payment.Amount;

            foreach (var maintenanceFee in maintenanceFees)
            {
                var amountAlreadyPaid =
                    _maintenancePaymentTrackerRepository.GetAll()
                        .Where(x => x.MaintenanceFeeId == maintenanceFee.Id)
                        .Select(x => x.Amount)
                        .DefaultIfEmpty(0)
                        .Sum();

                var toPay = maintenanceFee.Amount - amountAlreadyPaid;
                if (toPay == 0)
                {
                    maintenanceFee.IsFinishedPayment = true;
                    await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
                    continue;
                }

                var paymentTracker =
                    new MaintenancePaymentTracker
                    {
                        PaymentId = payment.Id,
                        MaintenanceFeeId = maintenanceFee.Id,
                        Amount = paymentAmount > toPay ? toPay : paymentAmount
                    };

                paymentAmount -= paymentTracker.Amount;
                await _maintenancePaymentTrackerRepository.InsertAsync(paymentTracker);

                if (paymentTracker.Amount == toPay)
                {
                    maintenanceFee.IsFinishedPayment = true;
                    await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
                }

                if (paymentAmount == 0)
                {
                    break;
                }
            }
        }

        public async Task UpdatePaymentTrackers(ReceivedPayment payment)
        {
            if (!payment.PropertyId.HasValue)
            {
                return;
            }

            var trackers = await _maintenancePaymentTrackerRepository.GetAllListAsync(x => x.PaymentId == payment.Id);
            foreach (var paymentTracker in trackers)
            {
                await _maintenancePaymentTrackerRepository.DeleteAsync(paymentTracker);
                var maintenanceFee = await _maintenanceFeeRepository.GetAsync(paymentTracker.MaintenanceFeeId);
                maintenanceFee.IsFinishedPayment = false;
                await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
            }

            _unitOfWorkManager.Current.SaveChanges();

            var property = await _propertyRepository.GetAsync(payment.PropertyId.Value);
            await SyncPropertyPayments(property);
        }

        public async Task UpdatePaymentTrackers(MaintenanceFee maintenanceFee)
        {
            var trackers = await _maintenancePaymentTrackerRepository.GetAllListAsync(x => x.MaintenanceFeeId == maintenanceFee.Id);
            foreach (var paymentTracker in trackers)
            {
                await _maintenancePaymentTrackerRepository.DeleteAsync(paymentTracker);
                maintenanceFee.IsFinishedPayment = false;
                await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
            }

            _unitOfWorkManager.Current.SaveChanges();

            var property = await _propertyRepository.GetAsync(maintenanceFee.PropertyId);
            await SyncPropertyPayments(property);
        }

        /// <summary>
        /// Deletes the payment along with related payment trackers.
        /// Updates maintenance fees as non finished.
        /// </summary>
        /// <param name="input"></param>
        public async Task DeleteReceivedPayment(EntityDto<int> input)
        {
            var payment = await _recievedPaymentRepository.GetAsync(input.Id);
            if (payment == null)
            {
                throw new EntityNotFoundException();
            }

            var paymentTrackers = await _maintenancePaymentTrackerRepository.GetAllListAsync(x => x.PaymentId == input.Id);
            var maintenanceFeeIds = paymentTrackers.Select(p => p.MaintenanceFeeId);
            var maintenanceFees = await _maintenanceFeeRepository.GetAllListAsync(x => maintenanceFeeIds.Contains(x.Id));
            foreach (var paymentTracker in paymentTrackers)
            {
                await _maintenancePaymentTrackerRepository.DeleteAsync(paymentTracker.Id);
            }

            foreach (var maintenanceFee in maintenanceFees)
            {
                maintenanceFee.IsFinishedPayment = false;
                await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
            }

            await _recievedPaymentRepository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// Deletes payment trackers for a maintenance fee.
        /// </summary>
        /// <param name="maintenanceFeeId"></param>
        /// <returns></returns>
        public async Task DeletePaymentTrackers(int maintenanceFeeId)
        {
            var paymentTrackers =
                await _maintenancePaymentTrackerRepository.GetAllListAsync(x => x.MaintenanceFeeId == maintenanceFeeId);
            foreach (var maintenancePaymentTracker in paymentTrackers)
            {
                await _maintenancePaymentTrackerRepository.DeleteAsync(maintenancePaymentTracker.Id);
            }
        }

        /// <summary>
        /// Prevent overpayments and save if payment is valid.
        /// </summary>
        /// <param name="payment"></param>
        /// <returns>Saved payment entity.</returns>
        public async Task<SentPayment> SaveSentPayment(SentPayment payment)
        {
            var expense = await _expenseRepository.GetAsync(payment.ExpenseId);
            var payments = await _sentPaymentRepository.GetAllListAsync(x => x.ExpenseId == expense.Id);
            var alreadyPaid = payments.Select(p => p.Amount).DefaultIfEmpty(0).Sum();
            if (expense.Amount - alreadyPaid < payment.Amount)
            {
                throw new InvalidOperationException("Not allowed to overpay an expense.");
            }

            if (expense.Amount == alreadyPaid + payment.Amount)
            {
                expense.IsFinishedPayment = true;
                await _expenseRepository.UpdateAsync(expense);
            }

            payment = await _sentPaymentRepository.InsertAsync(payment);
            return payment;
        }


        /// <summary>
        /// Synchronize payments with maintenance fees - generate trackers.
        /// </summary>
        public async Task SyncPayments()
        {
            var buildings = await _buildingRepository.GetAllListAsync();
            var count = buildings.Count;
            foreach (var building in buildings)
            {
                Console.WriteLine($"Payment sync building:{building.Id}/{building.PIB} STARTED.");
                _logger.Info($"Payment sync building:{building.Id}/{building.PIB} STARTED.");
                var properties = await _propertyRepository.GetAllListAsync(x => x.BuildingId == building.Id);
                foreach (var property in properties)
                {
                    if (_maintenanceFeeRepository.GetAll().Where(x => x.PropertyId == property.Id).Sum(x => x.Amount)
                        <= _receivedPaymentRepository.GetAll().Where(x => x.PropertyId == property.Id).Sum(x => x.Amount)
                        && _maintenanceFeeRepository.GetAll().Any(x => x.PropertyId == property.Id && !x.IsFinishedPayment))
                    {
                        await SyncPropertyPayments(property);
                    }
                }

                Console.WriteLine($"Payment sync building:{building.Id}/{building.PIB} FINISHED.");
                _logger.Info($"Payment sync building:{building.Id}/{building.PIB} FINISHED.");
                Console.WriteLine($"{--count} to go!");
                _logger.Info($"{count} to go!");
            }
        }

        public async Task SyncPropertyPayments(Property property)
        {
            Console.WriteLine($"START Property:{property.ReferenceNumber}");
            _logger.Info($"START Property:{property.ReferenceNumber}");

            var maintenanceFees =
                await _maintenanceFeeRepository.GetAllListAsync(
                    x => x.PropertyId == property.Id && !x.IsFinishedPayment);
            if (!maintenanceFees.Any())
            {
                Console.WriteLine($"Property:{property.ReferenceNumber} FINISHED - No Maintenance fees");
                _logger.Info($"Property:{property.ReferenceNumber} FINISHED - No Maintenance fees");
                return;
            }

            var payments = await _receivedPaymentRepository.GetAllListAsync(x => x.PropertyId == property.Id);
            var availablePayments = new List<ReceivedPayment>();
            foreach (var payment in payments)
            {
                var paymentTrackers =
                    await _maintenancePaymentTrackerRepository.GetAllListAsync(x => x.PaymentId == payment.Id);
                if (paymentTrackers.Sum(x => x.Amount) < payment.Amount)
                {
                    availablePayments.Add(payment);
                }
            }

            if (!availablePayments.Any())
            {
                Console.WriteLine($"Property:{property.ReferenceNumber} FINISHED - No available payments");
                _logger.Info($"Property:{property.ReferenceNumber} FINISHED - No available payments");
                return;
            }

            foreach (var availablePayment in availablePayments)
            {
                var trackers =
                    await _maintenancePaymentTrackerRepository.GetAllListAsync(x =>
                        x.PaymentId == availablePayment.Id);
                var alreadyPaid = trackers.Sum(x => x.Amount);
                var remainingAmount = availablePayment.Amount - alreadyPaid;
                if (remainingAmount == 0)
                {
                    continue;
                }

                foreach (var maintenanceFee in maintenanceFees.OrderBy(x => x.Year).ThenBy(x => x.Month))
                {
                    if (remainingAmount == 0)
                    {
                        break;
                    }

                    var maintenanceFeeTrackers =
                        await _maintenancePaymentTrackerRepository.GetAllListAsync(x =>
                            x.MaintenanceFeeId == maintenanceFee.Id);
                    var feePaid = maintenanceFeeTrackers.Sum(x => x.Amount);
                    var toPay = maintenanceFee.Amount - feePaid;
                    if (toPay == 0)
                    {
                        maintenanceFee.IsFinishedPayment = true;
                        await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
                        continue;
                    }

                    var trackerAmount = remainingAmount < toPay ? remainingAmount : toPay;
                    var newTracker = new MaintenancePaymentTracker
                    {
                        Amount = trackerAmount,
                        PaymentId = availablePayment.Id,
                        MaintenanceFeeId = maintenanceFee.Id
                    };

                    await _maintenancePaymentTrackerRepository.InsertAsync(newTracker);
                    remainingAmount -= trackerAmount;
                    if (trackerAmount == toPay)
                    {
                        maintenanceFee.IsFinishedPayment = true;
                        await _maintenanceFeeRepository.UpdateAsync(maintenanceFee);
                    }
                }

                if (remainingAmount > 0)
                {
                    break;
                }
            }

            Console.WriteLine($"Property:{property.ReferenceNumber} FINISHED");
            _logger.Info($"Property:{property.ReferenceNumber} FINISHED");
        }
    }
}
