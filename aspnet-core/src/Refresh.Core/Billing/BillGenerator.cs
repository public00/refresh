﻿namespace Refresh.Billing
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Dependency;
    using Abp.Domain.Repositories;
    using Castle.Core.Logging;
    using Entities.Billing;
    using Entities.Buildings;
    using Entities.Properties;

    public class BillGenerator : ITransientDependency
    {
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;
        private readonly IRepository<MaintenanceFeeCategory> _maintenanceFeeCategoryRepository;
        private readonly IRepository<ScheduledMaintenanceFee> _scheduledFeeRepository;
        private readonly IRepository<ScheduledExpense> _scheduledExpenseRepository;
        private readonly IRepository<Expense> _expenseRepository;
        private readonly ILogger _logger;

        public BillGenerator(
            IRepository<Building> buildingRepository, 
            IRepository<Property> propertyRepository, 
            IRepository<MaintenanceFee> maintenanceFeeRepository, 
            IRepository<MaintenanceFeeCategory> maintenanceFeeCategoryRepository, 
            IRepository<ScheduledMaintenanceFee> scheduledFeeRepository, 
            IRepository<ScheduledExpense> scheduledExpenseRepository, 
            IRepository<Expense> expenseRepository, ILogger logger)
        {
            _buildingRepository = buildingRepository;
            _propertyRepository = propertyRepository;
            _maintenanceFeeRepository = maintenanceFeeRepository;
            _maintenanceFeeCategoryRepository = maintenanceFeeCategoryRepository;
            _scheduledFeeRepository = scheduledFeeRepository;
            _scheduledExpenseRepository = scheduledExpenseRepository;
            _expenseRepository = expenseRepository;
            _logger = logger;
        }

        /// <summary>
        /// Monthly maintenance fee per building
        /// </summary>
        public async Task GenerateMonthlyMaintenanceFeesBuilding(int buildingId)
        {
            var properties = await _propertyRepository.GetAllListAsync(x => x.BuildingId == buildingId);
            foreach (var property in properties)
            {
                await GenerateMonthlyMaintenanceFeesProperty(property);
            }
        }

        /// <summary>
        /// Monthly maintenance fee per property
        /// </summary>
        public async Task GenerateMonthlyMaintenanceFeesProperty(Property property)
        {
            var building = await _buildingRepository.GetAsync(property.BuildingId);
            if (building == null)
            {
                _logger.Error($"GenerateMonthlyMaintenanceFeesProperty - can't get buidling ID:{property.BuildingId}");
                return;
            }

            var feeAmmount = property.MaintenanceFeeFixedPrice ?? (property.PricePerSquareMeter ?? building.PricePerSquareMeter) * property.Quadrature;

            var maintenanceFees = 
                await _maintenanceFeeRepository.GetAllListAsync(
                    x => x.Title == RefreshConsts.MaintenanceFeeTitle && x.PropertyId == property.Id);
            var lastMaintenanceFee = maintenanceFees.OrderBy(x => x.Year).ThenBy(x => x.Month).LastOrDefault();

            var month = lastMaintenanceFee == null ? building.ContractStartDate.Month : lastMaintenanceFee.Month + 1;
            var year = lastMaintenanceFee == null ? building.ContractStartDate.Year : lastMaintenanceFee.Year;
            if (month > 12)
            {
                month = 1;
                year += 1;
            }

            var monthlyMaintenanceCategory = _maintenanceFeeCategoryRepository.FirstOrDefault(x => true);

            var allowedMonths = property.GetMaintenanceFeeMonths();
            var feeDate = new DateTime(year, month, 1);
            while (feeDate < DateTime.Now)
            {
                if (!allowedMonths.Contains(month))
                {
                    month++;
                    continue;
                }

                var maintenanceFee = new MaintenanceFee
                {
                    Title = RefreshConsts.MaintenanceFeeTitle,
                    Description = RefreshConsts.MaintenanceFeeDescription,
                    CategoryId = monthlyMaintenanceCategory.Id,
                    PropertyId = property.Id,
                    Year = feeDate.Year,
                    Month = feeDate.Month,
                    Amount = feeAmmount
                };

                await _maintenanceFeeRepository.InsertAsync(maintenanceFee);
                feeDate = feeDate.AddMonths(1);
            }
        }

        /// <summary>
        /// Creates previous debt for a property.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="debt"></param>
        public async Task GeneratePreviousDebt(Property property, decimal debt)
        {
            var monthlyMaintenanceCategory = _maintenanceFeeCategoryRepository.FirstOrDefault(x => true);
            var building = await _buildingRepository.GetAsync(property.BuildingId);

            // Debt month should be before the ContractStartDate
            var feeDate = building.ContractStartDate.AddMonths(-1);

            var fee = new MaintenanceFee
            {
                Title = RefreshConsts.PreviousDebtTitle,
                Description = string.Empty,
                CategoryId = monthlyMaintenanceCategory.Id,
                PropertyId = property.Id,
                Month = feeDate.Month,
                Year = feeDate.Year,
                Amount = debt
            };

            await _maintenanceFeeRepository.InsertAsync(fee);
        }

        /// <summary>
        /// Scheduled maintenance fee per building
        /// </summary>
        public async Task GenerateScheduledFeesBuilding(int buildingId)
        {
            var properties = await _propertyRepository.GetAllListAsync(x => x.BuildingId == buildingId);
            foreach (var property in properties)
            {
                await GenerateScheduledFeesProperty(property);
            }
        }

        /// <summary>
        /// Scheduled maintenance fee per property
        /// </summary>
        public async Task GenerateScheduledFeesProperty(Property property)
        {
            var now = DateTime.UtcNow;
            var scheduledFees = await _scheduledFeeRepository.GetAllListAsync(x => x.PropertyId == property.Id);
            foreach (var scheduledFee in scheduledFees)
            {
                var scheduledMonths = scheduledFee.GetScheduledMonths();
                if (!scheduledMonths.Contains(now.Month)
                    || scheduledFee.LastRunDate.HasValue
                    && scheduledFee.LastRunDate.Value.Year != now.Year
                    && scheduledFee.LastRunDate.Value.Month != now.Month)
                {
                    continue;
                }

                var maintenanceFee = new MaintenanceFee
                {
                    Title = scheduledFee.Title,
                    Description = scheduledFee.Description,
                    CategoryId = scheduledFee.CategoryId,
                    PropertyId = property.Id,
                    Year = now.Year,
                    Month = now.Month,
                    Amount = scheduledFee.Amount
                };
                await _maintenanceFeeRepository.InsertAsync(maintenanceFee);

                scheduledFee.LastRunDate = now;
                await _scheduledFeeRepository.UpdateAsync(scheduledFee);
            }
        }

        /// <summary>
        /// Scheduled expenses for building
        /// </summary>
        public async Task GenerateScheduledExpenses(int buildingId)
        {
            var now = DateTime.UtcNow;
            var scheduledExpenses = await _scheduledExpenseRepository.GetAllListAsync(x => x.BuildingId == buildingId);
            foreach (var scheduledExpense in scheduledExpenses)
            {
                var scheduledMonths = scheduledExpense.GetScheduledMonths();
                if (!scheduledMonths.Contains(now.Month)
                    || scheduledExpense.LastRunDate.HasValue
                    && scheduledExpense.LastRunDate.Value.Year != now.Year
                    && scheduledExpense.LastRunDate.Value.Month != now.Month)
                {
                    continue;
                }

                var expense = new Expense
                {
                    Title = scheduledExpense.Title,
                    Description = scheduledExpense.Description,
                    CategoryId = scheduledExpense.CategoryId,
                    SubCategoryId = scheduledExpense.SubCategoryId,
                    BuildingId = buildingId,
                    Company = scheduledExpense.Company,
                    Amount = scheduledExpense.Amount
                };
                await _expenseRepository.InsertAsync(expense);

                scheduledExpense.LastRunDate = now;
                await _scheduledExpenseRepository.UpdateAsync(scheduledExpense);
            }
        }
    }
}
