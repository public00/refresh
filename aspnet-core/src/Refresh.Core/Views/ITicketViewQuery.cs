﻿namespace Refresh.Views
{
    using Refresh.Entities.Tickets;

    public interface ITicketViewQuery : IDbQueryBase<TicketView>
    {
    }
}
