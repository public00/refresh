﻿namespace Refresh.Views
{
    using Entities.Properties;
    public interface IPropertyOwnerViewQuery : IDbQueryBase<PropertyOwnerView>
    {
    }
}
