﻿namespace Refresh.Views
{
    using Entities.Properties;
    public interface IPropertyPickerViewQuery : IDbQueryBase<PropertyPickerView>
    {
    }
}
