﻿namespace Refresh.Views
{
    using Entities.Billing.Invoices;

    public interface IInvoiceViewQuery : IDbQueryBase<InvoiceView>
    {
    }
}
