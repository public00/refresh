﻿namespace Refresh.Views
{
    using Entities.Properties;

    public interface IPropertyDebtViewQuery : IDbQueryBase<PropertyDebtView>
    {
    }
}
