﻿namespace Refresh.Views
{
    using Entities.Billing;

    public interface IBalanceViewQuery : IDbQueryBase<BalanceView>
    {
    }
}
