﻿namespace Refresh.Views
{
    using System.Linq;
    using System.Threading.Tasks;
    using Abp.Application.Services.Dto;
    using Abp.Dependency;
    using QueryBuilder;

    public interface IDbQueryBase<TView> : ITransientDependency where TView : class, IEntityDto
    {
        IQueryable<TView> GetQueryable();

        IQueryable<TView> GetDataQuery(QueryInfo queryInfo);

        IQueryable<TView> GetCountQuery(QueryInfo queryInfo);

        Task<TView> Get(int id);
    }
}
