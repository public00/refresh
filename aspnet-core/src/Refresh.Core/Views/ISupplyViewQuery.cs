﻿namespace Refresh.Views
{
    using Refresh.Entities.Reports;

    public interface ISupplyViewQuery : IDbQueryBase<SupplyView>
    {
    }
}
 