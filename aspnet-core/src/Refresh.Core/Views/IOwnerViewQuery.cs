﻿namespace Refresh.Views
{
    using Refresh.Entities.Owners;

    public interface IOwnerViewQuery : IDbQueryBase<OwnerView>
    {
    }
}
