﻿using Refresh.Entities.Reports;

namespace Refresh.Views
{
    public interface IReportViewQuery : IDbQueryBase<ReportView>
    {
    }
}
