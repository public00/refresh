﻿namespace Refresh.Views
{
    using Entities.Buildings;

    public interface IBuildingViewQuery : IDbQueryBase<BuildingView>
    {
    }
}
