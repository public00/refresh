﻿using Abp.MultiTenancy;
using Refresh.Authorization.Users;

namespace Refresh.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
