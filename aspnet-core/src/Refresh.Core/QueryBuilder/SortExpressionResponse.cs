﻿namespace Refresh.QueryBuilder
{
    using System;
    using System.Linq.Expressions;

    public class SortExpressionResponse<T>
    {
        public string Direction { get; set; }

        public Expression<Func<T, object>> SortExpression { get; set; }
    }
}
