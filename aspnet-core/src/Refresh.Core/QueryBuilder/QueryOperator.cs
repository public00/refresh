﻿namespace Refresh.QueryBuilder
{
    public class QueryOperator
    {
        public const string BeginsWith = "begins_with";

        public const string Between = "between";

        public const string Contains = "contains";

        public const string DoesntBeginWith = "not_begins_with";

        public const string DoesntContain = "not_contains";

        public const string DoesntEndWith = "not_ends_with";

        public const string EndsWith = "ends_with";

        public const string Equal = "equal";

        public const string Greater = "greater";

        public const string GreaterOrEqual = "greater_or_equal";

        public const string IdEqual = "ideq";

        public const string In = "in";

        public const string Is = "is";

        public const string IsEmpty = "is_empty";

        public const string IsNotEmpty = "is_not_empty";

        public const string IsNotNull = "is_not_null";

        public const string IsNull = "is_null";

        public const string Less = "less";

        public const string LessOrEqual = "less_or_equal";

        public const string NotEqual = "not_equal";

        public const string NotIn = "not_in";
    }
}
