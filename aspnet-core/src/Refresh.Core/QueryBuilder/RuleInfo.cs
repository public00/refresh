﻿namespace Refresh.QueryBuilder
{
    using System.Collections.Generic;

    public class RuleInfo
    {
        public RuleInfo()
        {
            Rules = new List<RuleInfo>();
        }

        public string Condition { get; set; }

        public string Field { get; set; }

        public string Operator { get; set; }

        public object Value { get; set; }

        public IList<RuleInfo> Rules { get; set; }
    }
}
