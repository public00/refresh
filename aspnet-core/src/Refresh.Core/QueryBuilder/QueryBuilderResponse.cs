﻿namespace Refresh.QueryBuilder
{
    using System;
    using System.Linq.Expressions;

    public class QueryBuilderResponse<T>
    {
        public QueryBuilderResponse(QueryInfo queryInfo)
        {
            WhereExpression = queryInfo.GetWhereExpression<T>();
            if (queryInfo.Sorter != null)
            {
                Sorter = new SortExpressionResponse<T>
                {
                    Direction = queryInfo.Sorter.Dir,
                    SortExpression = queryInfo.Sorter.GetMemberExpression<T>()
                };
            }
            
            Skip = queryInfo.Skip ?? 0;
            Take = queryInfo.Take ?? 20;
        }

        public int? Skip { get; set; }

        public SortExpressionResponse<T> Sorter { get; set; }

        public int? Take { get; set; }

        public Expression<Func<T, bool>> WhereExpression { get; set; }
    }
}
