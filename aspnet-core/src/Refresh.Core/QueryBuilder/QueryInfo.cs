﻿namespace Refresh.QueryBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using Services.Export;

    public class QueryInfo
    {
        public QueryInfo()
        {
            FilterRule = new FilterRule();
            SearchableColumns = new List<string>();
        }

        public FilterRule FilterRule { get; set; }

        public IList<string> SearchableColumns { get; [UsedImplicitly] set; }

        public string SearchText { get; [UsedImplicitly] set; }

        public SortInfo Sorter { get; set; }

        public int? Skip { get; set; }

        public int? Take { get; set; }

        public IList<ExportColumnInfo> ExportColumns { get; set; }

        public static Expression ConstructExpression(MemberExpression left, UnaryExpression right, string queryOperator)
        {
            Expression exp;
            switch (queryOperator)
            {
                case QueryOperator.Equal:
                    exp = Expression.Equal(left, right);
                    break;

                case QueryOperator.NotEqual:
                    exp = Expression.NotEqual(left, right);
                    break;

                case QueryOperator.LessOrEqual:
                    exp = Expression.LessThanOrEqual(left, right);
                    break;

                case QueryOperator.GreaterOrEqual:
                    exp = Expression.GreaterThanOrEqual(left, right);
                    break;

                case QueryOperator.Less:
                    exp = Expression.LessThan(left, right);
                    break;

                case QueryOperator.Greater:
                    exp = Expression.GreaterThan(left, right);
                    break;

                case QueryOperator.Contains:
                    var method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                    exp = Expression.Call(left, method, right);
                    break;

                case QueryOperator.IsNull:
                    exp = Expression.Equal(left, Expression.Default(left.Type));
                    break;

                case QueryOperator.IsNotNull:
                    exp = Expression.NotEqual(left, Expression.Default(left.Type));
                    break;
                default:
                    throw new NotImplementedException($"The {queryOperator} rule operator is not implemented yet.");
            }

            return exp;
        }

        public static object GetValueBasedOnType(Type type, RuleInfo rule)
        {
            if (rule.Value == null || string.IsNullOrEmpty(rule.Field))
            {
                return null;
            }

            var propertyType = type;
            foreach (var field in rule.Field.Split("."))
            {
                var prop = propertyType.GetProperty(
                    field,
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (prop == null)
                {
                    return null;
                }

                propertyType = prop.PropertyType;
            }

            var value = rule.Value.ToString();

            if (propertyType == typeof(string))
            {
                return value;
            }

            if ((propertyType == typeof(int) || propertyType == typeof(int?)) 
                && int.TryParse(value, out var intResult))
            {
                return intResult;
            }

            if ((propertyType == typeof(bool) || propertyType == typeof(bool?)) 
                && bool.TryParse(value, out var boolResult))
            {
                return boolResult;
            }

            if ((propertyType == typeof(DateTime) || propertyType == typeof(DateTime?)) 
                && DateTime.TryParse(value, out var dateTimeResult))
            {
                return dateTimeResult;
            }

            if ((propertyType == typeof(decimal) || propertyType == typeof(decimal?))
                && decimal.TryParse(value, out var decimalResult))
            {
                return decimalResult;
            }

            if ((propertyType == typeof(Guid) || propertyType == typeof(Guid?))
                && Guid.TryParse(value, out var guidResult))
            {
                return guidResult;
            }

            return null;
        }

        public Expression<Func<T, bool>> GetWhereExpression<T>()
        {
            var type = typeof(T);
            var pe = Expression.Parameter(typeof(T), "x");
            Expression expression = null;

            var filterExpression = ParseFilterRules(type, pe, FilterRule, null, QueryCondition.And);
            if (filterExpression != null)
            {
                expression = filterExpression;
            }

            if (!string.IsNullOrEmpty(SearchText))
            {
                var searchExpression = GetSearchExpression<T>(type, pe);

                if (searchExpression != null)
                {
                    expression = expression != null
                                     ? Expression.AndAlso(expression, searchExpression)
                                     : searchExpression;
                }
            }

            return expression != null ? Expression.Lambda<Func<T, bool>>(expression, pe) : null;
        }

        public QueryBuilderResponse<T> ToQueryBuilderResponse<T>()
        {
            var result = new QueryBuilderResponse<T>(this);
            return result;
        }

        protected static MemberExpression GetLeftExpression(Type type, Expression pe, string ruleField)
        {
            MemberExpression retVal = null;
            var lastType = type;
            foreach (var field in ruleField.Split("."))
            {
                var prop = lastType.GetProperty(
                    field,
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (prop == null)
                {
                    throw new Exception($"Unable to resolve property {field} in type {type.Name}");
                }

                lastType = prop.PropertyType;
                retVal = Expression.PropertyOrField(retVal ?? pe, field);
            }

            return retVal;
        }

        protected static Expression ParseFilterRules(
            Type type,
            Expression pe,
            FilterRule filter,
            Expression initialExpression,
            string queryCondition)
        {
            if (filter == null)
            {
                return initialExpression;
            }

            Expression innerExpression = null;
            foreach (var rule in filter.Rules)
            {
                var left = GetLeftExpression(type, pe, rule.Field);
                var value = GetValueBasedOnType(type, rule);
                var constant = Expression.Constant(value);
                var right = Expression.Convert(constant, left.Type);
                Expression exp;
                if (rule.Rules.Any())
                {
                    exp = ParseFilterRules(
                        type,
                        pe,
                        new FilterRule { Rules = rule.Rules },
                        innerExpression,
                        rule.Condition);
                }
                else
                {
                    var propType = 
                        type.GetProperty(
                            rule.Field, 
                            BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)?.PropertyType;
                    if (propType == null)
                    {
                        continue;
                    }

                    exp = ConstructExpression(left, right, rule.Operator);
                }

                innerExpression = innerExpression == null
                                      ? exp
                                      : (filter.Condition == QueryCondition.And
                                             ? Expression.AndAlso(innerExpression, exp)
                                             : Expression.OrElse(innerExpression, exp));
            }

            if (innerExpression == null)
            {
                return initialExpression;
            }

            initialExpression = initialExpression == null
                                    ? innerExpression
                                    : (queryCondition == QueryCondition.And
                                           ? Expression.AndAlso(initialExpression, innerExpression)
                                           : Expression.OrElse(initialExpression, innerExpression));

            return initialExpression;
        }

        protected static Expression<Func<T, bool>> True<T>()
        {
            return param => true;
        }

        protected Expression GetSearchExpression<T>(Type type, Expression pe)
        {
            Expression searchExpression = null;
            if (string.IsNullOrEmpty(SearchText))
            {
                return null;
            }

            // Adding all string properties as searchable columns in case none is passed from client
            if (!SearchableColumns.Any())
            {
                SearchableColumns = typeof(T).GetProperties().Where(x => x.PropertyType == typeof(string)).Select(x => x.Name).ToList();
            }

            foreach (var column in SearchableColumns)
            {
                var prop = type.GetProperty(
                    column,
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (prop == null || prop.PropertyType != typeof(string))
                {
                    continue;
                }

                var left = Expression.Property(pe, prop);
                var value = SearchText;
                var constant = Expression.Constant(value);
                var right = Expression.Convert(constant, left.Type);
                var valueExp = ConstructExpression(left, right, QueryOperator.Contains);
                searchExpression = searchExpression == null ? valueExp : Expression.OrElse(searchExpression, valueExp);
            }

            return searchExpression;
        }

        public IQueryable<TModel> ApplyDataQuery<TModel>(IQueryable<TModel> query)
        {
            var queryBuilder = ToQueryBuilderResponse<TModel>();
            if (queryBuilder.WhereExpression != null)
            {
                query = query.Where(queryBuilder.WhereExpression);
            }

            if (queryBuilder.Sorter != null)
            {
                query = queryBuilder.Sorter.Direction.ToLower() == "asc"
                    ? query.OrderBy(queryBuilder.Sorter.SortExpression)
                    : query.OrderByDescending(queryBuilder.Sorter.SortExpression);
            }

            query = query.Skip(Skip ?? 0).Take(Take ?? 10);

            return query;
        }

        public IQueryable<TModel> ApplyCountQuery<TModel>(IQueryable<TModel> query)
        {
            var queryBuilder = ToQueryBuilderResponse<TModel>();
            if (queryBuilder.WhereExpression != null)
            {
                query = query.Where(queryBuilder.WhereExpression);
            }

            return query;
        }
    }
}
