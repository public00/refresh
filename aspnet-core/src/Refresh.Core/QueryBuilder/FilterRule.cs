﻿namespace Refresh.QueryBuilder
{
    using System.Collections.Generic;

    public class FilterRule
    {
        public FilterRule()
        {
            Rules = new List<RuleInfo>();
        }

        public string Condition { get; set; }

        public IList<RuleInfo> Rules { get; set; }
    }
}
