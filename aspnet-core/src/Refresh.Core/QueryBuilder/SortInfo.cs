﻿namespace Refresh.QueryBuilder
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    public class SortInfo
    {
        public string Dir { get; set; }

        public string Field { get; set; }

        public Expression<Func<T, object>> GetMemberExpression<T>()
        {
            var type = typeof(T);
            var prop = type.GetProperty(Field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            if (prop == null)
            {
                return null;
            }

            var parameter = Expression.Parameter(type, "x");
            var propAccess = Expression.Property(parameter, prop);
            var propExpression = Expression.Convert(propAccess, typeof(object));
            return Expression.Lambda<Func<T, object>>(propExpression, parameter);
        }
    }
}
