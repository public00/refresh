﻿namespace Refresh.QueryBuilder
{
    public class QueryCondition
    {
        public const string And = "and";

        public const string Or = "or";
    }
}
