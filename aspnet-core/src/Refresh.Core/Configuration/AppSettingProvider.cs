﻿using System.Collections.Generic;
using Abp;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;

namespace Refresh.Configuration
{
    public class AppSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                new SettingDefinition(AppSettingNames.UiTheme, "red", scopes: SettingScopes.Application | SettingScopes.Tenant | SettingScopes.User, isVisibleToClients: true),
                new SettingDefinition(EmailSettingNames.Smtp.Host, "smtp.gmail.com", L("SmtpHost"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.Smtp.Port, "587", L("SmtpPort"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.Smtp.UserName, "refresh.appdev@gmail.com", L("Username"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.Smtp.Password, "gtl12345", L("Password"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.Smtp.EnableSsl, "true", L("UseSSL"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.Smtp.UseDefaultCredentials, "false", L("UseDefaultCredentials"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.DefaultFromAddress, "Default address", L("DefaultFromSenderEmailAddress"), scopes: SettingScopes.Application | SettingScopes.Tenant),
                new SettingDefinition(EmailSettingNames.DefaultFromDisplayName, "Display name", L("DefaultFromSenderDisplayName"), scopes: SettingScopes.Application | SettingScopes.Tenant)
            };
        }

        private static LocalizableString L(string name)
        {
            return new LocalizableString(name, AbpConsts.LocalizationSourceName);
        }
    }
}
