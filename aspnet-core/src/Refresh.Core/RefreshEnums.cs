﻿namespace Refresh
{
    public enum UserTypes
    {
        SystemAdministrator = 1,
        CompanyOwner = 2,
        CityAdministrator = 3,
        Janitor = 4,
        PropertyOwner = 5
    }

    public enum Months
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9, 
        October = 10,
        November = 11,
        December = 12
    }
}
