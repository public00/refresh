﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace Refresh.Entities.InvoiceSendingTrackers
{
    [Table("InvoiceSendingTrackers", Schema = "dbo")]
    public class InvoiceSendingTracker : Entity
    { 
        public int OwnerId { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }
    }
}
