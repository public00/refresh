﻿namespace Refresh.Entities.Buildings
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Domain.Entities.Auditing;

    public class Inventory : FullAuditedEntity
    {
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public int? ExpenseId { get; set; }
    }
}
