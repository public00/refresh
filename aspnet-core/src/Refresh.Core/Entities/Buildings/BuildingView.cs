﻿namespace Refresh.Entities.Buildings
{
    using Abp.Application.Services.Dto;

    public class BuildingView : IEntityDto
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public string StreetAddress { get; set; }
        public string BankAccountPrimary { get; set; }
        public string PIB { get; set; }
        public string PlotNumber { get; set; }
        public string ImmovableProperty { get; set; }
        public int? ManagerId { get; set; }
        public string ManagerName { get; set; }
        public int? PresidentId { get; set; }
        public string PresidentName { get; set; }
        public int? Quadrature { get; set; }
        public decimal? Balance { get; set; }
    }
}
