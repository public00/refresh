﻿namespace Refresh.Entities.Tickets
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using Authorization.Users;

    [Table("TicketComments", Schema = "dbo")]
    public class TicketComment : FullAuditedEntity
    {
        public int TicketId { get; set; }

        public string Comment { get; set; }

        [ForeignKey("CreatorUserId")]
        public virtual User Creator { get; set; }
    }
}
