﻿namespace Refresh.Entities.Tickets
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using Buildings;
    using Properties;

    [Table("Tickets", Schema = "dbo")]
    public class Ticket : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }

        [ForeignKey("Building")]
        public int BuildingId { get; set; }

        [ForeignKey("Property")]
        public int? PropertyId { get; set; }

        public bool IsClosed { get; set; }

        public virtual Building Building { get; set; }

        public virtual Property Property { get; set; }
    }
}