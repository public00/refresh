﻿namespace Refresh.Entities.Tickets
{
    using System;
    using Abp.Application.Services.Dto;

    public class TicketView : IEntityDto
    {
        public int Id { get; set; }

        public int BuildingId { get; set; }

        public string StreetAddress { get; set; }

        public int CityId { get; set; }

        public string CityName { get; set; }

        public string Title { get; set; }
        
        public string Description { get; set; }
        
        public int? PropertyId { get; set; }

        public string PropertyReferenceNumber { get; set; }
        
        public DateTime CreationTime { get; set; }

        public bool IsClosed { get; set; }

        public long CreatorUserId { get; set; }

        public string CreatedBy { get; set; }
    }
}