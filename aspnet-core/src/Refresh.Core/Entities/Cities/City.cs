﻿namespace Refresh.Entities.Cities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("Cities", Schema = "dbo")]
    public class City : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(64)]
        public string PhoneNumber { get; set; }

        [StringLength(256)]
        public string Address { get; set; }
    }
}
