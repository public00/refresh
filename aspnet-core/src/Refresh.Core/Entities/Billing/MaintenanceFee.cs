﻿namespace Refresh.Entities.Billing
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using Properties;

    [Table("MaintenanceFee", Schema = "dbo")]
    public class MaintenanceFee : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [ForeignKey("Property")]
        public int PropertyId { get; set; }

        public decimal Amount { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public bool IsFinishedPayment { get; set; }

        public virtual MaintenanceFeeCategory Category { get; set; }

        public virtual Property Property { get; set; }
    }
}
