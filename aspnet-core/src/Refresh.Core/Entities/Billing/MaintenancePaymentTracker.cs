﻿namespace Refresh.Entities.Billing
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("MaintenancePaymentTrackers", Schema = "Dbo")]
    public class MaintenancePaymentTracker : FullAuditedEntity
    {
        public int PaymentId { get; set; }

        public int MaintenanceFeeId { get; set; }

        public decimal Amount { get; set; }
    }
}
 