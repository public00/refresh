﻿namespace Refresh.Entities.Billing.Invoices
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Domain.Entities.Auditing;

    public class Article : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string Unit { get; set; }

        public decimal Price { get; set; }

        public decimal? Vat { get; set; }
    }
}
