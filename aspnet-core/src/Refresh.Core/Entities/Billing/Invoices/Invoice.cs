﻿namespace Refresh.Entities.Billing.Invoices
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("Invoices", Schema = "dbo")]
    public class Invoice : FullAuditedEntity
    {
        public string ReferenceNumber { get; set; }

        public int BuildingId { get; set; }

        public DateTime? ProinvoiceDate { get; set; }

        public DateTime? InvoiceDate { get; set; }
    }
}
