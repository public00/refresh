﻿namespace Refresh.Entities.Billing.Invoices
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("InvoiceItems", Schema = "dbo")]
    public class InvoiceItem : FullAuditedEntity
    {
        public int InvoiceId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string Unit { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public decimal? Vat { get; set; }

        public decimal VatAmount => Vat.HasValue ? Price * Quantity * Vat.Value / 100 : 0;

        public decimal Total => Price * Quantity + VatAmount;
    }
}
