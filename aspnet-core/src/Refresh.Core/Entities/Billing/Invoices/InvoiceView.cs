﻿namespace Refresh.Entities.Billing.Invoices
{
    using System;
    using Abp.Application.Services.Dto;

    public class InvoiceView : IEntityDto
    {
        public int Id { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime? ProinvoiceDate { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public int BuildingId { get; set; }
        public string BuildingAddress { get; set; }
        public long? CreatorUserId { get; set; }
        public string CreatorName { get; set; }
        public decimal Total { get; set; }
    }
}
