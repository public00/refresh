﻿namespace Refresh.Entities.Billing
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using System.ComponentModel.DataAnnotations;

    [Table("ExpenseCategory", Schema = "dbo")]
    public class ExpenseCategory : FullAuditedEntity
    {
        public int? ParentId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
