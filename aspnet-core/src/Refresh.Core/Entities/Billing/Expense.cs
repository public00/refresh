﻿namespace Refresh.Entities.Billing
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using System.ComponentModel.DataAnnotations;
    using Buildings;

    [Table("Expenses", Schema = "dbo")]
    public class Expense : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey("Building")]
        public int BuildingId { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [ForeignKey("SubCategory")]
        public int SubCategoryId { get; set; }

        [StringLength(128)]
        public string Company { get; set; }

        [StringLength(128)]
        public string InvoiceNumber { get; set; }

        public bool IsFinishedPayment { get; set; }

        public virtual Building Building { get; set; }

        public virtual ExpenseCategory Category { get; set; }

        public virtual ExpenseCategory SubCategory { get; set; }
    }
}
