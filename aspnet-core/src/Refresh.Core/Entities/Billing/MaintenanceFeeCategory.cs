﻿namespace Refresh.Entities.Billing
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("MaintenanceFeeCategories", Schema = "dbo")]
    public class MaintenanceFeeCategory : FullAuditedEntity
    {
        [Required]
        [StringLength(256)]
        public string Name { get; set; }
    }
}
