﻿namespace Refresh.Entities.Billing
{
    using Abp.Domain.Entities.Auditing;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Buildings;

    [Table("SentPayments", Schema = "dbo")]
    public class SentPayment : FullAuditedEntity
    {
        [ForeignKey("Building")]
        public int BuildingId { get; set; }

        [ForeignKey("Expense")]
        public int ExpenseId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [StringLength(128)]
        public string BankStatementNumber { get; set; }

        public decimal Amount { get; set; }

        public virtual Building Building { get; set; }

        public virtual Expense Expense { get; set; }
    }
}
