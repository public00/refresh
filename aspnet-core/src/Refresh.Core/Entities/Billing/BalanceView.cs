﻿namespace Refresh.Entities.Billing
{
    using Abp.Application.Services.Dto;

    public class BalanceView : IEntityDto
    {
        public int Id { get; set; }
        public int BuildingId { get; set; }
        public string Pd { get; set; }
        public string ReferenceNumber { get; set; }
        public string Owner { get; set; }
        public int Quadrature { get; set; }
        public string Number { get; set; }
        public string Floor { get; set; }
        public string CityName { get; set; }
        public decimal? PricePerSquareMeter { get; set; }
        public decimal? MaintenanceFeeFixedPrice { get; set; }
        public decimal MonthlyMaintenanceRate { get; set; }
        public int PropertyTypeId { get; set; }
        public string PropertyTypeName { get; set; }
        public int Year { get; set; }
        public decimal JanCost { get; set; }
        public decimal JanPaid { get; set; }
        public decimal FebCost { get; set; }
        public decimal FebPaid { get; set; }
        public decimal MarCost { get; set; }
        public decimal MarPaid { get; set; }
        public decimal AprCost { get; set; }
        public decimal AprPaid { get; set; }
        public decimal MayCost { get; set; }
        public decimal MayPaid { get; set; }
        public decimal JunCost { get; set; }
        public decimal JunPaid { get; set; }
        public decimal JulCost { get; set; }
        public decimal JulPaid { get; set; }
        public decimal AugCost { get; set; }
        public decimal AugPaid { get; set; }
        public decimal SepCost { get; set; }
        public decimal SepPaid { get; set; }
        public decimal OctCost { get; set; }
        public decimal OctPaid { get; set; }
        public decimal NovCost { get; set; }
        public decimal NovPaid { get; set; }
        public decimal DecCost { get; set; }
        public decimal DecPaid { get; set; }
        public decimal TotalCost { get; set; }
        public decimal Overpaid { get; set; }
        public decimal PreviousDebt { get; set; }
        public decimal TotalPaid => 
            JanPaid + FebPaid + MarPaid + AprPaid + MayPaid + JunPaid + 
            JulPaid + AugPaid + SepPaid + OctPaid + NovPaid + DecPaid + Overpaid;
        public decimal Debt => TotalCost + PreviousDebt - TotalPaid;
    }
}
