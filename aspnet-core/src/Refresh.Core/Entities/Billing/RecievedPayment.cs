﻿namespace Refresh.Entities.Billing
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using System.ComponentModel.DataAnnotations;
    using Buildings;
    using Properties;

    [Table("ReceivedPayments", Schema ="dbo")]
    public class ReceivedPayment : FullAuditedEntity
    {
        [ForeignKey("Building")]
        public int BuildingId { get; set; }

        [ForeignKey("Property")]
        public int? PropertyId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [StringLength(128)]
        public string BankStatementNumber { get; set; }

        public decimal Amount { get; set; }
        
        [StringLength(128)]
        public string BankAccount { get; set; }

        public virtual Building Building { get; set; }

        public virtual Property Property { get; set; }
    }
}
