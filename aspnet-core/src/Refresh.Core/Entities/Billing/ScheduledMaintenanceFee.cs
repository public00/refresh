﻿namespace Refresh.Entities.Billing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Abp.Domain.Entities.Auditing;
    using Properties;

    [Table("ScheduledMaintenanceFees", Schema = "dbo")]
    public class ScheduledMaintenanceFee : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [ForeignKey("Property")]
        public int PropertyId { get; set; }

        public decimal Amount { get; set; }

        public DateTime? LastRunDate { get; set; }

        public virtual MaintenanceFeeCategory Category { get; set; }

        public virtual Property Property { get; set; }

        [Required]
        [StringLength(32)]
        public string Months { get; set; }

        public List<int> GetScheduledMonths()
        {
            return Months.Split(',').Select(x => Convert.ToInt32(x)).ToList();
        }
    }
}
