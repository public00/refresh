﻿namespace Refresh.Entities.Billing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Buildings;

    [Table("ScheduledExpense", Schema = "dbo")]
    public class ScheduledExpense : FullAuditedEntity
    {
        [ForeignKey("Building")]
        public int BuildingId { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [ForeignKey("SubCategory")]
        public int SubCategoryId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        [StringLength(128)]
        public string Company { get; set; }

        public DateTime? LastRunDate { get; set; }

        [Required]
        [StringLength(32)]
        public string Months { get; set; }

        public virtual Building Building { get; set; }

        public virtual ExpenseCategory Category { get; set; }

        public virtual ExpenseCategory SubCategory { get; set; }

        public List<int> GetScheduledMonths()
        {
            return Months.Split(',').Select(x => Convert.ToInt32(x)).ToList();
        }
    }
}
