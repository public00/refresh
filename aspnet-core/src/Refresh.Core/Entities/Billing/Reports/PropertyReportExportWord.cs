﻿namespace Refresh.Entities.Billing
{
    public class PropertyReportExportWord
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public string Type { get; set; }

        public int Quadrature { get; set; }

        public string StreetAddress { get; set; }

        public string PrintDate { get; set; }

        public string Pib { get; set; }

        public string PropertyPd { get; set; }

        public decimal Total { get; set; }

        public string BankNumber { get; set; }

        public string Bic { get; set; }

        public string IBan { get; set; }

        public string FcaAddress { get; set; }

        public string  FcaName { get; set; }

        public decimal Amount { get; set; }

        public decimal TrackerAmount { get; set; }

        public decimal Debt { get; set; }

        public string Dm { get; set; }

        public int Dy { get; set; }


    }
}
