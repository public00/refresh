﻿namespace Refresh.Entities.Billing
{
    public class PropertyMaintenanceReport
    {
        public string Floor { get; set; }

        public string DateToday { get; set; }

        public string BuildingName { get; set; }

        public string PropertyNumber { get; set; }

        public int Quadrature { get; set; }

        public string Name { get; set; }

        public decimal Debt { get; set; }

        public decimal Amount { get; set; }

        public decimal AccountMoney { get; set; }

        public string January { get; set; }

        public string February { get; set; }

        public string March { get; set; }

        public string April { get; set; }

        public string May { get; set; }

        public string June { get; set; }

        public string July { get; set; }

        public string August { get; set; }

        public string September { get; set; }

        public string October { get; set; }

        public string November { get; set; }

        public string December { get; set; }
    }
}
