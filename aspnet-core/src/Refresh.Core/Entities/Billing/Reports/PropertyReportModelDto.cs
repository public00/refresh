﻿namespace Refresh.Entities.Billing
{
    public class PropertyReportModelDto
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public string Type { get; set; }

        public int Quadrature { get; set; }

        public string AccountingPeriod { get; set; }

        public string Law { get; set; }
        
        public string ApprovalNumber { get; set; }

        public string StreetAddress { get; set; }

        public string Date { get; set; }

        public string Pib { get; set; }

        public string PropertyPd { get; set; }

        public string Total { get; set; }

        public string Account { get; set; }

        public string Bic { get; set; }

        public string IBan { get; set; }

        public string FcaAddress { get; set; }

        public string FcaName { get; set; }

        public string Amount { get; set; }

        public decimal TrackerAmount { get; set; }

        public string Debt { get; set; }

        public string Month { get; set; }

        public int Year { get; set; }
    }
}
