﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refresh.Entities.Billing.Reports
{
    public class ExpensesExportModel
    {
        public List<ExpensesExport> ExpensesModel { get; set; }

        public string CurrentTime { get; set; }
         
        public string Building { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }

    public class ExpensesExport
    { 
        public string Description { get; set; }

        public decimal Price { get; set; }

        public string Date { get; set; }

        public decimal Payed { get; set; }

        public string Number { get; set; }
    }
}
