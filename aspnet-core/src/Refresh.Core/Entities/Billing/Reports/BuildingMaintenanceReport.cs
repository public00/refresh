﻿using System.Collections.Generic;

namespace Refresh.Entities.Billing.Reports
{
    public class FinancialReport
    {
        public FinancialReport()
        {
            Revenues = new List<WorkExpenses>();

            Expenses = new List<ExpenseGroup>();
        }

        public List<WorkExpenses> Revenues { get; set; }

        public List<ExpenseGroup> Expenses { get; set; }
    }

    public class ExpenseGroup
    {
        public ExpenseGroup()
        {
            Items = new List<WorkExpenses>();
        }

        public string Name { get; set; }

        public List<WorkExpenses> Items { get; set; }
    }

    public class WorkExpenses
    {
        public string Name { get; set; }

        public decimal Invoiced { get; set; }

        public decimal Paid { get; set; }

        public decimal Saldo => Invoiced - Paid;
    }
}