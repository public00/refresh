﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refresh.Entities.Billing.Reports
{
    public class ExportSendModel
    {
        public int Id { get; set; }

        public int Year { get; set; }
    }
}
