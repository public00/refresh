﻿namespace Refresh.Entities.Announcements
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("Announcements", Schema = "dbo")]
    public class Announcement : FullAuditedEntity
    {
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }

        [StringLength(512)]
        public string Path { get; set; }
    }
}
