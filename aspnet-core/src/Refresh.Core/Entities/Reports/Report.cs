﻿namespace Refresh.Entities.Reports
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("Reports", Schema = "dbo")]
    public class Report : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        public DateTime From { get; set; }
        
        public DateTime To { get; set; }
        
        public int BuildingId { get; set; }

        [Required]
        [StringLength(128)]
        public string MaintenanceType { get; set; }

        [StringLength(128)]
        public string Invoice { get; set; }

        [StringLength(128)]
        public string SpentResources { get; set; }

        public int Quantity { get; set; }

        public decimal NPrice { get; set; }
        
        public decimal? PPrice { get; set; }
        
        [StringLength(32)]
        public string TimeSpent { get; set; }

        [StringLength(256)]
        public string Note { get; set; }
    }
}
