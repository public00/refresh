﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Refresh.Entities.Reports
{
    using System.ComponentModel.DataAnnotations;

    [Table("Supplies", Schema = "dbo")]
    public class Supply : FullAuditedEntity
    {
        public DateTime Date { get; set; }

        [Required]
        [StringLength(64)]
        public string InvoiceNumber { get; set; }

        public int? CityId { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        public int Quantity { get; set; }

        public decimal ItemPrice { get; set; }

        [StringLength(512)]
        public string Note { get; set; }
    }
}
