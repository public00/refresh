﻿using System;

namespace Refresh.Entities.Reports
{
    using Abp.Application.Services.Dto;

    public class ReportView : IEntityDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public long? JanitorId { get; set; }

        public string Janitor { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public int BuildingId { get; set; }

        public string Client { get; set; }

        public int CityId { get; set; }

        public string CityName { get; set; }

        public string MaintenanceType { get; set; }

        public string Invoice { get; set; }

        public string SpentResources { get; set; }

        public int Quantity { get; set; }

        public decimal NPrice { get; set; }

        public decimal? PPrice { get; set; }

        public string TimeSpent { get; set; }

        public string Note { get; set; }
    }
}
