﻿using System;

namespace Refresh.Entities.Reports
{
    using Abp.Application.Services.Dto;

    public class SupplyView : IEntityDto
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string InvoiceNumber { get; set; }

        public int? CityId { get; set; }

        public string CityName { get; set; } 

        public int Quantity { get; set; }

        public decimal ItemPrice { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public string  CreatedBy { get; set; }
    }
}
