﻿namespace Refresh.Entities.Properties
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [Table("PropertyLegalStatuses", Schema = "dbo")]
    public class PropertyLegalStatus : Entity
    {
        public int PropertyId { get; set; }

        [StringLength(1024)]
        public string Note { get; set; }
    }
}
