﻿using Refresh.Entities.Billing;
using System.Collections.Generic;

namespace Refresh.Entities.Properties
{
    public class PropertyInfoReport
    {
        public List<Property> Properties { get; set; }

        public List<MaintenanceFee> MaintenanceFees { get; set; }

        public List<MaintenancePaymentTracker> MaintenancePaymentTrackers { get; set; }

        public int[] PropertyIds { get; set; }

        public decimal Invoiced { get; set; }

        public decimal Paid { get; set; }
    } 
}
