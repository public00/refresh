﻿namespace Refresh.Entities.Properties
{
    using Abp.Application.Services.Dto;

    public class PropertyPickerView : IEntityDto
    {
        public int Id { get; set; }

        public int BuildingId { get; set; }

        public string ReferenceNumber { get; set; }
    }
}
