﻿namespace Refresh.Entities.Properties
{
    using System;
    using Abp.Application.Services.Dto;

    public class PropertyOwnerView : IEntityDto
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string Jmbg { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
    }
}
