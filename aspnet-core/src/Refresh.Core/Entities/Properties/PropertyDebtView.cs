﻿namespace Refresh.Entities.Properties
{
    using Abp.Application.Services.Dto;

    public class PropertyDebtView : EntityDto
    {
        public string ReferenceNumber { get; set; }
        public int Quadrature { get; set; }
        public int BuildingId { get; set; }
        public string Floor { get; set; }
        public string Number { get; set; }
        public decimal MonthlyMaintenanceRate { get; set; }
        public int PropertyTypeId { get; set; }
        public int CityId { get; set; }
        public string PropertyTypeName { get; set; }
        public string Owner { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalPaid { get; set; }
        public bool IsOverdue { get; set; }
        public string LegalStatus { get; set; }
        public decimal Debt => TotalCost - TotalPaid;
    }
}
