﻿namespace Refresh.Entities.Properties
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("PropertyTypes")]
    public class PropertyType : FullAuditedEntity
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
