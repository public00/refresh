﻿namespace Refresh.Entities.Properties
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using Owners;

    [Table("PropertyOwners", Schema = "dbo")]
    public class PropertyOwner : FullAuditedEntity
    {
        [ForeignKey("Owner")]
        public int OwnerId { get; set; }

        [ForeignKey("Property")]
        public int PropertyId { get; set; }

        public virtual Owner Owner { get; set; }

        public virtual Property Property { get; set; }
    }
}
