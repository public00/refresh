﻿namespace Refresh.Entities.Owners
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;

    [Table("Owners", Schema ="dbo")]
    public class Owner : FullAuditedEntity
    {
        // nullable jer se UserId upisuje prilikom registracije
        public long? UserId { get; set; }

        [StringLength(64)]
        public string JMBG { get; set; }

        [Required]
        [StringLength(128)]
        public string FirstName { get; set; }

        [StringLength(128)]
        public string LastName { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(256)]
        public string Address { get; set; }

        [StringLength(64)]
        public string PhoneNumber { get; set; }
    }
}
