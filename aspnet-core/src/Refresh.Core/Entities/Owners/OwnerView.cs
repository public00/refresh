﻿namespace Refresh.Entities.Owners
{
    using Abp.Application.Services.Dto;

    public class OwnerView : IEntityDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}
