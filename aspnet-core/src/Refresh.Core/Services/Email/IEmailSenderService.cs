﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refresh.Services.Email
{
    public interface IEmailSenderService : ITransientDependency
    {
        void SendInvoices(int buildingId, int month, int year);
    }
}
