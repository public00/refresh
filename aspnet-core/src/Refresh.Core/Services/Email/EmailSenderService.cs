﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Refresh.Entities.Billing;
using Refresh.Entities.Buildings;
using Refresh.Entities.InvoiceSendingTrackers;
using Refresh.Entities.Owners;
using Refresh.Entities.Properties;
using Refresh.Services.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace Refresh.Services.Email
{
    public class EmailSenderService : IDomainService, IEmailSenderService
    {
        private readonly SmtpClient _client;
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<MaintenancePaymentTracker> _maintenancePaymentTrackerRepository;
        private readonly IRepository<PropertyOwner> _propertyOwnersRepository;
        private readonly IRepository<Owner> _ownerRepository;
        private readonly IRepository<PropertyType> _propertyTypeRepository;
        private readonly IRepository<ExpenseCategory> _expenseCategoriesRepository;
        private readonly IRepository<InvoiceSendingTracker> _invoiceSendingTrackerRepository;
        private readonly IExportService _exportService;
        private readonly ReportExportHelper _reportExportHelper;
        
        public EmailSenderService(
            IExportService exportService,
            ReportExportHelper reportExportHelper,
            IRepository<InvoiceSendingTracker> invoiceSendingTrackerRepository,
            IRepository<Building> buildingRepository,
            IRepository<Property> propertyRepository,
            IRepository<Owner> ownerRepository,
            IRepository<MaintenanceFee> maintenanceFeeRepository, 
            IRepository<PropertyOwner> propertyOwnerRepository,
            IRepository<PropertyType> propertyType,
            IRepository<ExpenseCategory> expenseCategoryRepository,
            IRepository<MaintenancePaymentTracker> maintenancePaymentTrackerRepository)
        {
            _buildingRepository = buildingRepository;
            _exportService = exportService;
            this._reportExportHelper = reportExportHelper;
            _propertyOwnersRepository = propertyOwnerRepository;
            _ownerRepository = ownerRepository;
            _expenseCategoriesRepository = expenseCategoryRepository;
            _propertyTypeRepository = propertyType;
            _propertyRepository = propertyRepository;
            _invoiceSendingTrackerRepository = invoiceSendingTrackerRepository;
            _maintenanceFeeRepository = maintenanceFeeRepository;
            _maintenancePaymentTrackerRepository = maintenancePaymentTrackerRepository;
            var emailConfigKey = "Email";
            var passwordConfigKey = "Password";

            _client = new SmtpClient("smtp.gmail.com", 587);
            var email = System.Configuration.ConfigurationManager.AppSettings[emailConfigKey];
            var password = System.Configuration.ConfigurationManager.AppSettings[passwordConfigKey];
            _client.EnableSsl = true;
            _client.Credentials = new NetworkCredential(email, password);
        }

        public bool SendEmail(MailMessage message)
        {
            try
            {
                _client.Send(message);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public void SendInvoices(int buildingId, int month, int year)
        {
            var building = _buildingRepository.Get(buildingId);
            var helper = new PropertyHelper(_propertyRepository, _maintenancePaymentTrackerRepository, _maintenanceFeeRepository);
            var propertyInfo = helper.GetPropertyInfo(buildingId);
            var propOwners = _propertyOwnersRepository.GetAllList(x => propertyInfo.PropertyIds.Contains(x.PropertyId));
            var ownerIds = (from a in propOwners select a.OwnerId).ToArray();
            var owners = _ownerRepository.GetAllList(x => x.Email != null && x.Email != string.Empty && ownerIds.Contains(x.Id));
            
            foreach (var owner in owners)
            {
                var isSent = this._invoiceSendingTrackerRepository.GetAllList(x =>
                x.OwnerId == owner.Id &&
                x.Month == month &&
                x.Year == year).Any();
                if (isSent)
                {
                    continue;
                }
                
                using (var message = new MailMessage(
                    System.Configuration.ConfigurationManager.AppSettings["Email"],
                    owner.Email,
                    "Račun za održavanje",
                    "Poštovani/a,<br>&nbsp;<br>U prilogu Vam dostavljam račun za tekući mjesec.<br>" +
                    "&nbsp;<br>Srdačan pozdrav,<br>Refresh services doo<br>069/555-120<br>II Crnogorskog Bataljona 2/J,<br>Podgorica"))
                {
                    var file = GetAttachment(owner, building.Id, month, year);
                    
                    message.IsBodyHtml = true;
                    message.Attachments.Add(file);
                    SendEmail(message);
                }

                _invoiceSendingTrackerRepository.Insert(
                    new InvoiceSendingTracker
                    {
                        Month = month,
                        Year = year,
                        OwnerId = owner.Id
                    });
            }
        }
        
        // vraca Zip file kao attacment
        public Attachment GetAttachment(Owner owner, int buildingId, int month, int year)
        {
            List<PropertyReportModelDto> propertiesModel = new List<PropertyReportModelDto>();
            var building =_buildingRepository.Get(buildingId);
            var propertyIds = _propertyOwnersRepository.GetAllList(x => x.OwnerId == owner.Id).Select(x=>x.PropertyId);
            // zipuj ih i posalji na mail
            foreach (var property in propertyIds)
            {
                var propertyItem = this._propertyRepository.Get(property);
                var model = _reportExportHelper.GeneratePropertyReportModel(building, propertyItem, month, year);
                propertiesModel.Add(model);
            }

            // pretvori model u fajlove
            return GetAttachmentFile(propertiesModel, buildingId);
        }

        private Attachment GetAttachmentFile(List<PropertyReportModelDto> model, int buildingId)
        {
            var folderPath = RefreshConsts.TempLocation + buildingId;
            Directory.Delete(folderPath, true);
            var path = @"Templates\racun-template.docx";
            var name = model?.FirstOrDefault()?.StreetAddress ?? string.Empty;
            var fol = RefreshConsts.TempLocation + "\\" + buildingId + "\\" + "Zip";
            var zipName = fol + "\\" + name + "_" + buildingId + ".zip"; // ime buduceg zip fajl
            Directory.CreateDirectory(folderPath);
            Directory.CreateDirectory(fol);

            int d = 1;
            foreach (var item in model) // napravimo word fajlove
            {
                var outputFile = $"{folderPath}\\Racun{item.Id}-PIB{item.Pib}-PD{item.PropertyPd.Replace(@"\", "-").Replace(@"/", "-")}.docx";
                var output = new FileStream(outputFile, FileMode.Create);
                using (var input = new MemoryStream(File.ReadAllBytes(path)))
                using (output)
                using (var document = NGS.Templater.Configuration.Factory.Open(input, output, "docx"))
                {
                    document.Process(item);
                }
                d++;
            }

            using (var newFile = ZipFile.Open(zipName, ZipArchiveMode.Create))
            {
                int i = 1;
                foreach (string f in Directory.GetFiles(folderPath)) // sve fajlove ubacimo u zip
                {
                    var fileName = f.Split('\\').LastOrDefault() ?? $"Racun-{i}.docx";
                    newFile.CreateEntryFromFile(f, fileName);
                    i++;
                }
            }

            byte[] file = File.ReadAllBytes(zipName);
            Stream stream = new MemoryStream(file);
            Directory.Delete(folderPath, true);


            var attachment = new Attachment(stream, "RefreshMailZip.zip", "application/msword");
            attachment.ContentDisposition.FileName = "RefreshMailZip.zip";

            return attachment;
        }
    }
}
