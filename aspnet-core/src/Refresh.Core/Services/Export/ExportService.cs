﻿namespace Refresh.Services.Export
{
    using System;
    using System.Collections.Generic;
    using Refresh;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using NGS.Templater;
    using ExportModel;
    using Refresh.Entities.Billing;
    using Refresh.Entities.Buildings;
    using Abp.Domain.Repositories;
    using Refresh.Entities.Properties;
    using Refresh.Entities.Owners;
    using Views;
    using Refresh.Entities.Billing.Reports;

    /// <summary>
    /// Default export service implementation 
    /// </summary>
    public class ExportService : IExportService
    {
        private readonly IBuildingViewQuery _buildingViewQuery;
        private readonly ReportExportHelper _reportExportHelper;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<PropertyOwner> _propertyOwnersRepository;
        private readonly IRepository<Expense> _expenseRepository;
        private readonly IRepository<Owner> _ownerRepository;
        private readonly IRepository<MaintenancePaymentTracker> _maintenancePaymentTrackerRepository;
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;
        private readonly IRepository<ReceivedPayment> _receivedPaymentRepository;
        private readonly IRepository<SentPayment> _sentPaymentRepository;

        public ExportService(ReportExportHelper reportExportHelper,
            IRepository<Building> buildingRepository,
            IRepository<MaintenanceFee> maintenenceFeeRepository,
            IRepository<MaintenancePaymentTracker> maintenancePaymentTrackerRepository,
            IRepository<MaintenanceFee> maintenanceFeeRepository,
            IRepository<PropertyOwner> propertyOwnersRepository,
            IRepository<Owner> ownerRepository,
            IRepository<Expense> expenseRepository,
            IRepository<Property> propertyRepository,
            IRepository<SentPayment> sentPaymentRepository,
            IBuildingViewQuery buildingViewQuery,
            IRepository<ReceivedPayment> receivedPaymentRepository)
        {
            _maintenancePaymentTrackerRepository = maintenancePaymentTrackerRepository;
            _reportExportHelper = reportExportHelper;
            _ownerRepository = ownerRepository;
            _buildingRepository = buildingRepository;
            _maintenanceFeeRepository = maintenanceFeeRepository;
            _sentPaymentRepository = sentPaymentRepository;
            _propertyOwnersRepository = propertyOwnersRepository;
            _propertyRepository = propertyRepository;
            _expenseRepository = expenseRepository;
            _buildingViewQuery = buildingViewQuery;
            _receivedPaymentRepository = receivedPaymentRepository;
        }

        public Stream GetExpenses(int buildingId, int month, int year)
        {
            var building = _buildingRepository.Get(buildingId);

            var endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            var startDate = new DateTime(year, month, 1);
            var sentPayments = new List<SentPayment>();
            var expenses = _expenseRepository.GetAllList(x => x.BuildingId == buildingId && x.CreationTime > startDate && x.CreationTime < endDate);

            var propertiesModel = new ExpensesExportModel();
            var expensesModel = new List<ExpensesExport>();

            foreach (var item in expenses)
            {
                var model = new ExpensesExport();
                var payed = _sentPaymentRepository.GetAllList(x => x.ExpenseId == item.Id).Sum(x => x.Amount);
                model.Date = item.CreationTime.ToShortDateString();
                model.Description = item.Title;
                model.Number = item.InvoiceNumber;
                model.Payed = payed;

                expensesModel.Add(model);
            }
            propertiesModel.ExpensesModel = new List<ExpensesExport>(expensesModel);
            propertiesModel.CurrentTime = DateTime.Now.ToShortDateString();
            propertiesModel.Building = building.StreetAddress;
            propertiesModel.StartDate = startDate.ToShortDateString();
            propertiesModel.EndDate = endDate.ToShortDateString();

            var path = @"Templates\Expenses.xlsx";
            var outputFile = RefreshConsts.TempLocation + "Expenses-Building-" + buildingId + ".xlsx";
            var output = new FileStream(outputFile, FileMode.Create);
            using (var input = new MemoryStream(File.ReadAllBytes(path)))
            using (output)
            using (var document = Configuration.Factory.Open(input, output, "xlsx"))
            {
                document.Process(propertiesModel);
            }

            byte[] file = File.ReadAllBytes(outputFile);
            System.IO.File.Delete(outputFile);
            return new MemoryStream(file);
        }


        public Stream GetInvoices(int buildingId, int month, int year)
        {
            var building = _buildingRepository.Get(buildingId);
            PropertyHelper helper = new PropertyHelper(_propertyRepository, _maintenancePaymentTrackerRepository, _maintenanceFeeRepository);
            var propertyInfo = helper.GetPropertyInfo(buildingId);
            var propOwners = _propertyOwnersRepository.GetAllList(x => propertyInfo.PropertyIds.Contains(x.PropertyId));
            var ownerIds = (from a in propOwners select a.OwnerId).ToArray();
            var owners = _ownerRepository.GetAllList(x => ownerIds.Contains(x.Id));
            var zipName = RefreshConsts.TempLocation + $"buildingId-{buildingId}" + ".zip"; // ime buduceg zip fajl

            var rootFolderPath = RefreshConsts.TempLocation + $"buildingId-{buildingId}";
            Directory.CreateDirectory(rootFolderPath);

            foreach (var owner in owners)// folder sa imenom
            {
                CreateOwnerFolder(owner, building.Id, month, year, rootFolderPath);
            }

            ZipFile.CreateFromDirectory(rootFolderPath, zipName);

            byte[] file = File.ReadAllBytes(zipName);
            Stream stream = new MemoryStream(file);
            System.IO.DirectoryInfo di = new DirectoryInfo(RefreshConsts.TempLocation);

            foreach (FileInfo f in di.GetFiles())
            {
                f.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }

            return stream;
        }

        public Stream GetPropertyInvoice(int propertyId, int month, int year, bool print)
        {
            var reportDto = new List<PropertyReportModelDto>();
            var model = this.GetPropertyReportFile(propertyId, month, year);
            reportDto.Add(model);

            var filePath = $"Prostor-{model.Pib}_{model.PropertyPd}_{model.Id}";

            var zipName = RefreshConsts.TempLocation + filePath + ".zip";
            var rootFolderPath = RefreshConsts.TempLocation + filePath;
            Directory.CreateDirectory(rootFolderPath);
            var property = this._propertyRepository.Get(propertyId);
            GetAttachmentFile(reportDto, property.BuildingId, rootFolderPath, print);

            ZipFile.CreateFromDirectory(rootFolderPath, zipName);

            byte[] file = File.ReadAllBytes(zipName);
            Stream stream = new MemoryStream(file);
            System.IO.DirectoryInfo di = new DirectoryInfo(RefreshConsts.TempLocation);

            foreach (FileInfo f in di.GetFiles())
            {
                f.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }

            return stream;
        }

        private PropertyReportModelDto GetPropertyReportFile(int propertyId, int month, int year)
        {
            var propertyItem = this._propertyRepository.Get(propertyId);
            var building = this._buildingRepository.Get(propertyItem.BuildingId);

            return _reportExportHelper.GeneratePropertyReportModel(building, propertyItem, month, year);

        }

        // vraca Zip file kao attacment
        private void CreateOwnerFolder(Owner owner, int buildingId, int month, int year, string rootPath)
        {
            List<PropertyReportModelDto> propertiesModel = new List<PropertyReportModelDto>();
            var building = _buildingRepository.Get(buildingId);
            var propertyIds = _propertyOwnersRepository.GetAllList(x => x.OwnerId == owner.Id).Select(x => x.PropertyId);
            var ids = propertyIds;
            propertyIds = _propertyRepository.GetAllList(x => ids.Contains(x.Id) && x.BuildingId == buildingId).Select(x => x.Id).ToList();
            foreach (var property in propertyIds)
            {
                var propertyItem = this._propertyRepository.Get(property);
                var model = _reportExportHelper.GeneratePropertyReportModel(building, propertyItem, month, year);
                propertiesModel.Add(model);
            }

            // pretvori model u fajlove
            GetAttachmentFile(propertiesModel, buildingId, rootPath, true);
        }

        private void GetAttachmentFile(List<PropertyReportModelDto> model, int buildingId, string path, bool print)
        {
            var templateDocument = $@"Templates\racun-template{(print ? "-print" : string.Empty)}.docx";

            foreach (var item in model) // napravimo word fajlove
            {
                var outputFile = path + $"\\Racun-{item.Pib}_{item.PropertyPd.Replace(@"\", "-").Replace(@"/", "-")}.docx";
                var output = new FileStream(outputFile, FileMode.Create);
                using (var input = new MemoryStream(File.ReadAllBytes(templateDocument)))
                using (output)
                using (var document = NGS.Templater.Configuration.Factory.Open(input, output, "docx"))
                {
                    document.Process(item);
                }
            }
        }

        public Stream DownloadInvoicses(int buildingId, int year, int month)
        {
            var model = _reportExportHelper.GetPropertyReportExportWord(buildingId, year, month);

            var folderPath = RefreshConsts.TempLocation + $"buildingId-{buildingId}";
            var path = @"Templates\Invoice.docx";
            var name = model.Count == 0 ? model[0].Pib : string.Empty;
            var zipName = RefreshConsts.TempLocation + $"buildingId-{buildingId}" + "\\" + "Zip" + "\\" + name + "_" + buildingId + ".zip"; // ime buduceg zip fajl
            var fol = RefreshConsts.TempLocation + $"buildingId-{buildingId}" + "\\" + "Zip";
            Directory.CreateDirectory(folderPath);
            Directory.CreateDirectory(fol);

            int d = 1;
            foreach (var item in model) // napravimo word fajlove
            {
                var outputFile = RefreshConsts.TempLocation + $"buildingId-{buildingId}" + "\\" + "Invoice-" + buildingId + "-" + month + "-" + item.PropertyPd.Replace(@"\", "-").Replace(@"/", "-") + ".docx";
                var output = new FileStream(outputFile, FileMode.Create);
                using (var input = new MemoryStream(File.ReadAllBytes(path)))
                using (output)
                using (var document = Configuration.Factory.Open(input, output, "docx"))
                {
                    document.Process(item);
                }
                d++;
            }

            using (var newFile = ZipFile.Open(zipName, ZipArchiveMode.Create))
            {
                int i = 1;
                foreach (string f in Directory.GetFiles(folderPath)) // sve fajlove ubacimo u zip
                {
                    newFile.CreateEntryFromFile(f, "Invoice" + i + ".docx");
                    i++;
                }
            }

            byte[] file = File.ReadAllBytes(zipName);
            Directory.Delete(folderPath, true);

            return new MemoryStream(file);
        }

        public Stream ExportWordInvoice(object invoices, int id)
        {
            var path = @"Templates\ReportInvoice.docx";
            var outputFile = RefreshConsts.TempLocation + "Invoice_" + id + ".docx";
            var output = new FileStream(outputFile, FileMode.Create);
            using (var input = new MemoryStream(File.ReadAllBytes(path)))
            using (output)
            using (var document = Configuration.Factory.Open(input, output, "docx"))
            {
                document.Process(invoices);
            }

            byte[] file = File.ReadAllBytes(outputFile);
            System.IO.File.Delete(outputFile);
            return new MemoryStream(file);
        }

        public Stream BuildingFinancialReport(int buildingId, DateTime from, DateTime to) // nova forma excel categories
        {
            var model = _reportExportHelper.GetBuildingFinancialReport(buildingId, from, to);

            var path = @"Templates\FI.xlsx";
            var outputFile = RefreshConsts.TempLocation + "FinanciaReport" + buildingId + ".xlsx";
            var output = new FileStream(outputFile, FileMode.Create);
            using (var input = new MemoryStream(File.ReadAllBytes(path)))
            using (output)
            using (var document = Configuration.Factory.Open(input, output, "xlsx"))
            {
                document.Process(model);
            }

            byte[] file = File.ReadAllBytes(outputFile);
            System.IO.File.Delete(outputFile);
            return new MemoryStream(file);
        }

        public Stream ExportExcelReport(BuildingExportModel balanceView, int buildingId, int year)
        {
            var balanceModel = ConvertBalanceView(balanceView, buildingId, year);

            var path = @"Templates\Template.xlsx";
            var outputFile = RefreshConsts.TempLocation + "Report_" + buildingId + ".xlsx";
            var output = new FileStream(outputFile, FileMode.Create);
            using (var input = new MemoryStream(File.ReadAllBytes(path)))
            using (output)
            using (var document = Configuration.Factory.Open(input, output, "xlsx"))
            {
                document.Process(balanceModel);
            }

            byte[] file = File.ReadAllBytes(outputFile);
            System.IO.File.Delete(outputFile);
            return new MemoryStream(file);
        }

        public BuildingExportModelConverted ConvertBalanceView(BuildingExportModel model, int buildingId, int year)
        {
            var buildingView = _buildingViewQuery.Get(buildingId);
            var balance = buildingView.Result.Balance ?? 0;
            BuildingExportModelConverted exportModel = new BuildingExportModelConverted
            {
                CityName = model.CityName,
                DateNow = model.DateNow,
                Balance = balance,
                Balances = ConvertBalances(model.Balances),
                BankAccountNo = buildingView.Result.BankAccountPrimary,
                UnknownPayments = 
                    _receivedPaymentRepository.GetAll()
                        .Where(
                            x => x.BuildingId == buildingId
                            && !x.PropertyId.HasValue
                            && !x.IsDeleted
                            && x.CreationTime.Year == year)
                        .Select(x => x.Amount).DefaultIfEmpty(0).Sum()
            };

            return exportModel;
        }

        public List<BalanceViewExportDto> ConvertBalances(List<BalanceView> balances)
        {
            List<BalanceViewExportDto> model = new List<BalanceViewExportDto>();
            foreach (var item in balances)
            {
                var exportDto = new BalanceViewExportDto
                {
                    Id = item.Id,
                    CityName = item.CityName,
                    BuildingId = item.BuildingId,
                    Pd = item.Pd,
                    Floor = item.Floor,
                    MaintenanceFeeFixedPrice = item.MaintenanceFeeFixedPrice,
                    MonthlyMaintenanceRate = item.MonthlyMaintenanceRate,
                    Year = item.Year,
                    TotalPaid = item.TotalPaid,
                    TotalCost = item.TotalCost + item.PreviousDebt,
                    ReferenceNumber = item.ReferenceNumber,
                    PropertyTypeName = item.PropertyTypeName,
                    Quadrature = item.Quadrature,
                    PropertyTypeId = item.PropertyTypeId,
                    PricePerSquareMeter = item.PricePerSquareMeter,
                    Owner = item.Owner,
                    Number = item.Number,
                    JanCost = CheckValue(item.JanCost, item.JanPaid, item.JanCost, true),
                    JanPaid = CheckValue(item.JanCost, item.JanPaid, item.JanPaid, false),
                    FebCost = CheckValue(item.FebCost, item.FebPaid, item.FebCost, true),
                    FebPaid = CheckValue(item.FebCost, item.FebPaid, item.FebPaid, false),
                    MarCost = CheckValue(item.MarCost, item.MarPaid, item.MarCost, true),
                    MarPaid = CheckValue(item.MarCost, item.MarPaid, item.MarPaid, false),
                    AprCost = CheckValue(item.AprCost, item.AprPaid, item.AprCost, true),
                    AprPaid = CheckValue(item.AprCost, item.AprPaid, item.AprPaid, false),
                    MayCost = CheckValue(item.MayCost, item.MayPaid, item.MayCost, true),
                    MayPaid = CheckValue(item.MayCost, item.MayPaid, item.MayPaid, false),
                    JunCost = CheckValue(item.JunCost, item.JunPaid, item.JunCost, true),
                    JunPaid = CheckValue(item.JunCost, item.JunPaid, item.JunPaid, false),
                    JulCost = CheckValue(item.JulCost, item.JulPaid, item.JulCost, true),
                    JulPaid = CheckValue(item.JulCost, item.JulPaid, item.JulPaid, false),
                    AugCost = CheckValue(item.AugCost, item.AugPaid, item.AugCost, true),
                    AugPaid = CheckValue(item.AugCost, item.AugPaid, item.AugPaid, false),
                    SepCost = CheckValue(item.SepCost, item.SepPaid, item.SepCost, true),
                    SepPaid = CheckValue(item.SepCost, item.SepPaid, item.SepPaid, false),
                    OctCost = CheckValue(item.OctCost, item.OctPaid, item.OctCost, true),
                    OctPaid = CheckValue(item.OctCost, item.OctPaid, item.OctPaid, false),
                    NovCost = CheckValue(item.NovCost, item.NovPaid, item.NovCost, true),
                    NovPaid = CheckValue(item.NovCost, item.NovPaid, item.NovPaid, false),
                    DecCost = CheckValue(item.DecCost, item.DecPaid, item.DecCost, true),
                    DecPaid = CheckValue(item.DecCost, item.DecPaid, item.DecPaid, false),
                    PreviousDebt = item.PreviousDebt
                };

                model.Add(exportDto);

            }

            return model;
        }

        public string CheckValue(decimal valueA, decimal valueB, decimal returnValue, bool isCost)
        {
            if (valueA == 0 && valueB == 0)
            {
                return string.Empty;
            }
            else
            {
                if (isCost)
                {
                    return returnValue.ToString() + "/";
                }
                else
                {
                    return returnValue.ToString();
                }
            }
        }


        #region Default methods
        public Stream ExportToExcel<T>(IList<T> data, IList<ExportColumnInfo> columnInfo)
        {
            if (data == null || !data.Any())
            {
                return null;
            }

            var stream = new MemoryStream();
            // Create a spreadsheet document by supplying the filepath.
            // By default, AutoSave = true, Editable = true, and Type = xlsx.
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook);

            // Add a WorkbookPart to the document.
            WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            // Add a WorksheetPart to the WorkbookPart.
            WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet();

            // Add styles
            WorkbookStylesPart stylesPart = workbookpart.AddNewPart<WorkbookStylesPart>();
            stylesPart.Stylesheet = GenerateStylesheet();
            stylesPart.Stylesheet.Save();

            // Format columns
            var columns = this.CreateColumns(columnInfo.Count, 30);
            worksheetPart.Worksheet.Append(columns);

            // Add Sheets to the Workbook.
            Sheets sheets = workbookpart.Workbook.AppendChild(new Sheets());

            // Append a new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = workbookpart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet" };
            sheets.Append(sheet);

            // Get the sheetData cell table.
            SheetData sheetData = new SheetData();
            worksheetPart.Worksheet.Append(sheetData);

            sheetData.Append(this.CreateHeader(columnInfo));
            var index = 2;
            foreach (var item in data)
            {
                sheetData.Append(this.CreateRow(item, Convert.ToUInt32(index++), columnInfo));
            }

            // Close the document.
            spreadsheetDocument.Close();
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        /// <summary>
        /// Creates sheet header
        /// </summary>
        /// <param name="columns"></param>
        private Row CreateHeader(IList<ExportColumnInfo> columns)
        {
            var row = new Row { RowIndex = 1 };
            var cellIndex = 0;
            foreach (var column in columns)
            {
                var cell = new Cell { StyleIndex = 2 };
                cell.CellValue = new CellValue(column.Title);
                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                row.InsertAt(cell, cellIndex++);
            }

            return row;
        }

        /// <summary>
        /// Creates row in excel document
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        /// <param name="columns"></param>
        private Row CreateRow(object item, uint index, IList<ExportColumnInfo> columns)
        {
            var row = new Row { RowIndex = index };
            var cellIndex = 0;
            foreach (var column in columns)
            {
                var cell = new Cell();
                var propertyInfo = item.GetType().GetProperty(column.Property);
                var val = propertyInfo.GetValue(item)?.ToString();
                cell.CellValue = new CellValue(val);
                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                row.InsertAt(cell, cellIndex++);
            }

            return row;
        }

        /// <summary>
        /// Defines excel document columns
        /// </summary>
        /// <param name="numberOfColumns"></param>
        /// <param name="columnWidth"></param>
        private Columns CreateColumns(int numberOfColumns, DoubleValue columnWidth)
        {
            var columns = new Columns();
            for (int i = 0; i < numberOfColumns; i++)
            {
                var column = new Column { Width = columnWidth, Min = (uint)1, Max = (uint)numberOfColumns, CustomWidth = true, BestFit = true };
                columns.Append(column);
            }

            return columns;
        }

        /// <summary>
        /// Defines styles used in document
        /// </summary>
        private Stylesheet GenerateStylesheet()
        {
            var fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 10 }
                ), new Font( // Index 1 - header
                    new FontSize() { Val = 10 },
                    new Bold(),
                    new Color() { Rgb = "FFFFFF" }
                ));

            var fills = new Fills(
                new Fill(new PatternFill { PatternType = PatternValues.None }), // Index 0 - default
                new Fill(new PatternFill { PatternType = PatternValues.Gray125 }), // Index 1 - default
                new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "66666666" } }) { PatternType = PatternValues.Solid }) // Index 2 - header
            );

            var borders = new Borders(
                new Border(), // index 0 default
                new Border( // index 1 black border
                    new LeftBorder(new Color { Auto = true }) { Style = BorderStyleValues.Thin },
                    new RightBorder(new Color { Auto = true }) { Style = BorderStyleValues.Thin },
                    new TopBorder(new Color { Auto = true }) { Style = BorderStyleValues.Thin },
                    new BottomBorder(new Color { Auto = true }) { Style = BorderStyleValues.Thin },
                    new DiagonalBorder())
            );

            var cellFormats = new CellFormats(
                new CellFormat(), // default
                new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
            );

            return new Stylesheet(fonts, fills, borders, cellFormats);
        }
        #endregion
    }
}
