﻿using Refresh.Entities.Billing;
using System.Collections.Generic;

namespace Refresh.Services.Export.ExportModel
{
    public class BuildingExportModel
    {
        public List<BalanceView> Balances { get; set; } 
        public string DateNow { get; set; }
        public string CityName { get; set; }
    }
}
