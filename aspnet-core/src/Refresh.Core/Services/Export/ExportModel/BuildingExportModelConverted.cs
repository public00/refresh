﻿using System.Collections.Generic;

namespace Refresh.Services.Export.ExportModel
{
    public class BuildingExportModelConverted
    { 
        public List<BalanceViewExportDto> Balances { get; set; }
        public decimal UnknownPayments { get; set; }
        public decimal Balance { get; set; }
        public string DateNow { get; set; }
        public string CityName { get; set; }
        public string BankAccountNo { get; set; }
    }
}
