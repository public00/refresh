﻿namespace Refresh.Services.Export.ExportModel
{
    public class BalanceViewExportDto
    {
        public int Id { get; set; }
        public int BuildingId { get; set; }
        public string Pd { get; set; }
        public string ReferenceNumber { get; set; }
        public string Owner { get; set; }
        public int Quadrature { get; set; }
        public string Number { get; set; }
        public string Floor { get; set; }
        public string CityName { get; set; }
        public decimal? PricePerSquareMeter { get; set; }
        public decimal? MaintenanceFeeFixedPrice { get; set; }
        public decimal MonthlyMaintenanceRate { get; set; }
        public int PropertyTypeId { get; set; }
        public string PropertyTypeName { get; set; }
        public int Year { get; set; }
        public decimal PreviousDebt { get; set; }
        public string JanCost { get; set; }
        public string JanPaid { get; set; }
        public string FebCost { get; set; }
        public string FebPaid { get; set; }
        public string MarCost { get; set; }
        public string MarPaid { get; set; }
        public string AprCost { get; set; }
        public string AprPaid { get; set; }
        public string MayCost { get; set; }
        public string MayPaid { get; set; }
        public string JunCost { get; set; }
        public string JunPaid { get; set; }
        public string JulCost { get; set; }
        public string JulPaid { get; set; }
        public string AugCost { get; set; }
        public string AugPaid { get; set; }
        public string SepCost { get; set; }
        public string SepPaid { get; set; }
        public string OctCost { get; set; }
        public string OctPaid { get; set; }
        public string NovCost { get; set; }
        public string NovPaid { get; set; }
        public string DecCost { get; set; }
        public string DecPaid { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal Debt => TotalCost - TotalPaid;
    }
}
