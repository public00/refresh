﻿using Abp.Dependency;
using Refresh.Entities.Billing;
using System.Collections.Generic;

namespace Refresh.Services.Export 
{
    public interface IReportExportHelper : ITransientDependency
    {
        List<PropertyMaintenanceReport> GetPropertyMaintenanceReport(int buildingId);

        List<PropertyReportModelDto> GetPropertyReportExportWord(int building, int year, int month);
    }
}
