﻿namespace Refresh.Services.Export
{
    using System.Collections.Generic;
    using System.IO;
    using Abp.Dependency;
    using System;
    using System.Net.Http;
    using ExportModel;

    public interface IExportService : ITransientDependency
    {
        Stream ExportToExcel<T>(IList<T> data, IList<ExportColumnInfo> columnInfo);

        Stream BuildingFinancialReport(int buildingId, DateTime from, DateTime to); // financijski izvjestaj

        Stream ExportExcelReport(BuildingExportModel model, int buildingId, int year); // Template.xlsx

        Stream GetInvoices(int buildingId, int month, int year); // download za building
         
        Stream GetPropertyInvoice(int buildingId, int month, int year, bool print); // download za property

        Stream GetExpenses(int buildingId, int month, int year);

        Stream ExportWordInvoice(object invoices, int id);
    }
}
