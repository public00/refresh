﻿using Abp.Domain.Repositories;
using Refresh.Entities.Billing;
using Refresh.Entities.Buildings;
using Refresh.Entities.Properties;
using System.Linq;
using System.Collections.Generic;
using Refresh.Entities.Owners;
using System;
using System.Reflection;
using Refresh.Entities.Billing.Reports;

namespace Refresh.Services.Export
{
    using Views;

    public class ReportExportHelper : IReportExportHelper
    {
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;
        private readonly IRepository<Property> _propertyRepository;
        private readonly IRepository<Building> _buildingRepository;
        private readonly IRepository<MaintenancePaymentTracker> _maintenancePaymentTrackerRepository;
        private readonly IRepository<PropertyOwner> _propertyOwnersRepository;
        private readonly IRepository<Owner> _ownerRepository;
        private readonly IRepository<PropertyType> _propertyTypeRepository;
        private readonly IRepository<ExpenseCategory> _expenseCategoriesRepository;
        private readonly IRepository<Expense> _expensesRepository;
        private readonly IRepository<SentPayment> _sentPaymentsRepository;
        private readonly IBalanceViewQuery _balanceViewQuery;

        public ReportExportHelper(
            IRepository<Building> buildingRepository,
            IRepository<Property> propertyRepository,
            IRepository<Owner> ownerRepository,
            IRepository<MaintenanceFee> maintenanceFeeRepository,
            IRepository<PropertyOwner> propertyOwnerRepository,
            IRepository<PropertyType> propertyType,
            IRepository<ExpenseCategory> expenseCategoryRepository,
            IRepository<MaintenancePaymentTracker> maintenancePaymentTrackerRepository,
            IRepository<Expense> expensesRepository,
            IRepository<SentPayment> sentPayments, 
            IBalanceViewQuery balanceViewQuery)
        {
            _buildingRepository = buildingRepository;
            _propertyOwnersRepository = propertyOwnerRepository;
            _ownerRepository = ownerRepository;
            _expenseCategoriesRepository = expenseCategoryRepository;
            _propertyTypeRepository = propertyType;
            _propertyRepository = propertyRepository;
            _maintenanceFeeRepository = maintenanceFeeRepository;
            _maintenancePaymentTrackerRepository = maintenancePaymentTrackerRepository;
            _expensesRepository = expensesRepository;
            _sentPaymentsRepository = sentPayments;
            _balanceViewQuery = balanceViewQuery;
        }

        public FinancialReport GetBuildingFinancialReport(int buildingId, DateTime from, DateTime to)
        {
            var properties = _propertyRepository.GetAllList(x => x.BuildingId == buildingId);
            FinancialReport report = new FinancialReport();
            var propertyTypes = _propertyTypeRepository.GetAllList();

            List<WorkExpenses> revenues = new List<WorkExpenses>();
            foreach (var item in propertyTypes)
            {
                WorkExpenses revenue = new WorkExpenses {Name = item.Name};
                var prop = (from a in properties where a.PropertyTypeId == item.Id select a);
                var typesPropIds = (from a in prop.ToList() select a.Id).ToArray();
                var maintenances = _maintenanceFeeRepository.GetAllList(x => typesPropIds.Contains(x.PropertyId) && new DateTime(x.Year, x.Month, 1) >= from && new DateTime(x.Year, x.Month, 1) < to);
                var maintenanceFeeIds = (from a in maintenances select a.Id).ToArray();
                revenue.Invoiced = maintenances.Sum(x => x.Amount);
                revenue.Paid = _maintenancePaymentTrackerRepository.GetAllList(x => maintenanceFeeIds.Contains(x.MaintenanceFeeId)).Sum(x => x.Amount);
                if (revenue.Invoiced != 0 || revenue.Paid != 0)
                {
                    revenues.Add(revenue);
                }
            }

            var previous = this._maintenanceFeeRepository.GetAllIncluding(x => x.Property)
                .Where(x => !x.IsFinishedPayment && x.Property.BuildingId == buildingId && new DateTime(x.Year, x.Month, 1) < from).ToList();
            var feeIds = (from a in previous select a.Id).ToArray();
            var paymentTrackers = this._maintenancePaymentTrackerRepository.GetAllList(x => feeIds.Contains(x.MaintenanceFeeId));

            var previousRevenue = new WorkExpenses
            {
                Name = "Prethodni dug",
                Invoiced = previous.Select(x => x.Amount).DefaultIfEmpty(0).Sum()
                           - paymentTrackers.Where(x => x.CreationTime < @from).Select(x => x.Amount).DefaultIfEmpty(0).Sum(),
                Paid = paymentTrackers.Where(x => x.CreationTime >= @from).Select(x => x.Amount).DefaultIfEmpty(0).Sum()
            };
            revenues.Add(previousRevenue);
            report.Revenues = revenues;

            var rootCategories = _expenseCategoriesRepository.GetAllList(x => x.ParentId == null);

            foreach (var category in rootCategories)
            {
                var items = GetModelItems(category.Id, buildingId, from, to);
                if (items.Count != 0)
                {
                    report.Expenses.Add(new ExpenseGroup
                    {
                        Name = category.Name,
                        Items = GetModelItems(category.Id, buildingId, from, to)
                    });
                }
            }

            return report;
        }

        public List<WorkExpenses> GetModelItems(int id, int buildingId, DateTime from, DateTime to)
        {
            List<WorkExpenses> returnModel = new List<WorkExpenses>();
            var list = _expenseCategoriesRepository.GetAllList(x => x.ParentId == id);
            foreach (var item in list)
            {
                WorkExpenses m = new WorkExpenses();
                m.Name = item.Name;
                var amounts = GetAmountAndDebt(item, buildingId, from, to);
                m.Paid = amounts[0];
                m.Invoiced = amounts[1];
                if (amounts[0] != 0 || amounts[1] != 0)
                {
                    returnModel.Add(m);
                }
            }

            return returnModel;
        }

        public List<decimal> GetAmountAndDebt(ExpenseCategory category, int buildingId, DateTime from, DateTime to)
        {
            List<decimal> list = new List<decimal>();

            var expenses = this._expensesRepository.GetAllList(x => x.BuildingId == buildingId && x.SubCategoryId == category.Id && x.CreationTime >= from && x.CreationTime < to);
            var expenseIds = (from a in expenses select a.Id).ToList();
            var sent = this._sentPaymentsRepository.GetAllList(x => expenseIds.Contains(x.Id) && x.CreationTime >= from && x.CreationTime < to);

            var sumInvoiced = expenses.Sum(x => x.Amount);
            var sumSent = sent.Sum(x => x.Amount);
            list.Add(sumInvoiced);
            list.Add(sumSent);
            return list;
        }

        public PropertyReportModelDto GeneratePropertyReportModel(Building building, Property property, int month, int year)
        {
            var balance = _balanceViewQuery.GetQueryable().FirstOrDefault(x => x.Id == property.Id && x.Year == year);
            var fees = _maintenanceFeeRepository.GetAllList(x => (x.Month <= month && x.Year == year || x.Year < year)
               && x.PropertyId == property.Id && x.IsDeleted == false && x.IsFinishedPayment == false);
            var feeIds = (from a in fees select a.Id).ToArray();
            var trackers = (from a in _maintenancePaymentTrackerRepository.GetAllList(x => feeIds.Contains(x.MaintenanceFeeId) && x.IsDeleted == false) select a);

            var sumTrackers = trackers.Sum(x => x.Amount);
            var maintenanceAmount = fees.Sum(x => x.Amount);
            var total = maintenanceAmount - sumTrackers;
            var currentMonthAmount = fees.Where(x => x.Month == month && x.Year == year).ToList().Sum(x => x.Amount);

            PropertyReportModelDto propertyModel = new PropertyReportModelDto();
            var propOwner = _propertyOwnersRepository.GetAllList(x => x.PropertyId == property.Id).FirstOrDefault();
            Refresh.Months monthString = (Months)month;
            string stringMonth = monthString.ToString();

            var owner = _ownerRepository.GetAllList(x => x.Id == propOwner.OwnerId).FirstOrDefault();
            propertyModel.Type = _propertyTypeRepository.Get(property.PropertyTypeId).Name;
            propertyModel.Id = property.Id;
            propertyModel.Name = owner?.FirstName + " " + owner?.LastName;
            propertyModel.Quadrature = property.Quadrature;
            propertyModel.Date = $"{DateTime.Now:dd}.{DateTime.Now:MM}.{DateTime.Now.Year}";
            propertyModel.AccountingPeriod = GetAccountingPeriod();
            propertyModel.Pib = building.PIB;
            propertyModel.Account = building.BankAccountPrimary;
            propertyModel.TrackerAmount = sumTrackers;
            propertyModel.StreetAddress = building.StreetAddress;
            propertyModel.Month = stringMonth;
            propertyModel.Year = year;
            propertyModel.FcaName = building.FCAName;
            propertyModel.FcaAddress = building.FCAAddress;
            propertyModel.IBan = building.IBAN;
            propertyModel.ApprovalNumber = $"{building.PIB}/{property.Pd}/{year}/{month:00}";
            propertyModel.Law = @"Račun je izdat u skladu sa Čl. 16 stav 1,2,3 Zakona o održavanju stambenih zgrada ['Službeni list CG' br. 041/16 I 084/18]";
            propertyModel.PropertyPd = property.Pd;
            propertyModel.Amount = currentMonthAmount.ToString("F") + " €";
            propertyModel.Debt = (total - currentMonthAmount).ToString("F") + " €";
            propertyModel.Total = (total - balance?.Overpaid ?? 0).ToString("F") + " €";

            return propertyModel;
        }

        public string GetAccountingPeriod()
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            return $"{startDate:dd}.{startDate:MM}. - {endDate:dd}.{endDate:MM}.{endDate.Year}";
        }

        // Metoda koja generise zip sa svim fakturama za svaki properti u odredjenoj zgradi za odredjeni mjesec
        public List<PropertyReportModelDto> GetPropertyReportExportWord(int buildingId, int year, int month) // Dugme Racuni na staroj app
        {
            var building = _buildingRepository.Get(buildingId);
            List<Property> properties = _propertyRepository.GetAllList(x => x.BuildingId == buildingId);
            List<PropertyReportModelDto> model = new List<PropertyReportModelDto>();
            foreach (var prop in properties)
            {
                model.Add(this.GeneratePropertyReportModel(building, prop, month, year));
            }

            return model;
        }

        public List<PropertyMaintenanceReport> GetPropertyMaintenanceReport(int buildingId) //  Presjek stanja na prosloj app
        {
            List<PropertyMaintenanceReport> list = new List<PropertyMaintenanceReport>();
            var building = _buildingRepository.Get(buildingId);

            foreach (var property in _propertyRepository.GetAllList(x => x.BuildingId == buildingId))
            {
                var propertyOwner = _propertyOwnersRepository.GetAllList(x => x.PropertyId == property.Id).FirstOrDefault();
                var owner = _ownerRepository.GetAllList(x => x.Id == propertyOwner.OwnerId).FirstOrDefault();
                List<MaintenanceFee> maintenanceFee = _maintenanceFeeRepository.GetAllList(x => x.PropertyId == property.Id);

                var report = new PropertyMaintenanceReport();
                Type type = report.GetType();

                for (int i = 1; i <= 12; i++)
                {
                    Months month = (Months)i;
                    string stringMonth = month.ToString();
                    PropertyInfo prop = type.GetProperty(stringMonth);
                    List<MaintenanceFee> fees = (from a in maintenanceFee where a.Month == i select a).ToList();
                    var feesSum = fees.Sum(x => x.Amount);
                    var feeIds = (from a in fees select a.Id).ToArray();
                    var sumTrackers = (from a in _maintenancePaymentTrackerRepository.GetAllList(x => feeIds.Contains(x.MaintenanceFeeId)) select a).Sum(x => x.Amount);
                    var value = feesSum + " \\ " + sumTrackers;
                    prop.SetValue(report, value, null);
                }

                report.Floor = property.Floor;
                report.Name = owner?.FirstName + owner?.LastName;
                report.PropertyNumber = property.Number;
                report.Quadrature = property.Quadrature;
                report.DateToday = new DateTime().ToLocalTime().ToLongDateString();
                // report.BuildingName = building.City.Name;
                report.Debt = 000000;//TODO:: 
                report.Amount = property.PricePerSquareMeter ?? building.PricePerSquareMeter * property.Quadrature;
                report.AccountMoney = 00000; // TODO ::
                list.Add(report);
            }

            return list;
        }
    }
}
