﻿namespace Refresh.Services.Export
{
    public class ExportColumnInfo
    {
        public string Property { get; set; }

        public string Title { get; set; }
    }
}
