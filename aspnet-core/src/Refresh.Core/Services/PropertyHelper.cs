﻿using Abp.Domain.Repositories;
using Refresh.Entities.Billing;
using Refresh.Entities.Properties;
using System;
using System.Linq;

namespace Refresh.Services
{
    public class PropertyHelper
    {
        private readonly IRepository<MaintenanceFee> _maintenanceFeeRepository;
        private readonly IRepository<MaintenancePaymentTracker> _maintenancePaymentTrackerRepository;
        private readonly IRepository<Property> _propertyRepository;

        public PropertyHelper(IRepository<Property> propertyRepository, IRepository<MaintenancePaymentTracker> maintenancePaymentTrackerRepository, IRepository<MaintenanceFee> maintenanceFeeRepository)
        {
            _maintenanceFeeRepository = maintenanceFeeRepository;
            _propertyRepository = propertyRepository;
            _maintenancePaymentTrackerRepository = maintenancePaymentTrackerRepository;
        }

        public PropertyInfoReport GetPropertyInfo(int buildingId)
        {
            PropertyInfoReport report = new PropertyInfoReport();
            var properties = _propertyRepository.GetAllList(x => x.BuildingId == buildingId);
            var propertyIds = (from a in properties select a.Id).ToArray();
            var maintenanceFees = _maintenanceFeeRepository.GetAllList(x => propertyIds.Contains(x.PropertyId));
            var maintenanceFeeIds = (from a in properties select a.Id).ToArray();
            var paymentTracker = _maintenancePaymentTrackerRepository.GetAllList(x=> maintenanceFeeIds.Contains(x.MaintenanceFeeId));
            report.MaintenanceFees = maintenanceFees;
            report.Properties = properties;
            report.MaintenancePaymentTrackers = paymentTracker;
            report.Invoiced = maintenanceFees.Sum(x=>x.Amount);
            report.Paid = paymentTracker.Sum(x=>x.Amount);
            report.PropertyIds = propertyIds;
            return report;
        }
    }
}
