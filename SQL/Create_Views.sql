--==========================================================================================================--
-- 001. BuildingView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'BuildingView')
drop view dbo.BuildingView
go
create view dbo.BuildingView as 
	select 
		b.Id,
		b.CityId,
		c.Name as CityName,
		b.StreetAddress,
		b.BankAccountPrimary,
		b.PIB,
		b.PlotNumber,
		b.ImmovableProperty,
		b.ManagerId,
		case when mng.Id is null then null else isnull(mng.FirstName, '') + ' ' + isnull(mng.LastName, '') end as ManagerName,
		b.PresidentId,
		case when pr.Id is null then null else isnull(pr.FirstName, '') + ' ' + isnull(pr.LastName, '') end as PresidentName,
		(select sum(isnull(quadrature, 0)) from dbo.Properties where BuildingId = b.Id and IsDeleted = 0) as Quadrature,
		isnull((select sum(Amount) from ReceivedPayments where BuildingId = b.Id and IsDeleted = 0), 0) 
			- isnull((select sum(Amount) from SentPayments where BuildingId = b.Id and IsDeleted = 0), 0) as Balance
	from dbo.Buildings b
	inner join dbo.Cities c on c.Id = b.CityId and c.IsDeleted = 0
	left join dbo.Owners mng on mng.Id = b.ManagerId and mng.IsDeleted = 0
	left join dbo.Owners pr on pr.Id = b.PresidentId and pr.IsDeleted = 0
	where b.IsDeleted = 0
go

--==========================================================================================================--
--==========================================================================================================--
-- 002. PropertyOwnerView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'PropertyOwnerView')
drop view dbo.PropertyOwnerView
go

create view dbo.PropertyOwnerView as 
	select 
		po.Id,
		po.PropertyId,
		po.OwnerId,
		isnull(o.FirstName, '') + ' ' + isnull(o.LastName, '') as OwnerName,
		o.JMBG,
		o.Email,
		o.DateOfBirth,
		o.Address
	from dbo.PropertyOwners as po
	inner join dbo.Properties as p on p.Id = po.PropertyId and p.IsDeleted = 0
	inner join dbo.Owners as o on o.Id = po.OwnerId and p.IsDeleted = 0
	where po.IsDeleted = 0
go

--==========================================================================================================--
--==========================================================================================================--
-- 003. ReportView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'ReportView')
drop view dbo.ReportView
go

create view dbo.ReportView as 
	select
		r.Id,
		r.Title,
		u.Id as JanitorId,
		isnull(u.Name, '') + ' ' + isnull(u.Surname, '') as Janitor,
		r.[From],
		r.[To],
		b.Id as BuildingId,
		b.StreetAddress as Client,
		c.Id as CityId,
		c.Name as CityName,
		r.MaintenanceType,
		r.Invoice,
		r.SpentResources,
		r.Quantity,
		r.NPrice,
		r.PPrice,
		r.TimeSpent,
		r.Note
	from Reports as r
	left outer join Buildings as b on r.BuildingId = b.Id and b.IsDeleted = 0
	left outer join Cities as c on b.CityId = c.Id and c.IsDeleted = 0
	left outer join dbo.AbpUsers as u on r.CreatorUserId =  u.Id and u.IsDeleted = 0
	where r.IsDeleted = 0
 go

--==========================================================================================================--
--==========================================================================================================--
-- 004. SupplyView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'SupplyView')
drop view dbo.SupplyView
go

create view dbo.SupplyView as
	select
		s.Id,
		s.Date,
		s.InvoiceNumber,
		c.Id as CityId,
		c.Name as CityName,
		s.Name,
		s.Quantity,
		s.ItemPrice,
		s.Note,
		o.Name + ' ' + o.Surname as CreatedBy
	from Supplies as s
	left outer join Cities as c on s.CityId = c.Id and c.IsDeleted = 0
	left outer join AbpUsers as o on s.CreatorUserId = o.Id
	where s.IsDeleted = 0
go

--==========================================================================================================--
--==========================================================================================================--
-- 005. InvoiceView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'InvoiceView')
drop view dbo.InvoiceView
go

create view dbo.InvoiceView as
	select
		i.Id,
		i.ReferenceNumber,
		i.ProinvoiceDate,
		i.InvoiceDate,
		i.BuildingId,
		b.StreetAddress as BuildingAddress,
		i.CreatorUserId,
		u.Name + ' ' + u.Surname as CreatorName,
		(select sum((Quantity * Price) + (case when Vat is null then 0 else (Quantity * Price * Vat / 100) end)) 
			from dbo.InvoiceItems where InvoiceId = i.Id and IsDeleted = 0) as Total
	from dbo.Invoices as i
	inner join dbo.Buildings as b on b.Id = i.BuildingId and b.IsDeleted = 0
	left outer join dbo.AbpUsers as u on u.Id = i.CreatorUserId and u.IsDeleted = 0
	where i.IsDeleted = 0
go

--==========================================================================================================--
--==========================================================================================================--
-- 06. BalanceView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'BalanceView')
drop view dbo.BalanceView
go

create view [dbo].[BalanceView] as
		
	select
		p.Id,
		p.BuildingId,
		p.Pd,
		p.ReferenceNumber,
		(select top 1 isnull(FirstName, '') + ' ' + isnull(LastName, '') from Owners 
			where IsDeleted = 0 and Id in (select OwnerId from PropertyOwners where PropertyId = p.Id and IsDeleted = 0)) as Owner,
		p.Quadrature,
		p.Number,
		c.[Name] as CityName, 
		p.[Floor],
		p.PricePerSquareMeter,
		p.MaintenanceFeeFixedPrice,
		isnull(p.MaintenanceFeeFixedPrice, p.Quadrature * isnull(p.PricePerSquareMeter, b.PricePerSquareMeter)) as MonthlyMaintenanceRate,
		p.PropertyTypeId,
		pt.Name as PropertyTypeName,
		m.Year,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 1), 0) as JanCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 1)), 0) as JanPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 2), 0) as FebCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 2)), 0) as FebPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 3), 0) as MarCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 3)), 0) as MarPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 4), 0) as AprCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 4)), 0) as AprPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 5), 0) as MayCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 5)), 0) as MayPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 6), 0) as JunCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 6)), 0) as JunPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 7), 0) as JulCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 7)), 0) as JulPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 8), 0) as AugCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 8)), 0) as AugPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 9), 0) as SepCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 9)), 0) as SepPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 10), 0) as OctCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 10)), 0) as OctPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 11), 0) as NovCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f  where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 11)), 0) as NovPaid,
		isnull((select sum(Amount) from MaintenanceFee f 
			where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 12), 0) as DecCost,
		isnull((select sum(Amount) from MaintenancePaymentTrackers where IsDeleted = 0 and MaintenanceFeeId in 
			(select Id from MaintenanceFee f where f.PropertyId = p.Id and f.Year = m.Year and IsDeleted = 0 and f.Month = 12)), 0) as DecPaid,
		sum(m.Amount) as TotalCost,
		case when m.year = year(getdate())
		then isnull((select sum(Amount) from ReceivedPayments as r where r.IsDeleted = 0 and r.PropertyId = p.Id), 0) 
			- isnull((select sum(Amount) from MaintenancePaymentTrackers as pt where pt.IsDeleted = 0 and MaintenanceFeeId in (select id from MaintenanceFee where IsDeleted = 0 and PropertyId = p.Id)), 0)
		else 0 end as Overpaid,
		isnull((select sum(Amount) from MaintenanceFee where IsDeleted = 0 and IsFinishedPayment = 0 and PropertyId = p.Id and Year < m.Year), 0)
		- isnull((select sum(Amount) from MaintenancePaymentTrackers as pt where pt.IsDeleted = 0 and MaintenanceFeeId in 
				(select id from MaintenanceFee where IsDeleted = 0 and IsFinishedPayment = 0 and PropertyId = p.Id and Year < m.Year)), 0) as PreviousDebt
	from MaintenanceFee as m 
	inner join Properties as p on p.Id = m.PropertyId and p.IsDeleted = 0
	inner join PropertyTypes as pt on pt.Id = p.PropertyTypeId and p.IsDeleted = 0
	inner join Buildings as b on p.BuildingId = b.Id
	inner join Cities as c on b.CityId = c.Id
	where m.IsDeleted = 0
	group by  
		p.Id,
		p.BuildingId,
		p.Pd,
		p.ReferenceNumber,
		p.Quadrature,
		b.PricePerSquareMeter,
		p.Number,
		p.PricePerSquareMeter,
		p.MaintenanceFeeFixedPrice,
		p.PropertyTypeId,
		pt.Name,
		p.[Floor],
		c.[Name],
		m.Year
GO


--==========================================================================================================--

--==========================================================================================================--
--==========================================================================================================--
-- 007. TicketView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'TicketView')
drop view dbo.TicketView
go

create view dbo.TicketView as
	select
		t.Id,
		t.BuildingId,
		b.StreetAddress,
		c.Id as CityId,
		c.Name as CityName,
		t.Title,
		t.Description,
		t.PropertyId,
		isnull(p.ReferenceNumber, '-') as PropertyReferenceNumber,
		t.CreationTime,
		t.IsClosed,
		t.CreatorUserId,
		u.Name + ' ' + u.Surname as CreatedBy
	from dbo.Tickets as t
	inner join dbo.Buildings as b on b.Id = t.BuildingId
	inner join dbo.Cities as c on c.Id = b.CityId
	left join dbo.Properties as p on p.Id = t.PropertyId
	left outer join dbo.AbpUsers as u on u.Id = t.CreatorUserId and u.IsDeleted = 0
	where t.IsDeleted = 0
go


--==========================================================================================================--

--==========================================================================================================--
--==========================================================================================================--
-- 008. PropertyDebtView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'PropertyDebtView')
drop view dbo.PropertyDebtView
go

create view [dbo].[PropertyDebtView] as
	select
		p.Id,
		b.Id as BuildingId,
		p.ReferenceNumber,
		p.Quadrature,
		p.Floor,
		p.Number,
		b.CityId,
		isnull(p.MaintenanceFeeFixedPrice, p.Quadrature * isnull(p.PricePerSquareMeter, b.PricePerSquareMeter)) as MonthlyMaintenanceRate,
		p.PropertyTypeId,
		pt.Name as PropertyTypeName,
		(select top 1 FirstName + ' ' + LastName from Owners 
				where IsDeleted = 0 and Id in (select OwnerId from PropertyOwners where PropertyId = p.Id and IsDeleted = 0)) as Owner,
		isnull((select sum(Amount) from MaintenanceFee where PropertyId = p.Id and IsDeleted = 0), 0) as TotalCost,
		isnull((select sum(Amount) from ReceivedPayments where PropertyId = p.Id and IsDeleted = 0), 0) as TotalPaid,
		cast(case when exists(select * from MaintenanceFee m where PropertyId = p.Id and IsDeleted = 0 and IsFinishedPayment = 0 and datefromparts(m.Year, m.Month, 1) < DATEADD(m, -3, GETUTCDATE())) then 1 else 0 end as bit) as IsOverdue,
		isnull(pls.Note, '-') as LegalStatus
	from Properties as p
	inner join Buildings as b on b.Id = p.BuildingId
	inner join PropertyTypes as pt on pt.Id = p.PropertyTypeId
	left join PropertyLegalStatuses as pls on pls.PropertyId = p.Id
	where cast(case when exists(select * from MaintenanceFee m where PropertyId = p.Id and IsDeleted = 0 and IsFinishedPayment = 0 and datefromparts(m.Year, m.Month, 1) < DATEADD(m, -3, GETUTCDATE())) then 1 else 0 end as bit) = 1
	and (isnull((select sum(Amount) from MaintenanceFee where PropertyId = p.Id and IsDeleted = 0), 0) - isnull((select sum(Amount) from ReceivedPayments where PropertyId = p.Id and IsDeleted = 0), 0)) > 30
go


--==========================================================================================================--

--==========================================================================================================--
--==========================================================================================================--
-- 008. PropertyDebtView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'PropertyPickerView')
drop view dbo.PropertyPickerView
go

create view [dbo].[PropertyPickerView] as
	select 
		p.Id,
		p.BuildingId,
		p.ReferenceNumber + 
		case when (select top 1 id from PropertyOwners where PropertyId = p.Id and IsDeleted = 0) is null
		then ''
		else ' - ' + isnull((STUFF((
			SELECT ', ' + isnull(FirstName, '') + ' ' + isnull(LastName, '')
			FROM Owners
			WHERE Id in (select OwnerId from PropertyOwners where PropertyId = p.Id and IsDeleted = 0)
			FOR XML PATH('')
			), 1, 2, '')
		), '')
		END AS ReferenceNumber
	from Properties as p
	where IsDeleted = 0
go


--==========================================================================================================--

--==========================================================================================================--
--==========================================================================================================--
-- 009. OwnerView
--==========================================================================================================--

if exists(select * from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'OwnerView')
drop view dbo.OwnerView
go

create view [dbo].[OwnerView] as
	select 
		Id,
		case when FirstName is null then '' else FirstName + ' ' end
		+ case when LastName is null then '' else LastName + ' ' end
		+ case when Email is null then '' else Email + ' ' end
		+ case when JMBG is null then '' else JMBG + ' ' end as Text
	from Owners where IsDeleted = 0
go


--==========================================================================================================--